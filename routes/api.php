<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
Route::get('dropdown', 'ApiLocationController@findKelurahan')->name('find.kelurahan');
Route::get('puskes', 'Api\ApiController@anak_puskes')->name('api.puskes');
Route::get('all', 'Api\ApiController@anak_all')->name('api.all');
Route::get('ortu', 'Api\ApiController@ortu')->name('ortu.all');
Route::get('log', 'Api\ApiController@log')->name('puskes.log');

