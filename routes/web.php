<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('/home');
});

Auth::routes();
Route::get('/home', ['middleware' => 'auth', function () {
    if(Auth::user()->role()->first()->id == 2 || Auth::user()->role()->first()->id == 4) //Admin Puskes dan Petugas Puskes
    {
        if (Auth::user()->puskesmas()->count() <= 1)
        {
            return redirect(route('puskesmas.dashboard',Auth::user()->puskesmas()->first()->id));
        }
        else{
            return redirect(route('pilih establis [belum]'));//lebih dari 1 establishment
        }
    }
    else if (Auth::user()->role()->first()->id == 1)//Dinkes dan Super Admin(Dinkes)
    {
        return redirect(route('puskesmas.dinkes'));
    }
    elseif  (Auth::user()->role()->first()->id == 5)
    {
        return redirect(route('laporan.dinkes'));
    }
    else{
        if (isset(Auth::user()->ortu()->first()->address)){
            return redirect(route('ortu.dashboard',Auth::user()->ortu()->first()->id));// Orang tua Anak
        }
        else{
            return redirect(route('lengkapi',Auth::user()->ortu()->first()->id));
        }
    }
}]);

Route::get('/leaflet', 'TestingController@index')->name('leaflet');
Route::get('/test_point', 'TestingController@point')->name('test_point');
Route::get('/cek1', 'TestingController@cek1')->name('cek_point');
Route::get('/cal', 'TestingController@cal')->name('test_calendar');
Route::get('verify/{token}', 'OrtuController@verify')->name('veri');
Route::post('register/ortu', 'OrtuController@post_reg')->name('ortu_post_reg');
Route::get('register/success', 'OrtuController@reg_success')->name('ortu_reg_suc');
//Route::get('/', 'HomeController@index')->name('home');

Auth::routes();

//Route::get('/home', 'HomeController@index')->name('home');
