<?php

use Illuminate\Database\Seeder;
use App\Assigment;

class AssigmentTableSeeder extends Seeder
{
    protected $data = [
        [
            'user_id'        => '2',
            'puskesmas_id'           => '1',
        ],
        [
            'user_id'        => '3',
            'puskesmas_id'           => '1',
        ],
        [
            'user_id'        => '24',
            'puskesmas_id'           => '2',
        ],
        [
            'user_id'        => '25',
            'puskesmas_id'           => '2',
        ],
        [
            'user_id'        => '26',
            'puskesmas_id'           => '3',
        ],
        [
            'user_id'        => '27',
            'puskesmas_id'           => '3',
        ],
        [
            'user_id'        => '28',
            'puskesmas_id'           => '4',
        ],
        [
            'user_id'        => '29',
            'puskesmas_id'           => '4',
        ],

    ];
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach($this->data as $user)
        {
            Assigment::create($user);
        }

        $this->command->info('Proses Penambahan Data User Berhasil');
    }
}
