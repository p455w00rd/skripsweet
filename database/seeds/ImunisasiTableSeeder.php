<?php

use Illuminate\Database\Seeder;
use Flynsarmy\CsvSeeder\CsvSeeder;
use Illuminate\Support\Facades\DB;

class ImunisasiTableSeeder extends CsvSeeder
{
    public function __construct()
    {
        $this->table = 'imunisasi';
        $this->filename = base_path().'/database/seeds/data/imunisasi.csv';
    }
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Recommended when importing larger CSVs
        DB::disableQueryLog();

        // Uncomment the below to wipe the table clean before populating
//        DB::table($this->table)->truncate();

        parent::run();
        $this->command->info('Proses Penambahan Data Imunisasi Berhasil');
    }
}
