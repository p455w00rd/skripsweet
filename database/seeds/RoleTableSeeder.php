<?php

use Illuminate\Database\Seeder;
use App\Role;

class RoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    protected $data = [
        [
            'type'        => 'SuperAdmin(dinkes)',
        ],
        [
            'type'        => 'Admin Puskesmas ',
        ],
        [
            'type'        => 'Parent',
        ],
        [
            'type'        => 'Pegawai Puskesmas',
        ],
        [
            'type'        => 'Dinkes',
        ],

    ];

    public function run()
    {
        foreach($this->data as $role)
        {
            Role::create($role);
        }

        $this->command->info('Proses Penambahan Data Roles Berhasil');
    }
}
