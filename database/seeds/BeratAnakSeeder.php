<?php

use Illuminate\Database\Seeder;
use App\Berat;

class BeratAnakSeeder extends Seeder
{
    protected $data = [
        [
            'berat'        => '3',
        ],

    ];
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach($this->data as $user)
        {
            Berat::create($user);
        }

        $this->command->info('Proses Penambahan Data Berat Berhasil');
    }
}
