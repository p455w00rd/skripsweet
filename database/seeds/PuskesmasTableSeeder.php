<?php

use Illuminate\Database\Seeder;
use App\Puskesmas;

class PuskesmasTableSeeder extends Seeder
{
    protected $data = [
        [
            'name'        => 'Puskesmas Tambakrejo',
            'address'           => 'Jl. Ngaglik No.87',
            'phone'        => '0123232',
            'location'        => '22',
        ],
        [
            'name'        => 'Puskesmas Mojo',
            'address'           => 'Jl. Mojo Klanggru Wetan II No.11',
            'phone'        => '0123232',
            'location'        => '8',
        ],
        [
            'name'        => 'Puskesmas Pegirian',
            'address'           => 'Jl. Karang Tembok No.39',
            'phone'        => '0123232',
            'location'        => '21',
        ],
        [
            'name'        => 'Puskesmas Sidotopo Wetan',
            'address'           => 'Jl. Randu',
            'phone'        => '0123232',
            'location'        => '12',
        ],

    ];
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach($this->data as $user)
        {
            Puskesmas::create($user);
        }

        $this->command->info('Proses Penambahan Data User Berhasil');
    }
}
