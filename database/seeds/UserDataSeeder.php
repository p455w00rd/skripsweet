<?php

use Illuminate\Database\Seeder;
use App\UserData;

class UserDataSeeder extends Seeder
{
    protected $data = [
        [
            'user_id'        => '1',
            'name'           => 'ini admin dinkes',
            'nip'        => '123123',
            'phone'        => '0123232'
            
        ],
        [
            'user_id'        => '2',
            'name'           => 'ini Pegawai Puskesmas',
            'nip'        => '123123',
            'phone'        => '0123232'
        ],
        [
            'user_id'        => '3',
            'name'           => 'ini admin Puskesmas',
            'nip'        => '123123',
            'phone'        => '0123232'
        ],
        [
            'user_id'        => '24',
            'name'           => 'ini Admin Puskesmas',
            'nip'        => '123123',
            'phone'        => '0123232'
        ],
        [
            'user_id'        => '25',
            'name'           => 'ini Pegawai Puskesmas',
            'nip'        => '123123',
            'phone'        => '0123232'
        ],
        [
            'user_id'        => '26',
            'name'           => 'ini Admin Puskesmas',
            'nip'        => '123123',
            'phone'        => '0123232'
        ],
        [
            'user_id'        => '27',
            'name'           => 'ini Pegawai Puskesmas',
            'nip'        => '123123',
            'phone'        => '0123232'
        ],
        [
            'user_id'        => '28',
            'name'           => 'ini Admin Puskesmas',
            'nip'        => '123123',
            'phone'        => '0123232'
            
        ],
        [
            'user_id'        => '29',
            'name'           => 'ini Pegawai Puskesmas',
            'nip'        => '123123',
            'phone'        => '0123232'
            
        ],

    ];
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach($this->data as $user)
        {
            UserData::create($user);
        }

        $this->command->info('Proses Penambahan Data User Berhasil');
    }
}
