<?php

use Illuminate\Database\Seeder;
use App\UserRole;

class UserRoleTableSeeder extends Seeder
{

    protected $data = [
        [
            'user_id'        => '1',
            'role_id'           => '1',
        ],
        [
            'user_id'        => '2',
            'role_id'           => '2',
        ],
        [
            'user_id'        => '3',
            'role_id'           => '4',
        ],

    ];
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach($this->data as $user)
        {
            UserRole::create($user);
        }

        $this->command->info('Proses Penambahan Data User Berhasil');
    }
}
