<?php

use Illuminate\Database\Seeder;
use App\User;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */

    protected $data = [
        [
            'username'        => 'admin',
            'email'           => 'admin@admin.com',
            'password'        => 'admin'
        ],
        [
            'username'        => 'admintambakrejo',
            'email'           => 'admintambakrejo@admin.com',
            'password'        => 'admintambakrejo'
        ],
        [
            'username'        => 'tambakrejo',
            'email'           => 'tambakrejo@admin.com',
            'password'        => 'tambakrejo'
        ],

    ];

    public function run()
    {
        foreach($this->data as $user)
        {
            User::create($user);
        }

        $this->command->info('Proses Penambahan Data User Berhasil');
    }
}
