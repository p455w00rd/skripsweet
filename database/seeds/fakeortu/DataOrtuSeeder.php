<?php

//use Illuminate\Database\Seeder;
use Crockett\CsvSeeder\CsvSeeder;
use Illuminate\Support\Facades\DB;

class DataOrtuSeeder extends CsvSeeder
{
    public function __construct()
    {
        $this->table = 'parent';
        $this->filename = base_path().'/database/seeds/data/fakeUser/mockortu.csv';
//        $this->mapping = [
//            0 => 'id',
//            1 => 'name',
//            2 => 'nik',
//            3 => 'phone',
//            4 => 'address',
//            8 => 'id_user',
//            9 => 'location',
//        ];
    }
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Recommended when importing larger CSVs
        DB::enableQueryLog();

        // Uncomment the below to wipe the table clean before populating
//        DB::table($this->table)->truncate();

        parent::run();
        $this->command->info('Proses Penambahan Data Ortu Berhasil'.$this->filename);
    }
}
