<?php

use Illuminate\Database\Seeder;
//use Illuminate\Database\Eloquent;
//use Eloquent;
use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
//        Eloquent::unguard();
        //disable foreign key check for this connection before running seeders
//        DB::statement('SET FOREIGN_KEY_CHECKS=0;');

        $this->call(RoleTableSeeder::class);
        $this->call(UserTableSeeder::class);
        $this->call(KecamatanSeeder::class);
        $this->call(KelurahanSeeder::class);
        $this->call(UserRoleTableSeeder::class);
        $this->call(PuskesmasTableSeeder::class);
        $this->call(ImunisasiTableSeeder::class);
        $this->call(PuskesKelurahanSeeder::class);
        $this->call(BeratAnakSeeder::class);
        $this->call(UserOrtuSeeder::class);
        $this->call(UserDataSeeder::class);
        $this->call(DataOrtuSeeder::class);
        $this->call(AssigmentTableSeeder::class);
        $this->call(RoleOrtuSeeder::class);
        $this->call(AnakSeederTambakrejo::class);
        $this->call(HistoryAnakTambakrejo::class);
        $this->call(AnakSeederMojo::class);
        $this->call(HistoryAnakMojo::class);
        $this->call(AnakSeederPegirian::class);
        $this->call(HistoryAnakPegirian::class);
        $this->call(AnakSeederSidotopoWetan::class);
        $this->call(HistoryAnakSidotopoWetan::class);

//        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
    }
}
