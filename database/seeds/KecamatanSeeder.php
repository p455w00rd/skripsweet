<?php

use Illuminate\Database\Seeder;
use Flynsarmy\CsvSeeder\CsvSeeder;
use Illuminate\Support\Facades\DB;

class KecamatanSeeder extends CsvSeeder
{
    public function __construct()
    {
        $this->table = 'kecamatan2';
        $this->filename = base_path().'/database/seeds/data/kecamatan.csv';
    }
    /**
     * Run the database seeds.
     *
     * @return void
     */


    public function run()
    {
        // Recommended when importing larger CSVs
        DB::disableQueryLog();

        // Uncomment the below to wipe the table clean before populating
//        DB::table($this->table)->truncate();

        parent::run();
        $this->command->info('Proses Penambahan Data Kecamatan Berhasil');
    }
}
