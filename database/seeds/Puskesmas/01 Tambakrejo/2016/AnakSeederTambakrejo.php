<?php

use Illuminate\Database\Seeder;
use Crockett\CsvSeeder\CsvSeeder;
use Illuminate\Support\Facades\DB;

class AnakSeederTambakrejo extends CsvSeeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Recommended when importing larger CSVs
        DB::disableQueryLog();

        // Uncomment the below to wipe the table clean before populating
//        DB::table($this->table)->truncate();

        $this->filename = base_path().'/database/seeds/data/Puskesmas/01 tambakrejo/2016/bnba.csv';
        $this->table = 'child';
        parent::run();
        $this->filename = base_path().'/database/seeds/data/Puskesmas/01 tambakrejo/2016/no regis.csv';
        $this->table = 'regis_child';
        parent::run();
    }
}
