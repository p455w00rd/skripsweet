<?php

use Illuminate\Database\Seeder;
use Crockett\CsvSeeder\CsvSeeder;
use Illuminate\Support\Facades\DB;

class AnakSeederSidotopoWetan extends CsvSeeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::enableQueryLog();
        $this->filename = base_path().'/database/seeds/data/Puskesmas/04 sidotopo wetan/2016/bnba.csv';
        $this->table = 'child';
        parent::run();
        $this->filename = base_path().'/database/seeds/data/Puskesmas/04 sidotopo wetan/2016/no regis.csv';
        $this->table = 'regis_child';
        parent::run();
    }
}
