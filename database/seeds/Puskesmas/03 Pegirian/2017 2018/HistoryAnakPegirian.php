<?php

use Illuminate\Database\Seeder;
use Crockett\CsvSeeder\CsvSeeder;
use Illuminate\Support\Facades\DB;

class HistoryAnakPegirian extends CsvSeeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::enableQueryLog();

        $this->filename = base_path().'/database/seeds/data/Puskesmas/03 pegirian/2017 2018/imunisasi/all imun.csv';
        $this->table = 'history_imunisasi';
        parent::run();

        $this->filename = base_path().'/database/seeds/data/Puskesmas/03 pegirian/2017 2018/imunisasi/idl.csv';
        $this->table = 'idl';
        parent::run();
    }
}
