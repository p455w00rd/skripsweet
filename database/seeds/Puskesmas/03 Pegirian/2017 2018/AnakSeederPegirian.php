<?php

use Illuminate\Database\Seeder;
use Crockett\CsvSeeder\CsvSeeder;
use Illuminate\Support\Facades\DB;

class AnakSeederPegirian extends CsvSeeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::enableQueryLog();
        $this->filename = base_path().'/database/seeds/data/Puskesmas/03 pegirian/2017 2018/bnba.csv';
        $this->table = 'child';
        parent::run();
        $this->filename = base_path().'/database/seeds/data/Puskesmas/03 pegirian/2017 2018/no regis.csv';
        $this->table = 'regis_child';
        parent::run();
    }
}
