<?php

use Illuminate\Database\Seeder;
use App\PuskesKelurahan;

class PuskesKelurahanSeeder extends Seeder
{
    protected $data = [
        //tambakrejo
        [
            'kelurahan_id'        => '104',
            'puskesmas_id'           => '1',
        ],
        [
            'kelurahan_id'        => '105',
            'puskesmas_id'           => '1',
        ],
        [
            'kelurahan_id'        => '153',
            'puskesmas_id'           => '1',
        ],
        //mojo
        [
            'kelurahan_id'        => '42',
            'puskesmas_id'           => '2',
        ],
        [
            'kelurahan_id'        => '41',
            'puskesmas_id'           => '2',
        ],
        [
            'kelurahan_id'        => '43',
            'puskesmas_id'           => '2',
        ],
        //pegirian
        [
            'kelurahan_id'        => '109',
            'puskesmas_id'           => '3',
        ],
        [
            'kelurahan_id'        => '110',
            'puskesmas_id'           => '3',
        ],
        [
            'kelurahan_id'        => '111',
            'puskesmas_id'           => '3',
        ],
        //sidotopo wetan
        [
            'kelurahan_id'        => '152',
            'puskesmas_id'           => '4',
        ],
    ];
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach($this->data as $user)
        {
            PuskesKelurahan::create($user);
        }

        $this->command->info('Proses Penambahan Data Puskesmas pivot Kelurahan Berhasil');
    }
}
