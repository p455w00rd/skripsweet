<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PuskesKelurahanTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('puskes_kelurahan', function (Blueprint $table) {
            $table->integer('id')->nullable();
            $table->integer('kelurahan_id')->unsigned()->index();
            $table->integer('puskesmas_id')->unsigned()->index();

//            $table->foreign('puskesmas_id')->references('id')->on('puskesmas')->onDelete('cascade');
            $table->foreign('kelurahan_id')->references('id')->on('kecamatan')->onDelete('cascade');
            $table->foreign('puskesmas_id')->references('id')->on('puskesmas')->onDelete('cascade');

            $table->primary(['kelurahan_id', 'puskesmas_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
