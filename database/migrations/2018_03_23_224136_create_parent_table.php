<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateParentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('parent', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->bigInteger('nik')->nullable();
            $table->string('phone')->nullable();
            $table->string('address')->nullable();
            $table->integer('id_user')->unsigned()->index();
            $table->integer('location')->nullable()->unsigned()->index();
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('location')->references('id')->on('kecamatan')->onDelete('cascade');
            $table->foreign('id_user')->references('id')->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('parent');
    }
}
