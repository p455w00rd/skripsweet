<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TargetImunisasiTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('target_imunisasi', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('target');
            $table->integer('id_imunisasi')->unsigned()->index();
            $table->integer('tahun');
            $table->timestamps();

            $table->foreign('id_imunisasi')->references('id')->on('imunisasi')->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('target_imunisasi');
    }
}
