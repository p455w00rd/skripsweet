<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RescheduleImunTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reschedule_imun', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_anak')->unsigned()->index();
            $table->date('tanggal_kembali');
            $table->boolean('status')->default(0);
            $table->integer('id_puskesmas')->unsigned()->index();
            $table->timestamps();

            $table->foreign('id_anak')->references('id')->on('child')->onDelete('cascade');
            $table->foreign('id_puskesmas')->references('id')->on('puskesmas')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reschedule_imun');
    }
}
