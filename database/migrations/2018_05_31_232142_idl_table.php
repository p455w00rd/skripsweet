<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class IdlTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('idl', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_anak')->unsigned()->index();
            $table->integer('location')->unsigned()->index();
            $table->integer('id_puskesmas')->unsigned()->index();
            $table->string('gender');
            $table->timestamps();

            $table->foreign('id_anak')->references('id')->on('child')->onDelete('cascade');
            $table->foreign('location')->references('id')->on('kecamatan')->onDelete('cascade');
            $table->foreign('id_puskesmas')->references('id')->on('puskesmas')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('idl');
    }
}
