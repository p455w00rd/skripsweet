<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RepresentationSurvivingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('representation_surviving', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('tahun');
            $table->integer('PL');
            $table->integer('PP');
            $table->integer('SL')->nullable();
            $table->integer('SP')->nullable();
            $table->integer('kelurahan')->unsigned()->index();
            $table->integer('kecamatan')->unsigned()->index();
            $table->boolean('exp')->default(0);
//            $table->integer('id_puskesmas')->unsigned()->index();
            $table->timestamps();

//            $table->foreign('id_puskesmas')->references('id')->on('puskesmas')->onDelete('cascade');
            $table->foreign('kecamatan')->references('id')->on('kecamatan2')->onDelete('cascade');
            $table->foreign('kelurahan')->references('id')->on('kecamatan')->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
