<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePointPuskesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('puskes_places', function(Blueprint $table)
        {
            $table->increments('id');
            $table->integer('id_puskesmas')->unsigned()->index();
            $table->point('location')->nullable();
            $table->timestamps();

            $table->foreign('id_puskesmas')->references('id')->on('puskesmas')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('puskes_places');
    }
}
