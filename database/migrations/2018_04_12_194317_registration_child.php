<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RegistrationChild extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('regis_child', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_child')->unsigned()->index();
            $table->integer('id_puskes')->unsigned()->index();


            $table->string('no_regis');
            $table->timestamps();

            $table->foreign('id_child')->references('id')->on('child')->onDelete('cascade');
            $table->foreign('id_puskes')->references('id')->on('puskesmas')->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('regis_child');
    }
}
