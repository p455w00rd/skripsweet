<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateChildTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('child', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_parent')->unsigned()->index();
            $table->string('name');
            $table->bigInteger('nik')->nullable();
            $table->integer('gender');
            $table->string('pob');
            $table->date('dob');
            $table->string('address');
            $table->text('keterangan')->nullable();
            $table->boolean('surviving')->default(1);
            $table->integer('location')->unsigned()->index();
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('id_parent')->references('id')->on('parent')->onDelete('cascade');
            $table->foreign('location')->references('id')->on('kecamatan')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('child');
    }
}
