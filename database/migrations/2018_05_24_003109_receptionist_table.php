<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ReceptionistTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('receptionist', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('queue');
            $table->integer('id_anak')->unsigned()->index();
            $table->integer('id_puskes')->unsigned()->index();
            $table->boolean('status')->default('0');
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('id_anak')->references('id')->on('child')->onDelete('cascade');
            $table->foreign('id_puskes')->references('id')->on('puskesmas')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('receptionist');
    }
}
