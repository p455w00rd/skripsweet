<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePointParentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('point_parent', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_parent')->unsigned()->index();
            $table->point('location')->nullable();
            $table->timestamps();

            $table->foreign('id_parent')->references('id')->on('parent')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('point_parent');
    }
}
