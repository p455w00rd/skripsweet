<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLogImunization extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('log_imunization', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_puskesmas')->unsigned()->index();
            $table->integer('id_peg')->unsigned()->index();
            $table->integer('id_imunisasi')->unsigned()->index();
            $table->integer('id_anak')->unsigned()->index();
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('id_puskesmas')->references('id')->on('puskesmas')->onDelete('cascade');
            $table->foreign('id_imunisasi')->references('id')->on('imunisasi')->onDelete('cascade');
            $table->foreign('id_peg')->references('id')->on('user_data')->onDelete('cascade');
            $table->foreign('id_anak')->references('id')->on('child')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('log_imunization');
    }
}
