@extends('layouts.layout.app')

@section('content')

    <div class="page-content">
        <!-- BEGIN PAGE HEAD-->
        <div class="page-head">
            <!-- BEGIN PAGE TITLE -->
            <div class="page-title">
                <h1>Dashboard
                </h1>
            </div>
            <!-- END PAGE TITLE -->

        </div>
        <!-- END PAGE HEAD-->

        <!-- BEGIN PAGE BASE CONTENT -->
        <div class="row">
            <div class="col-md-8">
                <!-- BEGIN VALIDATION STATES-->
                <div class="portlet light portlet-fit portlet-form bordered">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class=" icon-layers font-green"></i>
                            <span class="caption-subject font-dark bold uppercase">Create Pegawai Puskesmas</span>
                        </div>
                    </div>
                    <div class="portlet-body">
                        <!-- BEGIN FORM-->
                        <form action="{{route('dinkes.user.post')}}" class="form-horizontal" method="post">
                            <div class="form-body">

                                @if(count( $errors ) > 0)
                                    @foreach ($errors->all() as $error)
                                        <div>{{ $error }}</div>
                                    @endforeach
                                @endif
                                <h4 class="form-title-h4">Akun</h4>
                                <div class="form-group form-md-line-input {{ $errors->has('username') ? 'has-error' : '' }}">
                                    <label class="col-md-3 control-label" for="form_control_1">Username
                                        <span class="required">*</span>
                                    </label>
                                    <div class="col-md-9">

                                        <input type="text" class="form-control " placeholder="" name="username" value="{{ old('username') }}">
                                        <div class="form-control-focus"> </div>
                                        <span class="help-block">{{ $errors->has('username') ? $error : 'masukkan Username' }}</span>
                                    </div>
                                </div>
                                <div class="form-group form-md-line-input {{ $errors->has('email') ? 'has-error' : '' }}">
                                    <label class="col-md-3 control-label" for="form_control_1">Email
                                        <span class="required">*</span>
                                    </label>
                                    <div class="col-md-9">
                                        <input type="email" class="form-control" placeholder="" name="email" value="{{ old('email') }}">
                                        <div class="form-control-focus"> </div>
                                        <span class="help-block">{{ $errors->has('email') ? $error : 'Masukkan Email' }}</span>
                                    </div>
                                </div>
                                <div class="form-group form-md-line-input {{ $errors->has('role') ? 'has-error' : '' }}">
                                    <label class="col-md-3 control-label" for="form_control_1">Hak Akses
                                        <span class="required">*</span>
                                    </label>
                                    <div class="col-md-9">
                                        <select name="role" class="form-control" id="">
                                            <option {{ old('role') == null ? 'selected' : '' }} value="">Pilih Hak Akses</option>
                                            <option {{ old('role') == 5 ? 'selected' : '' }} value="5">Pegawai Dinkes</option>
                                            <option {{ old('role') == 1 ? 'selected' : '' }} value="1">Admin Dinkes</option>
                                        </select>
                                        <div class="form-control-focus"> </div>
                                        <span class="help-block">{{ $errors->has('role') ? $error : 'Pilih Hak Akses' }}</span>
                                    </div>
                                </div>
                                <div class="form-group form-md-line-input {{ $errors->has('password_confirm') ? 'has-error' : '' }}">
                                    <label class="col-md-3 control-label" for="form_control_1">Password
                                        <span class="required">*</span>
                                    </label>
                                    <div class="col-md-9">
                                        <input type="password" class="form-control" placeholder="" name="password" value="">
                                        <div class="form-control-focus"> </div>
                                        <span class="help-block">{{ $errors->has('password_confirmation') ? $error : 'Masukkan Password' }}</span>
                                    </div>
                                </div>
                                <div class="form-group form-md-line-input {{ $errors->has('password_confirm') ? 'has-error' : '' }}">
                                    <label class="col-md-3 control-label" for="form_control_1">Ulangi Password
                                        <span class="required">*</span>
                                    </label>
                                    <div class="col-md-9">
                                        <input type="password" class="form-control" placeholder="" name="password_confirm" value="">
                                        <div class="form-control-focus"> </div>
                                        <span class="help-block">{{ $errors->has('password_confirm') ? $error : 'Masukkan kembali Password' }}</span>
                                    </div>
                                </div>
                                <hr>
                                <h4 class="form-title-h4-2">Data Pegawai</h4>
                                <div class="form-group form-md-line-input {{ $errors->has('nip') ? 'has-error' : '' }}">
                                    <label class="col-md-3 control-label" for="form_control_1">nip
                                        <span class="required">*</span>
                                    </label>
                                    <div class="col-md-9">
                                        <input type="text" class="form-control" name="nip" placeholder="" value="{{ old('nip') }}">
                                        <div class="form-control-focus"> </div>
                                        <span class="help-block">{{ $errors->has('nip') ? $error : 'Masukkan nip Orang Tua' }}</span>
                                    </div>
                                </div>
                                <div class="form-group form-md-line-input {{ $errors->has('name') ? 'has-error' : '' }}">
                                    <label class="col-md-3 control-label" for="form_control_1">Nama
                                        <span class="required">*</span>
                                    </label>
                                    <div class="col-md-9">
                                        <input type="text" class="form-control" name="name" placeholder="" value="{{ old('name') }}">
                                        <div class="form-control-focus"> </div>
                                        <span class="help-block">{{ $errors->has('name') ? $error : 'Masukkan Nama Orang Tua' }}</span>
                                    </div>
                                </div>
                                <div class="form-group form-md-line-input {{ $errors->has('phone') ? 'has-error' : '' }}">
                                    <label class="col-md-3 control-label" for="form_control_1">telephone
                                        <span class="required">*</span>
                                    </label>
                                    <div class="col-md-9">
                                        <input class="form-control" name="phone" value="{{old('phone')}}"/>
                                        <div class="form-control-focus"> </div>
                                        <span class="help-block">{{ $errors->has('phone') ? $error : 'Masukkan nomor Telephone Orang Tua' }}</span>
                                    </div>
                                </div>

                                {{ csrf_field() }}
                            </div>
                            <div class="form-actions">
                                <div class="row">
                                    <div class="col-md-offset-3 col-md-9">
                                        <button type="submit" class="btn green">Tambah</button>
                                        <a href="" type="reset" class="btn default">Batal</a>
                                    </div>
                                </div>
                            </div>
                        </form>
                        <!-- END FORM-->
                    </div>
                </div>
                <!-- END VALIDATION STATES-->
            </div>
        </div>
        <!-- END PAGE BASE CONTENT -->
    </div>
@endsection
@section('javascript')
    @parent
    <script src="{{asset('assets/global/plugins/bootstrap-select/js/bootstrap-select.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/pages/scripts/components-bootstrap-select.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/global/plugins/moment.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/global/plugins/bootstrap-daterangepicker/daterangepicker.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js')}}" type="text/javascript"></script>

    <script>
        $('.date-picker').datepicker({
            rtl: App.isRTL(),
            orientation: "right",
            autoclose: true,
            currentText: "Today:",
            format: "dd-MM-yyyy"
        });
    </script>
@endsection

@section('stylesheet')
    @parent
    <link href="{{asset('assets/global/plugins/bootstrap-select/css/bootstrap-select.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('assets/global/plugins/bootstrap-daterangepicker/daterangepicker.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css')}}" rel="stylesheet" type="text/css" />
@endsection