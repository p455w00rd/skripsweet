@extends('layouts.layout.app')

@section('content')

    <div class="page-content">
        <!-- BEGIN PAGE HEAD-->
        <div class="page-head">
            <!-- BEGIN PAGE TITLE -->
            <div class="page-title">
                <h1>Super Admin
                </h1>
            </div>
            <!-- END PAGE TITLE -->

        </div>
        <!-- END PAGE HEAD-->

        <!-- BEGIN PAGE BASE CONTENT -->
        <div class="row">
            <div class="col-md-8">
                <!-- BEGIN VALIDATION STATES-->
                <div class="portlet light portlet-fit portlet-form bordered">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class=" icon-layers font-green"></i>
                            <span class="caption-subject font-dark bold uppercase">Tambah Target Imunisasi</span>
                        </div>
                    </div>
                    <div class="portlet-body">
                        <!-- BEGIN FORM-->
                        <form action="{{route('imunisasi.add.target.dinkes',$imun->id)}}" class="form-horizontal" method="post">
                            <div class="form-body">

                                @if(count( $errors ) > 0)
                                    @foreach ($errors->all() as $error)
                                        <div>{{ $error }}</div>
                                    @endforeach
                                @endif

                                <h4 class="form-title-h4-2"> Data Imunisasi</h4>

                                <div class="form-group form-md-line-input {{ $errors->has('name') ? 'has-error' : '' }}">
                                    <label class="col-md-3 control-label" for="form_control_1">Nama Imunisasi
                                        <span class="required">*</span>
                                    </label>
                                    <div class="col-md-9">
                                        <input type="text" class="form-control" disabled name="name" placeholder="" value="{{$imun->name}}">
                                        <div class="form-control-focus"> </div>
                                        <span class="help-block">{{ $errors->has('name') ? $error : 'Masukkan Nama Imunisasi' }}</span>
                                    </div>
                                </div>
                                <div class="form-group form-md-line-input {{ $errors->has('phone') ? 'has-error' : '' }}">
                                    <label class="col-md-3 control-label" for="form_control_1"> Target
                                        <span class="required">*</span>
                                    </label>
                                    <div class="col-md-9">
                                        <input type="text" class="form-control" name="target" placeholder="" value="{{$imun->target()->get()->last()==null?'':$imun->target()->get()->last()->target}}">
                                        <div class="form-control-focus"> </div>
                                        <span class="help-block">{{ $errors->has('phone') ? $error : 'Dalam Persen %' }}</span>
                                    </div>
                                </div>
                                <div class="form-group form-md-line-input {{ $errors->has('address') ? 'has-error' : '' }}">
                                    <label class="col-md-3 control-label" for="form_control_1">Tahun
                                        <span class="required">*</span>
                                    </label>
                                    <div class="col-md-9">
                                        <input type="text" class="form-control" name="tahun" placeholder="" value="{{$imun->target()->get()->last()==null?'':$imun->target()->get()->last()->tahun}}">
                                        <div class="form-control-focus"> </div>
                                        <span class="help-block">{{ $errors->has('address') ? $error : 'Masukkan Alamat Pusekesmas' }}</span>
                                    </div>
                                </div>

                                {{ csrf_field() }}
                            </div>
                            <div class="form-actions">
                                <div class="row">
                                    <div class="col-md-offset-3 col-md-9">
                                        <button type="submit" class="btn green">add</button>
                                        <button type="button" onclick="window.history.back(); return false;" class="btn default">Batal</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                        <!-- END FORM-->
                    </div>
                </div>
                <!-- END VALIDATION STATES-->
            </div>
        </div>
        <!-- END PAGE BASE CONTENT -->
    </div>
@endsection
@section('javascript')
    @parent

@endsection

@section('stylesheet')
    @parent

@endsection