@extends('layouts.layout.app')

@section('content')

    <div class="page-content">
        <!-- BEGIN PAGE HEAD-->
        <div class="page-head">
            <!-- BEGIN PAGE TITLE -->
            <div class="page-title">
                <h1>Super Admin
                </h1>
            </div>
            <!-- END PAGE TITLE -->

        </div>
        <!-- END PAGE HEAD-->

        <!-- BEGIN PAGE BASE CONTENT -->
        <div class="row">
            <div class="col-md-8">
                <!-- BEGIN VALIDATION STATES-->
                <div class="portlet light portlet-fit portlet-form bordered">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class=" icon-layers font-green"></i>
                            <span class="caption-subject font-dark bold uppercase">Edit Imunisasi</span>
                        </div>
                    </div>
                    <div class="portlet-body">
                        <!-- BEGIN FORM-->
                        <form action="" class="form-horizontal" method="post">
                            <div class="form-body">

                                <h4 class="form-title-h4-2"> Data Imunisasi</h4>

                                <div class="form-group form-md-line-input {{ $errors->has('name') ? 'has-error' : '' }}">
                                    <label class="col-md-3 control-label" for="form_control_1">Nama Imunisasi
                                        <span class="required">*</span>
                                    </label>
                                    <div class="col-md-9">
                                        <input type="text" readonly class="form-control" name="name" placeholder="" value="{{$imun->name}}">
                                        <div class="form-control-focus"> </div>
                                        <span class="help-block">{{ $errors->has('name') ? $error : 'Masukkan Nama puskesmas' }}</span>
                                    </div>
                                </div>
                            </div>
                            <div class="form-actions">
                                <div class="row">
                                    <div class="col-md-12 text-center">
                                        <a href="{{route('imunisasi.edit.dinkes',$imun->id)}}" class="btn yellow m-icon m-icon-only ">
                                            <i class="icon-pencil"> Edit</i>
                                        </a>
                                        <a href="{{route('imunisasi.add.target.dinkes',$imun->id)}}" class="btn green m-icon m-icon-only ">
                                            <i class="fa fa-plus"> Tambah Target</i>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </form>
                        <!-- END FORM-->



                        <table class="table table-striped table-bordered table-hover">
                            <thead>
                            <tr>
                                <th>Tahun</th>
                                <th>Target</th>
                                <th></th>
                            </tr>

                            </thead>
                            <tbody>
                            @forelse($imun->target()->orderBy('created_at','desc')->get() as $target)
                            <tr>

                                <td>{{$target->tahun}}</td>
                                <td>{{$target->target}}%</td>

                                <td>
                                    <button type="button" class="icon-pencil btn yellow m-icon m-icon-only" data-toggle="modal" data-target="#myModal{{$target->id}}">
                                    </button>
                                </td>
                            </tr>
                            <div class="modal fade" id="myModal{{$target->id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                            <h4 class="modal-title" id="myModalLabel">Edit Target Imunisasi {{$imun->name}}</h4>
                                        </div>
                                        <form action="{{route('imunisasi.edit.target.dinkes',$imun->id)}}" class="form-horizontal" method="post">
                                        <div class="modal-body">
                                            <label class="col-md-3 control-label" for="form_control_1">Tahun
                                            </label>
                                            <div class="col-md-9">
                                                <input type="text" class="form-control" disabled name="name" placeholder="" value="{{$target->tahun}}">
                                                <div class="form-control-focus"> </div>
                                                <span class="help-block"></span>
                                            </div>
                                            <label class="col-md-3 control-label" for="form_control_1">Target
                                                <span class="required">*</span>
                                            </label>
                                            <div class="col-md-9">
                                                <input type="text" class="form-control" name="target" placeholder="" value="{{$target->target}}">
                                                <div class="form-control-focus"> </div>
                                                <span class="help-block">{{ $errors->has('name') ? $error : 'dalam %' }}</span>
                                            </div>
                                        </div>
                                            <input type="text" name="id" hidden value="{{$target->id}}">
                                            {{ csrf_field() }}
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                            <button  type="submit"  class="btn btn-primary" >Simpan</button>
                                        </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                                @empty
                            @endforelse
                            </tbody>
                        </table>
                    </div>
                </div>
                <!-- END VALIDATION STATES-->
            </div>
        </div>
        <!-- END PAGE BASE CONTENT -->
    </div>
@endsection
@section('javascript')
    @parent

@endsection

@section('stylesheet')
    @parent

@endsection