@extends('layouts.layout.app')

@section('content')

    <div class="page-content">
        <!-- BEGIN PAGE HEAD-->
        <div class="page-head">
            <!-- BEGIN PAGE TITLE -->
            <div class="page-title">
                <h1>Super Admin
                </h1>
            </div>
            <!-- END PAGE TITLE -->

        </div>
        <!-- END PAGE HEAD-->

        <!-- BEGIN PAGE BASE CONTENT -->
        <div class="row">
            <div class="col-md-8">
                <!-- BEGIN VALIDATION STATES-->
                <div class="portlet light portlet-fit portlet-form bordered">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class=" icon-layers font-green"></i>
                            <span class="caption-subject font-dark bold uppercase">Create Puskesmas</span>
                        </div>
                    </div>
                    <div class="portlet-body">
                        <!-- BEGIN FORM-->
                        <form action="{{route('puskesmas.dinkes.store')}}" class="form-horizontal" method="post">
                            <div class="form-body">

                                @if(count( $errors ) > 0)
                                    @foreach ($errors->all() as $error)
                                        <div>{{ $error }}</div>
                                    @endforeach
                                @endif

                                <h4 class="form-title-h4-2"> Data Puskesmas</h4>

                                <div class="form-group form-md-line-input {{ $errors->has('name') ? 'has-error' : '' }}">
                                    <label class="col-md-3 control-label" for="form_control_1">Nama Puskesmas
                                        <span class="required">*</span>
                                    </label>
                                    <div class="col-md-9">
                                        <input type="text" class="form-control" name="name" placeholder="" value="{{ old('name') }}">
                                        <div class="form-control-focus"> </div>
                                        <span class="help-block">{{ $errors->has('name') ? $error : 'Masukkan Nama puskesmas' }}</span>
                                    </div>
                                </div>
                                <div class="form-group form-md-line-input {{ $errors->has('phone') ? 'has-error' : '' }}">
                                    <label class="col-md-3 control-label" for="form_control_1"> Nomor Telepon
                                        <span class="required">*</span>
                                    </label>
                                    <div class="col-md-9">
                                        <input type="text" class="form-control" name="phone" placeholder="" value="{{ old('phone') }}">
                                        <div class="form-control-focus"> </div>
                                        <span class="help-block">{{ $errors->has('phone') ? $error : 'Masukkan Telepon Puskesmas' }}</span>
                                    </div>
                                </div>
                                <div class="form-group form-md-line-input {{ $errors->has('address') ? 'has-error' : '' }}">
                                    <label class="col-md-3 control-label" for="form_control_1">Alamat
                                        <span class="required">*</span>
                                    </label>
                                    <div class="col-md-9">
                                        <input type="text" class="form-control" name="address" placeholder="" value="{{ old('address') }}">
                                        <div class="form-control-focus"> </div>
                                        <span class="help-block">{{ $errors->has('address') ? $error : 'Masukkan Alamat Pusekesmas' }}</span>
                                    </div>
                                </div>
                                <div class="form-group form-md-line-input {{ $errors->has('location') ? 'has-error' : '' }}">
                                    <label class="col-md-3 control-label" for="form_control_1">Kecamatan
                                        <span class="required">*</span>
                                    </label>
                                    <div class="col-md-9">
                                        <select class="bs-select form-control" id="kecamatan" name="location" data-live-search="true" data-size="8">
                                            <option value="">Pilih Kecamatan</option>
                                            @forelse($kecamatan as $kecamatan)
                                                <option {{ old('location') == $kecamatan->id? 'selected' : '' }} value="{{$kecamatan->id}}">{{$kecamatan->name}}</option>
                                            @empty
                                                Tidak Ada Data
                                            @endforelse
                                        </select>
                                        <div class="form-control-focus"> </div>
                                        <span class="help-block">{{ $errors->has('location') ? $error : 'Masukkan Kecamatan Puskesmas' }}</span>
                                    </div>
                                </div>
                                    <div class="form-group form-md-line-input {{ $errors->has('location') ? 'has-error' : '' }}">
                                        <label class="col-md-3 control-label" for="form_control_1">Kelurahan
                                            <span class="required">*</span>
                                        </label>
                                        {{--<div class="" id="kelurahan"></div>--}}
                                        <div class="col-md-9">
                                            <select class="select2-multiple form-control" required multiple id="kelurahan" name="kelurahan[]" data-live-search="true" data-size="8">
                                                <option value="">Pilih Kelurahan</option>

                                            </select>
                                            <div class="form-control-focus"> </div>
                                            <span class="help-block">{{ $errors->has('location') ? $error : 'Masukkan Kecamatan Puskesmas' }}</span>
                                        </div>
                                    </div>
                                    <h4 class="form-title-h4-2"> Lokasi Puskesmas</h4>
                                    <hr>
                                    <div id="map" style="height: 80%"></div>
                                    <input type="text" hidden id="lat" name="lat" value="">
                                    <input type="text" hidden id="long" name="long" value="">
                                {{ csrf_field() }}
                            </div>
                            <div class="form-actions">
                                <div class="row">
                                    <div class="col-md-offset-3 col-md-12">
                                        <button type="submit" class="btn green">Tambah</button>
                                        <button type="reset" class="btn default">Batal</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                        <!-- END FORM-->
                    </div>
                </div>
                <!-- END VALIDATION STATES-->
            </div>
        </div>
        <!-- END PAGE BASE CONTENT -->
    </div>
@endsection
@section('javascript')
    @parent
    <script src="{{asset('assets/global/plugins/bootstrap-select/js/bootstrap-select.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/pages/scripts/components-bootstrap-select.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/global/plugins/moment.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/global/plugins/bootstrap-daterangepicker/daterangepicker.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/global/plugins/select2/js/select2.full.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/apps/leaflet/leaflet.js')}}" type="text/javascript"></script>

    <script>
        $('.date-picker').datepicker({
            rtl: App.isRTL(),
            orientation: "right",
            autoclose: true,
            currentText: "Today:",
            format: "dd-MM-yyyy"
        });
        $(document).ready(function() {
            $('.select2-multiple').select2();
        });
    </script>
    <script>
        $('#kecamatan').on('change', function(e){
            console.log(e);
            var state_id = e.target.value;

            $.get('{{ route('find.kelurahan') }}?id_kecamatan=' + state_id, function(data) {
                console.log(data);
                $('#kelurahan').empty();
                $.each(data, function(index,subCatObj){
                    // $('#kelurahan').append(''+subCatObj.name+'');
                    $('#kelurahan').append('<option value="' + subCatObj.id + '">' + subCatObj.name + '</option>');
                });
                $('#kelurahan').selectpicker('refresh');
            });
        });
    </script>
    <script>
        var map = L.map('map',{
            center: [-7.253339, 112.772028],
            zoom: 15
        });

        //map rendering
        L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', {
            attribution: '&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
        }).addTo(map);

        //map input
        map.on('click',
            function(e){
                var coord = e.latlng.toString().split(',');
                var lat = coord[0].split('(');
                var lng = coord[1].split(')');
                console.log("You clicked the map at latitude: " + lat[1] + " and longitude:" + lng[0]);
                var marker = L.marker([lat[1],lng[0]]).addTo(map);
                map.on('click', function () {
                    map.removeLayer(marker);
                });
                document.getElementById("lat").value = lat[1];
                document.getElementById("long").value = lng[0];
            });
    </script>
@endsection

@section('stylesheet')
    @parent
    <link href="{{asset('assets/apps/leaflet/leaflet.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('assets/global/plugins/select2/css/select2.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('assets/global/plugins/select2/css/select2-bootstrap.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('assets/global/plugins/bootstrap-select/css/bootstrap-select.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('assets/global/plugins/bootstrap-daterangepicker/daterangepicker.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css')}}" rel="stylesheet" type="text/css" />
@endsection