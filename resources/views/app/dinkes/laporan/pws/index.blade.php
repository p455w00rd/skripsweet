@extends('layouts.layout.app')

@section('content')

    <div class="page-content">
        <!-- BEGIN PAGE HEAD-->
        <div class="page-head">
            <!-- BEGIN PAGE TITLE -->
            <div class="page-title">
                <h1>Pilih Laporan PWS Imunisasi
                </h1>
            </div>
            <!-- END PAGE TITLE -->

        </div>
        <!-- END PAGE HEAD-->
        <!-- BEGIN PAGE BREADCRUMB -->
        <ul class="page-breadcrumb breadcrumb">
            <li>
                <a href="">Home</a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <span class="active">Laporan PWS Imunisasi</span>
            </li>
        </ul>
        <!-- END PAGE BREADCRUMB -->
        <!-- BEGIN PAGE BASE CONTENT -->
        <div class="row">
            <div class="col-md-12">
                <div class="portlet box">
                    <div class="portlet-body">
                        <div class="tiles">
                            @forelse($imunisasi as $imunisasi)
                                <a target="_blank" href="{{route('laporan.imunisasi.dinkes',$imunisasi->id)}}" class="tile bg-green">
                                    <div class="tile-body">
                                        <i class="fa fa-file-o"></i>
                                    </div>
                                    <div class="tile-object">
                                        <div class="name">
                                            Laporan {{$imunisasi->name}}
                                        </div>
                                        <div class="number">
                                        </div>
                                    </div>
                                </a>
                            @empty
                            @endforelse
                            <a target="_blank" href="{{route('laporan.imunisasi.dinkes.idl')}}" class="tile bg-green">
                                <div class="tile-body">
                                    <i class="fa fa-file-o"></i>
                                </div>
                                <div class="tile-object">
                                    <div class="name">
                                        Imunisasi Lengkap (IDL)
                                    </div>
                                    <div class="number">
                                    </div>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>


            </div>
            <!-- END PAGE BASE CONTENT -->

        </div>
    </div>

@endsection
@section('stylesheet')
    @parent
    <link href="{{asset('assets/global/plugins/select2/css/select2.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('assets/global/plugins/select2/css/select2-bootstrap.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('assets/global/plugins/bootstrap-select/css/bootstrap-select.min.css')}}" rel="stylesheet" type="text/css" />
    {{--    <link href="{{asset('assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css')}}" rel="stylesheet" type="text/css" />--}}
    <link href="{{asset('assets/global/plugins/bootstrap-daterangepicker/daterangepicker.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css')}}" rel="stylesheet" type="text/css" />
    <style>
        .bootstrap-switch-container{
            height: 32px;
            height: 32px;
        }
        .switchh{
            margin-top: 3px;
        }
    </style>

@stop

@section('javascript')
    @parent
    <script src="{{asset('assets/global/plugins/select2/js/select2.full.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/pages/scripts/components-bootstrap-switch.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/global/plugins/moment.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/global/plugins/bootstrap-daterangepicker/daterangepicker.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/global/plugins/bootstrap-select/js/bootstrap-select.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/pages/scripts/components-bootstrap-select.min.js')}}" type="text/javascript"></script>
    <script>
        $(document).ready(function() {
            $('.select2-multiple').select2();
        });
        $(document).ready(function(){
            var i=1;
            $('#add').click(function(){
                i++;
                $('#dynamic_field').append('<tr id="row'+i+'" class="dynamic-added"><td><input type="text" name="name[]" placeholder="Enter your Name" class="form-control name_list" /></td><td><button type="button" name="remove" id="'+i+'" class="btn btn-danger btn_remove">X</button></td></tr>');
            });
            $(document).on('click', '.btn_remove', function(){
                var button_id = $(this).attr("id");
                $('#row'+button_id+'').remove();
            });
        });
        $('.date-picker').datepicker({
            rtl: App.isRTL(),
            orientation: "right",
            autoclose: true,
            currentText: "Today:",
            format: "dd-MM-yyyy"
        });
    </script>

@stop