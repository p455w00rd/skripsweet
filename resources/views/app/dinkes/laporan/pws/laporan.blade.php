<!DOCTYPE html>
<html lang="en">
<head>
    <title>Lporan Imunisasi {{$imunisasi->name}}</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <style>
        thead tr th{
            text-align: center;
            vertical-align: middle !important;
        }
    </style>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
    <h2>Laporan Imunisasi {{$imunisasi->name}}</h2>
    {{--<p>The .table-striped class adds zebra-stripes to a table:</p>--}}
    <form class="form-inline" action=""method="get">

        <div class="clearfix"></div>
        <div class="form-group">
            <label for="pwd">Tahun : </label>
            <input type="text" class="form-control" name="year" value="{{$y}}">
        </div>
        <button type="submit" class="btn btn-info">Rubah</button>
    </form>
    <table class="table table-bordered table-striped">
        <thead>
        <tr>
            <th rowspan="4">No</th>
            <th rowspan="4">Kecamatan</th>
            <th rowspan="4">Puskesmas</th>
            <th colspan="3" rowspan="3">Jumlah Bayi (Surviving Invant)</th>
            <th colspan="6">Bayi Diimunisasi</th>
        </tr>
        <tr>
            <th colspan="6">{{$imunisasi->name}}</th>
        </tr>
        <tr>
            <th colspan="2">L</th>
            <th colspan="2">P</th>
            <th colspan="2">L+P</th>
        </tr>
        <tr>
            <th>L</th>
            <th>P</th>
            <th>L+P</th>
            <th>Jumlah</th>
            <th>%</th>
            <th>Jumlah</th>
            <th>%</th>
            <th>Jumlah</th>
            <th>%</th>
        </tr>

        </thead>
        <tbody>
        @foreach($kecamatan as $kecamatan)
            <?php
                    $kec = $kecamatan->puskesmas->count();
            ?>
            <tr>
                <td rowspan="{{$kec==0?1:$kec+1}}">{{$c++}}</td>
                <td rowspan="{{$kec==0?1:$kec+1}}">{{$kecamatan->name}}</td>
                @if($kec==0)
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                @endif
            </tr>
            @forelse($kecamatan->puskesmas as $puskesmas)
                <?php
                        $his = $history->where('id_puskesmas',$puskesmas->id);
                        $h1 = $his->where('gender',1)->count();
                        $h2 = $his->where('gender',2)->count();
                        $h3 = $his->count();
                ?>
                <tr>
                    <td>{{$puskesmas->name}}</td>
                    <td>{{$l=$puskesmas->kelurahan->sum('proyeksi.SL')}}</td>
                    <td>{{$p=$puskesmas->kelurahan->sum('proyeksi.SP')}}</td>
                    <td>{{$all=$l+$p}}</td>
                    <td>{{$h1}}</td>
                    <td>{{$l != 0 ?$h1/$l*100:0}}%</td>
                    <td>{{$h2}}</td>
                    <td>{{$p != 0 ?$h2/$l*100:0}}%</td>
                    <td>{{$h3}}</td>
                    <td>{{$all != 0 ?$h3/$all*100:0}}%</td>
                </tr>
            @empty
            @endforelse
        @endforeach

        </tbody>
    </table>
</div>

</body>
</html>
