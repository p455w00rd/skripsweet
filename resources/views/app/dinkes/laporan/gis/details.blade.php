@extends('layouts.layout.app')

@section('content')

    <div class="page-content">
        <!-- BEGIN PAGE HEAD-->
    {{--<div class="page-head">--}}
    {{--<!-- BEGIN PAGE TITLE -->--}}
    {{--<div class="page-title">--}}
    {{--<h1>Pemetaan--}}
    {{--</h1>--}}
    {{--</div>--}}
    {{--<!-- END PAGE TITLE -->--}}

    {{--</div>--}}
    <!-- END PAGE HEAD-->

        <!-- BEGIN PAGE BASE CONTENT -->
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN VALIDATION STATES-->
                <div class="portlet light portlet-fit portlet-form bordered">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class=" icon-layers font-green"></i>
                            <span class="caption-subject font-dark bold uppercase">Peta Wilayah Kelurahan {{$kelurahan}} Tahun
                                {{$y}}
                            </span>
                        </div>
                    </div>
                    <div class="portlet-body">
                        <div class="row">
                            <div class="col-md-6">
                                <?php
                                $pro = $pro = $data->pro->first();
                                if (isset($pro->PP)){
                                    $pro = $data->pro->first()->PP + $data->pro->first()->PL;
                                }else{
                                    $pro = 0;
                                }
                                $idl = $data->idl->count();
                                if ($pro != 0){
                                    $ti = ($idl/$pro)*100;
                                }else{
                                    $ti=0;
                                }
                                ?>
                                <div id="map" style="height: 80%"></div>
                                <h4>Target Idl >= 90%</h4>
                                <h4>Proyeksi Data Anak=  {{$pro}}</h4>
                                <h4>Total Anak IDL {{$y}} = {{$idl}}</h4>
                                @if($pro == 0)
                                    <h4>Prosentase = <span style="background: red">Data Belum disetting</span></h4>
                                @elseif($ti>=90)
                                    <h4>Prosentase = <span style="background: green">{{$ti}}%</span></h4>
                                @elseif($ti>=80)
                                    <h4>Prosentase = <span style="background: yellow">{{$ti}}%</span></h4>
                                @else
                                    <h4>Prosentase = <span style="background: red">{{$ti}}%</span></h4>
                                @endif
                            </div>
                            <div class="col-md-6">
                                <div class="col-md-12">
                                    <div class="table-responsive">
                                        <table class="table table-bordered table-hover">
                                            <thead>
                                            <tr>
                                                <th> Imunisasi </th>
                                                <th> Target </th>
                                                <th> proyeksi </th>
                                                <th> capaian (%)</th>
                                                <th> capaian (angka)</th>
                                                <th> Memenuhi </th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @forelse($imunisasi as $imun)
                                                <?php
                                                $a = $target->where('id_imunisasi',$imun->id)->first();
                                                $capaian = $data->imunisasi->where('id_imunisasi',$imun->id)->count();
                                                $t = $imun->target->first();
                                                if (isset($t->target)){
                                                    $t=$t->target;
                                                }else{
                                                    $t=0;
                                                }
                                                if ($pro != 0){
                                                    $pers = ($capaian/$pro)*100;
                                                }else{
                                                    $pers=0;
                                                }
                                                ?>
                                                <tr>
                                                    <td> {{$imun->name}} </td>
                                                    <td> {{$a == null? '-':$a->target.'%'}} </td>
                                                    <td> {{$pro==0?'-':$pro}}</td>
                                                    <td> {{$pers}}% </td>
                                                    <td> {{$capaian}}</td>
                                                    @if($pro == 0 || $t ==0)
                                                        <td style="background: red">Data Belum disetting</td>
                                                    @elseif($pers>=$t)
                                                        <td style="background: green">memenuhi</td>
                                                    @else
                                                        <td style="background: red">Tidak Memenuhi</td>
                                                    @endif
                                                    {{--                                                    <td> {{$pers>=$t?'memenuhi':'tidak memenuhi'}} </td>--}}
                                                </tr>
                                            @empty
                                            @endforelse
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- END VALIDATION STATES-->
            </div>
        </div>
        <!-- END PAGE BASE CONTENT -->
    </div>
@endsection
@section('javascript')
    @parent

    <script src="{{asset('assets/apps/leaflet/leaflet.js')}}" type="text/javascript"></script>
    <script>

        // initialize the map
        var map = L.map('map',{
            center: [-7.257207,112.752014],
            zoom: 15
        });

        L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', {
            attribution: '&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
        }).addTo(map);

        var info = L.control();

        info.onAdd = function (map) {
            this._div = L.DomUtil.create('div', 'info'); // create a div with a class "info"
            this.update();
            return this._div;
        };

        // method that we will use to update the control based on feature properties passed
        info.update = function (props) {
            this._div.innerHTML = '<h4>Peta Wilayah Kelurahan {{$kelurahan}}</h4>' +  (props ?
                'Kelurahan <b>' + props.Name + '</b><br />'
                : 'Hover over a state');
        };

        info.addTo(map);
        function highlightFeature(e) {
            var layer = e.target;
            info.update(layer.feature.properties);
            layer.setStyle({
                weight: 5,
                color: '#94A0B2 ',
                dashArray: '',
                fillOpacity: 0.7
            });

            if (!L.Browser.ie && !L.Browser.opera && !L.Browser.edge) {
                layer.bringToFront();
            }
        }
        function resetHighlight(e) {
            geojson.resetStyle(e.target);
            info.update();
        }
        var geojson;

        function onEachFeature(feature, layer) {
            layer.on({
                mouseover: highlightFeature,
                mouseout: resetHighlight,
                click: zoomToFeature
            });
            if (feature.properties && feature.properties.Name) {
                layer.bindPopup("Kelurahan "+feature.properties.Name);
            }
        }
        function zoomToFeature(e) {
            map.fitBounds(e.target.getBounds());
        }
        // ... our listeners
        {{--geojson = L.geoJson("http://geojson.kojyouproject.com/?kelurahan={{$kel}}");--}}

        // load GeoJSON from an external file
        $.getJSON("http://geojson.kojyouproject.com/?kelurahan={{$kelurahan}}",function(data){
            // add GeoJSON layer to the map once the file is loaded
            geojson = L.geoJson(data,{
                style: function(feature) {
                    switch (feature.properties.Name) {
                        case '{{$kelurahan}}': return {
                            {{--                                color: "{{$cd >=90? 'green':'red'}}",--}}
                            fillColor: "{{$ti >=90? 'green':'red'}}",
                            weight: 2,
                            opacity: 1,
                            color: 'white',
                            dashArray: '3',
                            fillOpacity: 0.7,
                            popupContent : 'This is where the Rockies play!'

                        };
                    }
                },
                onEachFeature: onEachFeature
            }).addTo(map);
        });
        var legend = L.control({position: 'bottomright'});

        legend.onAdd = function (map) {

            var div = L.DomUtil.create('div', 'info legend');

            div.innerHTML +=
                '<i style="background:' + 'green' + '"></i> ' +
                'Memenuhi Target Imunisasi'+'<br>';
            div.innerHTML +=
                '<i style="background:' + 'yellow' + '"></i> ' +
                'Belum Memenuhi Target imunisasi'+'<br>';
            div.innerHTML +=
                '<i style="background:' + 'red' + '"></i> ' +
                'Belum Memenuhi Target IDL'+'<br>';

            return div;
        };

        legend.addTo(map);



    </script>


@endsection

@section('stylesheet')
    @parent
    <link href="{{asset('assets/apps/leaflet/leaflet.css')}}" rel="stylesheet" type="text/css" />
    <style>
        .info {
            padding: 6px 8px;
            font: 14px/16px Arial, Helvetica, sans-serif;
            background: white;
            background: rgba(255,255,255,0.8);
            box-shadow: 0 0 15px rgba(0,0,0,0.2);
            border-radius: 5px;
        }
        .info h4 {
            margin: 0 0 5px;
            color: #777;
        }
        .legend {
            line-height: 18px;
            color: #555;
        }
        .legend i {
            width: 18px;
            height: 18px;
            float: left;
            margin-right: 8px;
            opacity: 0.7;
        }
    </style>

@endsection
{{--@forelse($kel->imunisasi as $imunisasi)--}}
{{--{{$imunisasi->id}}--}}
{{--@empty--}}
{{--@endforelse--}}
