@extends('layouts.layout.app')

@section('content')

    <div class="page-content">
        <!-- BEGIN PAGE HEAD-->
        <div class="page-head">
            <!-- BEGIN PAGE TITLE -->
            <div class="page-title">
                <h1>Pemetaan
                </h1>
            </div>
            <!-- END PAGE TITLE -->

        </div>
        <!-- END PAGE HEAD-->

        <!-- BEGIN PAGE BASE CONTENT -->
        <div class="row">
            <div class="col-md-8">
                <!-- BEGIN VALIDATION STATES-->
                <div class="portlet light portlet-fit portlet-form bordered">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class=" icon-layers font-green"></i>
                            <span class="caption-subject font-dark bold uppercase">Proses Proyeksi tahun {{\Carbon\Carbon::now()->year}}</span>
                        </div>
                    </div>
                    <div class="portlet-body">
                        <!-- BEGIN FORM-->
                        <form action="{{route('post.proyeksi')}}" class="form-horizontal" method="post">
                            <div class="form-body">
                                <h4 class="form-title-h4">Proses Proyeksi Berdasarkan Data dari tahun {{$proyeksi2->tahun}} / {{$proyeksi1->tahun}}</h4>
                                <hr>
                                <h5 class="form-title-h4">Data Proyeksi Tahun {{$proyeksi1->tahun}}</h5>
                                <div class="form-group form-md-line-input">
                                    <label class="col-md-3 control-label" for="form_control_1">Proyeksi Anak Laki-laki Tahun {{$proyeksi1->tahun}}
                                    </label>
                                    <div class="col-md-9">
                                        <input type="text" class="form-control" disabled placeholder="" value="{{ $proyeksi1->PL }}">
                                    </div>
                                </div>
                                <div class="form-group form-md-line-input">
                                    <label class="col-md-3 control-label" for="form_control_1">Proyeksi Anak Perempuan Tahun {{$proyeksi1->tahun}}
                                    </label>
                                    <div class="col-md-9">
                                        <input type="text" class="form-control " disabled placeholder="" value="{{ $proyeksi1->PP }}">
                                    </div>
                                </div>
                                <hr>
                                <h5 class="form-title-h4">Data Proyeksi Tahun {{$proyeksi2->tahun}}</h5>
                                <div class="form-group form-md-line-input">
                                    <label class="col-md-3 control-label" for="form_control_1">Proyeksi Anak Laki-laki Tahun {{$proyeksi2->tahun}}
                                    </label>
                                    <div class="col-md-9">
                                        <input type="text" class="form-control " disabled placeholder="" value="{{ $proyeksi2->PL }}">
                                    </div>
                                </div>
                                <div class="form-group form-md-line-input">
                                    <label class="col-md-3 control-label" for="form_control_1">Proyeksi Anak Perempuan Tahun {{$proyeksi2->tahun}}
                                    </label>
                                    <div class="col-md-9">
                                        <input type="text" class="form-control " placeholder="" disabled value="{{ $proyeksi2->PP }}">
                                    </div>
                                </div>
                                <hr>
                                <h5 class="form-title-h4">Penghitungan Hasil R</h5>
                                <div class="form-group form-md-line-input ">
                                    <label class="col-md-3 control-label" for="form_control_1">Penghitungan R Untuk Laki-laki
                                    </label>
                                    <div class="col-md-9">
                                        <input type="text" class="form-control" disabled placeholder="" value="{{ $RL }}">
                                    </div>
                                </div>
                                <div class="form-group form-md-line-input ">
                                    <label class="col-md-3 control-label" for="form_control_1">Penghitungan R Untuk Perempuan
                                    </label>
                                    <div class="col-md-9">
                                        <input type="text" class="form-control" placeholder="" disabled value="{{ $RP }}">
                                    </div>
                                </div>
                                <hr>
                                <h5 class="form-title-h4">Hasil Penghitungan Proyeksi</h5>
                                <div class="form-group form-md-line-input ">
                                    <label class="col-md-3 control-label" for="form_control_1">Penghitungan Proyeksi Untuk Laki-laki
                                    </label>
                                    <div class="col-md-9">
                                        <input type="text" class="form-control" placeholder="" name="PL" value="{{ $PL }}">
                                    </div>
                                </div>
                                <div class="form-group form-md-line-input ">
                                    <label class="col-md-3 control-label" for="form_control_1">Penghitungan ProyeksiUntuk Perempuan
                                    </label>
                                    <div class="col-md-9">
                                        <input type="text" class="form-control" placeholder="" name="PP" value="{{ $PP }}">
                                    </div>
                                </div>
                                <input type="id" class="form-control hidden" placeholder="" name="id" value="{{ $proyeksi1->id }}">
                                {{ csrf_field() }}
                                <div class="form-actions">
                                    <div class="row">
                                        <div class="col-md-offset-3 col-md-9">
                                            <button type="submit" class="btn green">Simpan</button>
                                            <button type="reset" class="btn default">Batal</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>

                        <!-- END FORM-->
                    </div>
                </div>
                <!-- END VALIDATION STATES-->
            </div>
        </div>
        <!-- END PAGE BASE CONTENT -->
    </div>
@endsection
@section('javascript')
    @parent


@endsection

@section('stylesheet')
    @parent

@endsection