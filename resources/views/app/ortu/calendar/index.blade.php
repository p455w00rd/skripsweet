@extends('layouts.layout.app')

@section('content')

    <div class="page-content">
        <!-- BEGIN PAGE HEAD-->

        <!-- BEGIN PAGE BASE CONTENT -->
        <div class="row">
            <div class="col-md-12">
                <div class="portlet light portlet-fit bordered calendar">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class=" icon-layers font-green"></i>
                            <span class="caption-subject font-green sbold uppercase">Calendar</span>
                        </div>
                    </div>
                    <div class="portlet-body">
                        <div class="row">
                            <div class="col-sm-12">
                                <div id="calendar"> </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- END PAGE BASE CONTENT -->
    </div>

@endsection

@section('stylesheet')
    <link href="{{asset('assets/global/plugins/fullcalendar/fullcalendar.min.css')}}" rel="stylesheet" type="text/css" />
    @parent

@endsection

@section('javascript')
    @parent

    <script src="{{asset('assets/global/plugins/moment.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/global/plugins/fullcalendar/fullcalendar.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/global/plugins/jquery-ui/jquery-ui.min.js')}}" type="text/javascript"></script>
    {{--<script src="{{asset('assets/apps/scripts/calendar.min.js')}}" type="text/javascript"></script>--}}
    <script>
        var AppCalendar = function() {

            return {
                //main function to initiate the module
                init: function() {
                    this.initCalendar();
                },

                initCalendar: function() {

                    if (!jQuery().fullCalendar) {
                        return;
                    }

                    var date = new Date();
                    var d = date.getDate();
                    var m = date.getMonth();
                    var y = date.getFullYear();

                    var h = {};


                    $('#calendar').fullCalendar({ //re-initialize the calendar
                        // contentHeight: 600,
                        // header: h,
                        defaultView: 'month', // change default view with available options from http://arshaw.com/fullcalendar/docs/views/Available_Views/
                        slotMinutes: 15,
                        // droppable: true, // this allows things to be dropped onto the calendar !!!
                        events: [
                            @forelse($cal as $cal)
                            {
                            title: 'Imunisasi pada {{$cal->puskesmas()->first()->name}}',
                            start: '{{$cal->tanggal_kembali}}',
                            backgroundColor: App.getBrandColor('yellow'),
                            url: '{{route('ortu.calendar.show',[$id,$cal->id])}}'
                        },
                            @empty
                            @endforelse
                        ],
                        eventClick: function(event) {
                            if (event.url) {
                                window.open(event.url);
                                return false;
                            }
                        }
                    });

                }

            };

        }();

        jQuery(document).ready(function() {
            AppCalendar.init();
        });
    </script>

@endsection
