@extends('layouts.layout.app')

@section('content')

    <div class="page-content">
        <!-- BEGIN PAGE HEAD-->
        <div class="page-head">
            <!-- BEGIN PAGE TITLE -->
            <div class="page-title">
                <h1>Pengingat
                </h1>
            </div>
            <!-- END PAGE TITLE -->
        </div>

        <!-- END PAGE BREADCRUMB -->
        <!-- BEGIN PAGE BASE CONTENT -->
        <div class="row">
            {{--@forelse($recept as $recept)--}}
                <div class="col-md-7 col-sm-6">
                    <!-- BEGIN PROFILE SIDEBAR -->
                    <div class="portlet light bordered">
                        <div class="portlet-body form">
                            <div class="tiles">
                                <h3>Nama Anak : {{$cal->anak()->first()->name}}</h3>
                                <br>
                                <h4>Untuk kembali pada {{$cal->puskesmas()->first()->name}} pada tanggal {{date('d F Y', strtotime($cal->tanggal_kembali))}}</h4>
                                <h4>guna imunisasi berikutnya...</h4>
                                @php
                                    $date = \Carbon\Carbon::parse($cal->tanggal_kembali);
                                    $diff = $date->diffInDays($now);
                                @endphp
                                <br>
                                <h4>Terimakasih...  wassalam</h4>
                                <hr>
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="text-center">
                                            @if($diff <= 7)
                                            <a href="{{route('ortu.puskesmas.index',$id)}}" class="btn btn-primary" id="singlebutton"><i class="icon-note"></i> Tambah ke Antrian </a>
                                            @endif
                                            <a class="btn green-jungle" href="{{route('ortu.calendar.andro',[$id,$re])}}" id="singlebutton"><i class="icon-screen-smartphone"></i> Tambahkan ke pengingat android </a>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                    <!-- END PROFILE CONTENT -->
                </div>
            {{--@empty--}}
            {{--@endforelse--}}

        </div>
        <!-- END PAGE BASE CONTENT -->
    </div>

@endsection

@section('stylesheet')
    <style>
        @media only screen and (max-width: 600px) {
            .tiles .tile.double-down {
                max-height: 135px!important;
                width: 280px !important;
            }
            .tiles .tile.double{
                max-width: 135px!important;
            }
            .tiles .tile.big{
                max-width: 280px!important;
            }
            .hidden-xxs{
                display: none;
            }
            .reveal-xxs{
                display: inline;
            }
        }
        @media  (min-width: 601px) {
            .reveal-xxs{
                display: none;
            }
        }
    </style>
    @parent

@endsection

@section('javascript')
    @parent


@endsection
