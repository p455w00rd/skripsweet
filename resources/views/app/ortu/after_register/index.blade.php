@extends('layouts.layout.app')

@section('content')

    <div class="page-content">
        <!-- BEGIN PAGE HEAD-->
        <div class="page-head">
            <!-- BEGIN PAGE TITLE -->
            <div class="page-title">
                <h1>Lengkapi Data Diri Anda Untuk Melanjutkan
                </h1>
            </div>
            <!-- END PAGE TITLE -->
        </div>
        <!-- END PAGE HEAD-->
        <!-- BEGIN PAGE BREADCRUMB -->

        <!-- END PAGE BREADCRUMB -->
        <!-- BEGIN PAGE BASE CONTENT -->
        <div class="row">
            <div class="col-md-9">
                <div class="portlet light portlet-fit portlet-form bordered">
                    <div class="portlet-body">
                        <div class="row">
                            <div class="col-sm-11">
                                <form action="{{route('lengkapi_post',$UserId)}}" class="form-horizontal" method="post">
                                    <div class="form-body">

                                        @if(count( $errors ) > 0)
                                            @foreach ($errors->all() as $error)
                                                <div>{{ $error }}</div>
                                            @endforeach
                                        @endif

                                        <h4 class="form-title-h4-2">Data Orang Tua</h4>
                                        <div class="form-group form-md-line-input {{ $errors->has('nik') ? 'has-error' : '' }}">
                                            <label class="col-md-3 control-label" for="form_control_1">NIK
                                                <span class="required">*</span>
                                            </label>
                                            <div class="col-md-9">
                                                <input type="text" class="form-control" name="nik" placeholder="" value="{{ old('nik') }}">
                                                <div class="form-control-focus"> </div>
                                                <span class="help-block">{{ $errors->has('nik') ? $error : 'Masukkan NIK Orang Tua' }}</span>
                                            </div>
                                        </div>
                                        <div class="form-group form-md-line-input {{ $errors->has('name-parent') ? 'has-error' : '' }}">
                                            <label class="col-md-3 control-label" for="form_control_1">Nama
                                                <span class="required">*</span>
                                            </label>
                                            <div class="col-md-9">
                                                <input type="text" class="form-control" name="name-parent" placeholder="" value="{{$ortu->name}}">
                                                <div class="form-control-focus"> </div>
                                                <span class="help-block">{{ $errors->has('name-parent') ? $error : 'Masukkan Nama Orang Tua' }}</span>
                                            </div>
                                        </div>
                                        <div class="form-group form-md-line-input {{ $errors->has('phone') ? 'has-error' : '' }}">
                                            <label class="col-md-3 control-label" for="form_control_1">telephone
                                                <span class="required">*</span>
                                            </label>
                                            <div class="col-md-9">
                                                <input class="form-control" name="phone" value="{{old('phone')}}"/>
                                                <div class="form-control-focus"> </div>
                                                <span class="help-block">{{ $errors->has('phone') ? $error : 'Masukkan nomor Telephone Orang Tua' }}</span>
                                            </div>
                                        </div>
                                        <div class="form-group form-md-line-input {{ $errors->has('address-parent') ? 'has-error' : '' }}">
                                            <label class="col-md-3 control-label" for="form_control_1">Alamat
                                                <span class="required">*</span>
                                            </label>
                                            <div class="col-md-9">
                                                <textarea class="form-control" name="address-parent" rows="3">{{ old('address-parent') }}</textarea>
                                                <div class="form-control-focus"> </div>
                                                <span class="help-block">{{ $errors->has('address-parent') ? $error : 'Masukkan Alamat Orang Tua' }}</span>
                                            </div>
                                        </div>
                                        <div class="form-group form-md-line-input {{ $errors->has('location') ? 'has-error' : '' }}">
                                            <label class="col-md-3 control-label" for="form_control_1">Kelurahan
                                                <span class="required">*</span>
                                            </label>
                                            <div class="col-md-9">
                                                <select class="bs-select form-control" name="location" data-live-search="true" data-size="8">
                                                    <option value="">Pilih Kelurahan</option>
                                                    @forelse($kelurahan as $kecamatan)
                                                        <option {{ old('location') == $kecamatan->id? 'selected' : '' }} value="{{$kecamatan->id}}">{{$kecamatan->name}}</option>
                                                    @empty
                                                        Tidak Ada Data
                                                    @endforelse
                                                </select>
                                                <div class="form-control-focus"> </div>
                                                <span class="help-block">{{ $errors->has('location') ? $error : 'Pilih Kecamatan' }}</span>
                                            </div>
                                        </div>
                                        <div class="form-group form-md-line-input ">
                                            <label class="col-md-3 control-label" for="form_control_1">Lokasi Ortu
                                                <span class="required">*</span>
                                            </label>
                                            <div class="col-md-9">
                                                <div id="map" style="height: 80%"></div>
                                                <input type="text" hidden id="lat" name="lat" value="">
                                                <input type="text" hidden id="long" name="long" value="">
                                            </div>
                                        </div>
                                        <hr>
                                        <h4 class="form-title-h4-2"> Data Anak</h4>
                                        <div class="form-group form-md-line-input {{ $errors->has('nik-child') ? 'has-error' : '' }}">
                                            <label class="col-md-3 control-label" for="form_control_1">No. NIK
                                            </label>
                                            <div class="col-md-9">
                                                <input type="text" class="form-control" name="nik-child" placeholder="" value="{{ old('nik-child') }}">
                                                <div class="form-control-focus"> </div>
                                                <span class="help-block">{{ $errors->has('nik-anak') ? $error : 'Masukkan Nik Anak bila ada' }}</span>
                                            </div>
                                        </div>
                                        <div class="form-group form-md-line-input {{ $errors->has('name-child') ? 'has-error' : '' }}">
                                            <label class="col-md-3 control-label" for="form_control_1">Nama Anak
                                                <span class="required">*</span>
                                            </label>
                                            <div class="col-md-9">
                                                <input type="text" class="form-control" name="name-child" placeholder="" value="{{ old('name-child') }}">
                                                <div class="form-control-focus"> </div>
                                                <span class="help-block">{{ $errors->has('name-child') ? $error : 'Masukkan Nama Anak' }}</span>
                                            </div>
                                        </div>
                                        <div class="form-group form-md-line-input {{ $errors->has('pob-child') ? 'has-error' : '' }}">
                                            <label class="col-md-3 control-label" for="form_control_1">Tempat Lahir Anak
                                                <span class="required">*</span>
                                            </label>
                                            <div class="col-md-9">
                                                <input type="text" class="form-control" name="pob-child" value="{{ old('pob-child') }}">
                                                <div class="form-control-focus"> </div>
                                                <span class="help-block">{{ $errors->has('pob-child') ? $error : 'Masukkan Kota Tempat Lahir Anak' }}</span>
                                            </div>
                                        </div>
                                        <div class="form-group form-md-line-input {{ $errors->has('dob-child') ? 'has-error' : '' }}">
                                            <label class="col-md-3 control-label" for="form_control_1">Tanggal Lahir Anak
                                                <span class="required">*</span>
                                            </label>
                                            <div class="col-md-9">
                                                <div class="input-group input-medium date date-picker" data-date="now" data-date-viewmode="years">
                                                    <input type="text" name="dob-child" class="form-control" value="{{ old('dob-child') }}"/>
                                                    <span class="input-group-btn">
                                                            <button class="btn default" type="button">
                                                                <i class="fa fa-calendar"></i>
                                                            </button>
                                                        </span>
                                                </div>
                                                <span class="help-block"> {{ $errors->has('dob-child') ? $error : 'Select date' }} </span>
                                            </div>
                                        </div>
                                        <div class="form-group form-md-line-input">
                                            <label class="col-md-3 control-label" for="form_control_1">Jenis Kelamin Anak
                                                <span class="required">*</span>
                                            </label>
                                            <div class="col-md-9">
                                                <div class="md-radio-list">
                                                    <div class="md-radio font-green">
                                                        <input type="radio" id="checkbox-child1" name="gender-child" value="1"  class="md-radiobtn"{{ old('gender-child') == 1? 'checked' : '' }}>
                                                        <label for="checkbox-child1">
                                                            <span></span>
                                                            <span class="check"></span>
                                                            <span class="box"></span> Laki Laki </label>
                                                    </div>
                                                    <div class="md-radio font-red-mint">
                                                        <input type="radio" id="checkbox-child2" name="gender-child" value="2" class="md-radiobtn"{{ old('gender-child') == 2? 'checked' : '' }}>
                                                        <label for="checkbox-child2">
                                                            <span></span>
                                                            <span class="check"></span>
                                                            <span class="box"></span> Perempuan </label>
                                                    </div>
                                                </div>
                                            </div>
                                            <input type="text" value="{{$UserId}}" hidden name="UserId">
                                            {{ csrf_field() }}
                                        </div>
                                    </div>
                                    <div class="form-actions">
                                        <div class="row">
                                            <div class="col-md-offset-3 col-md-9">
                                                <button type="submit" class="btn green">Simpan</button>
                                                <button type="reset" class="btn default">Batal</button>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- END PAGE BASE CONTENT -->
    </div>

@endsection

@section('stylesheet')
    @parent

    <link href="{{asset('assets/apps/leaflet/leaflet.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('assets/global/plugins/bootstrap-select/css/bootstrap-select.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('assets/global/plugins/bootstrap-daterangepicker/daterangepicker.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css')}}" rel="stylesheet" type="text/css" />

@endsection

@section('javascript')
    @parent
    <script src="{{asset('assets/global/plugins/bootstrap-select/js/bootstrap-select.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/pages/scripts/components-bootstrap-select.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/global/plugins/moment.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/global/plugins/bootstrap-daterangepicker/daterangepicker.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/apps/leaflet/leaflet.js')}}" type="text/javascript"></script>

    <script>
        $('.date-picker').datepicker({
            rtl: App.isRTL(),
            orientation: "right",
            autoclose: true,
            currentText: "Today:",
            format: "dd-MM-yyyy"
        });
    </script>
    <script>
        var map = L.map('map',{
            center: [-7.253339, 112.772028],
            zoom: 15
        });

        //map rendering
        L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', {
            attribution: '&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
        }).addTo(map);

        //map input
        map.on('click',
            function(e){
                var coord = e.latlng.toString().split(',');
                var lat = coord[0].split('(');
                var lng = coord[1].split(')');
                console.log("You clicked the map at latitude: " + lat[1] + " and longitude:" + lng[0]);
                var marker = L.marker([lat[1],lng[0]]).addTo(map);
                map.on('click', function () {
                    map.removeLayer(marker);
                });
                document.getElementById("lat").value = lat[1];
                document.getElementById("long").value = lng[0];
            });
    </script>

@endsection
