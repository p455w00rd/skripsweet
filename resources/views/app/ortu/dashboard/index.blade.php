@extends('layouts.layout.app')

@section('content')

    <div class="page-content">
        <!-- BEGIN PAGE HEAD-->
        <div class="page-head">
            <!-- BEGIN PAGE TITLE -->
            <div class="page-title">
                <h1>Dashboard Orang Tua
                </h1>
            </div>
            <!-- END PAGE TITLE -->
        </div>
        <!-- END PAGE HEAD-->
        <!-- BEGIN PAGE BREADCRUMB -->
        <ul class="page-breadcrumb breadcrumb">
            <li>
                <a href="">Home</a>
                <i class="fa fa-circle"></i>
            </li>
        </ul>
        <!-- END PAGE BREADCRUMB -->
        <!-- BEGIN PAGE BASE CONTENT -->
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN PROFILE SIDEBAR -->
                <div class="portlet light bordered">
                    <div class="portlet-body form">
                        <div class="tiles">
                            <a href="{{route('ortu.puskesmas.index',$id)}}" class="tile double-down bg-blue-hoki">
                                <div class="tile-body">
                                    <i class="fa fa-hospital-o"></i>
                                </div>
                                <div class="tile-object">
                                    <div class="name"> Pendaftaran Puskesmas </div>
                                </div>
                            </a>

                            <a href="{{route('ortu.near',$id)}}" class="tile double selected bg-green-turquoise">
                                <div class="corner"> </div>
                                <div class="check"> </div>
                                <div class="tile-body">
                                    <i class="fa fa-map-o"></i>
                                </div>
                                <div class="tile-object">
                                    <div class="name"> Peta Puskesmas Terdekat </div>
                                </div>
                            </a>
                            <a href="{{route('ortu.profile.me',[$id])}}" class="tile selected bg-yellow-saffron">
                                <div class="tile-body">

                                    <i class="fa fa-user"></i>
                                </div>
                                <div class="tile-object">
                                    <div class="name"> Profile </div>
                                </div>
                            </a>
                            <a href="{{route('ortu.anak.me',[$ortu->id])}}" class="tile double bg-blue-madison">
                                <div class="tile-body">
                                    <img src="{{asset('assets/image/baby-boy.png')}}" style="height: 86px;" alt="">
                                    <div class="hidden-xxs">
                                        <h4>Data Anak</h4>
                                        <p>
                                            @forelse($ortu->anak()->limit(7)->get() as $anak)
                                                {{$anak->name}},
                                            @empty
                                            @endforelse
                                            ...
                                        </p>
                                    </div>
                                </div>
                                <div class="tile-object">

                                        <div class="name">
                                            <div class="hidden-xxs">Jumlah Anak </div>
                                            <div class="reveal-xxs">Data Anak</div>
                                        </div>
                                        <div class="number">
                                            <div class="hidden-xxs">
                                                {{$ortu->anak()->count()}}
                                            </div>
                                        </div>
                                    {{--<div class="hidden-lg hidden-md hidden-sm">--}}
                                        {{--<div class="name"> Data Anak </div>--}}
                                    {{--</div>--}}

                                </div>
                            </a>
                            <div class="tile bg-purple">
                                <div class="tile-body">
                                    <i class="icon-home"></i>
                                </div>
                                <div class="tile-object">
                                    <div class="name"> Dashboard </div>
                                </div>
                            </div>
                            <a href="{{route('ortu.calendar.index',$id)}}" class="tile double bg-red-sunglo big">
                                <div class="tile-body">
                                    <i class="fa fa-calendar"></i>
                                </div>
                                <div class="tile-object">
                                    <div class="name"> Jadwal Imunisasi </div>
                                </div>
                            </a>
                            <a href="{{route('ortu.puskesmas.queue.list',$id)}}" class="tile double bg-yellow-mint big">
                                <div class="tile-body">
                                    <i class="icon-note"></i>
                                </div>
                                <div class="tile-object">
                                    <div class="name"> Kartu Antrian </div>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
                <!-- END PROFILE CONTENT -->
            </div>
        </div>
        <!-- END PAGE BASE CONTENT -->
    </div>

@endsection

@section('stylesheet')
    <style>
        @media only screen and (max-width: 600px) {
            .tiles .tile.double-down {
                max-height: 135px!important;
                width: 280px !important;
            }
            .tiles .tile.double{
                max-width: 135px!important;
            }
            .tiles .tile.big{
                max-width: 280px!important;
            }
            .hidden-xxs{
                display: none;
            }
            .reveal-xxs{
                display: inline;
            }
        }
        @media  (min-width: 601px) {
            .reveal-xxs{
                display: none;
            }
        }
    </style>
    @parent

@endsection

@section('javascript')
    @parent


@endsection
