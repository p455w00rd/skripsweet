@extends('layouts.layout.app')

@section('content')

    <div class="page-content">
        <!-- BEGIN PAGE HEAD-->
        <div class="page-head">
            <!-- BEGIN PAGE TITLE -->
            <div class="page-title">
                <h1>Dashboard Orang Tua
                </h1>
            </div>
            <!-- END PAGE TITLE -->
        </div>
        <!-- END PAGE HEAD-->
        <!-- BEGIN PAGE BREADCRUMB -->
        <ul class="page-breadcrumb breadcrumb">
            <li>
                <a href="">Home</a>
                <i class="fa fa-circle"></i>
            </li>
        </ul>
        <!-- END PAGE BREADCRUMB -->
        <!-- BEGIN PAGE BASE CONTENT -->
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN PROFILE SIDEBAR -->
                <div class="portlet light bordered">
                    <div class="portlet-body form">
                        <div class="tiles">
                            <h4>Pilih Puskesmas Terdekat</h4>
                            @if($puskesmas!=null)
                            @forelse($puskesmas as $puskesmas)
                            <a href="{{route('ortu.puskesmas.queue',[$id,$puskesmas->puskesmas()->first()->id])}}" class="tile double bg-green-turquoise">
                                <div class="tile-body">
                                    <i class="fa fa-hospital-o"></i>
                                </div>
                                <div class="tile-object">
                                    <div class="name"> {{$puskesmas->puskesmas()->first()->name}} </div>
                                    <div class="number">{{round($puskesmas->distance * 100,2)}} Km*</div>
                                </div>
                            </a>
                                @empty
                                    <h3>Tidak ada Data</h3>
                            @endforelse
                                @else
                                <h2>Pilih Lokasi Anda terlebih dahulu</h2>
                                <h4><a href="{{route('ortu.map.me',[$id])}}">klik disini</a></h4>
                                @endif

                        </div>
                    </div>
                </div>
                <div class="portlet light bordered">
                    <div class="portlet-body form">
                        <div class="tiles">
                            <h4>Seluruh Puskesmas</h4>

                            @forelse($puskesall as  $puskesmas)
                                <a href="{{route('ortu.puskesmas.queue',[$id,$puskesmas->id])}}" class="tile double bg-green-turquoise">
                                    <div class="tile-body">
                                        <i class="fa fa-hospital-o"></i>
                                    </div>
                                    <div class="tile-object">
                                        <div class="name"> {{$puskesmas->name}} </div>
                                    </div>
                                </a>
                            @empty
                            @endforelse
                        </div>
                    </div>
                </div>
                <!-- END PROFILE CONTENT -->
            </div>
        </div>
        <!-- END PAGE BASE CONTENT -->
    </div>

@endsection

@section('stylesheet')
    <style>
        @media only screen and (max-width: 600px) {
            .tiles .tile.double-down {
                max-height: 135px!important;
                width: 280px !important;
            }
            .tiles .tile.double{
                max-width: 135px!important;
            }
            .tiles .tile.big{
                max-width: 280px!important;
            }
            .hidden-xxs{
                display: none;
            }
            .reveal-xxs{
                display: inline;
            }
        }
        @media  (min-width: 601px) {
            .reveal-xxs{
                display: none;
            }
        }
    </style>
    @parent

@endsection

@section('javascript')
    @parent


@endsection
