@extends('layouts.layout.app')

@section('content')

    <div class="page-content">
        <!-- BEGIN PAGE HEAD-->
        <div class="page-head">
            <!-- BEGIN PAGE TITLE -->
            <div class="page-title">
                <h1>Kartu Antrian
                </h1>
            </div>
            <!-- END PAGE TITLE -->
        </div>

        <!-- END PAGE BREADCRUMB -->
        <!-- BEGIN PAGE BASE CONTENT -->
        <div class="row">
            @forelse($recept as $recept)
            <div class="col-md-4 col-sm-6">
                <!-- BEGIN PROFILE SIDEBAR -->
                <div class="portlet light bordered">
                    <div class="portlet-body form">
                        <div class="tiles">
                            <h4>{{$recept->puskesmas()->first()->name}}</h4>
                            <h5>Nomor Antrian</h5>
                            <h1 style="font-size: 200px" class="text-center">{{$recept->queue}}</h1>
                            <h5>Nama Anak</h5>
                            <h3>{{$recept->anak()->first()->name}}</h3>

                        </div>
                    </div>
                </div>
                <!-- END PROFILE CONTENT -->
            </div>
                @empty
                @endforelse

        </div>
        <!-- END PAGE BASE CONTENT -->
    </div>

@endsection

@section('stylesheet')
    <style>
        @media only screen and (max-width: 600px) {
            .tiles .tile.double-down {
                max-height: 135px!important;
                width: 280px !important;
            }
            .tiles .tile.double{
                max-width: 135px!important;
            }
            .tiles .tile.big{
                max-width: 280px!important;
            }
            .hidden-xxs{
                display: none;
            }
            .reveal-xxs{
                display: inline;
            }
        }
        @media  (min-width: 601px) {
            .reveal-xxs{
                display: none;
            }
        }
    </style>
    @parent

@endsection

@section('javascript')
    @parent


@endsection
