@extends('layouts.layout.app')

@section('content')

    <div class="page-content">
        <!-- BEGIN PAGE HEAD-->
        <div class="page-head">
            <!-- BEGIN PAGE TITLE -->
            <div class="page-title">
                <h1>Ambil Antrian Puskesmas
                </h1>
            </div>
            <!-- END PAGE TITLE -->
        </div>
        <!-- END PAGE HEAD-->
        <!-- BEGIN PAGE BREADCRUMB -->
        <ul class="page-breadcrumb breadcrumb">
            <li>
                <a href="">Home</a>
                <i class="fa fa-circle"></i>
            </li>
        </ul>
        <!-- END PAGE BREADCRUMB -->
        <!-- BEGIN PAGE BASE CONTENT -->
        <div class="row">
            <div class="col-md-8">
                <!-- BEGIN PROFILE SIDEBAR -->
                <div class="portlet light bordered">
                    <div class="portlet-body form">
                        <div class="tiles">
                            <h4>{{$pusk->name}}</h4>
                            <p>{{$pusk->address}}</p>
                            <form action="{{route('ortu.puskesmas.queue.add',$id)}}"class="form-horizontal" method="post">
                                <div class="form-group ">
                                    <label class="col-md-3 " for="form_control_1">Pilih Anak
                                    </label>
                                    <div class="col-md-7">
                                        <select name="id_anak" id="anak" class="form-control">
                                            <option value="" disabled="" selected>Pilih Anak</option>
                                            @forelse($ortu->anak()->get() as $anak)
                                            <option value="{{$anak->id}}">{{$anak->name}}</option>
                                                @empty
                                            @endforelse
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3" for="form_control_1">Nomor Registrasi
                                    </label>
                                    <div class="col-md-6">
                                        <input type="text" id="id_anak" disabled class="disabled form-control">
                                        <input type="text" value="{{$pusk->id}}" hidden name="id_puskesmas">
                                        {{ csrf_field() }}

                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="text-center">
                                            <button class="btn btn-primary" id="singlebutton"> Tambah ke Antrian </button>
                                        </div>
                                    </div>
                                </div>

                            </form>
                        </div>
                    </div>
                </div>
                <!-- END PROFILE CONTENT -->
            </div>
        </div>
        <!-- END PAGE BASE CONTENT -->
    </div>

@endsection

@section('stylesheet')
    <style>
        @media only screen and (max-width: 600px) {
            .tiles .tile.double-down {
                max-height: 135px!important;
                width: 280px !important;
            }
            .tiles .tile.double{
                max-width: 135px!important;
            }
            .tiles .tile.big{
                max-width: 280px!important;
            }
            .hidden-xxs{
                display: none;
            }
            .reveal-xxs{
                display: inline;
            }
        }
        @media  (min-width: 601px) {
            .reveal-xxs{
                display: none;
            }
        }
    </style>
    @parent

@endsection

@section('javascript')
    @parent
    <script>
        $('#anak').on('change', function(e){
            console.log(e);
            var state_id = e.target.value;

            $.get('{{ route('anak.no_regis',$pusk->id) }}?id_anak=' + state_id, function(data) {
                console.log(data);
                if(data.id == null){
                    id_anak ='Belum Terdaftar';
                } else {
                    id_anak = data.no_regis;
                }
                document.getElementById("id_anak").value = id_anak;
            });
        });
    </script>

@endsection
