<!DOCTYPE html>
<html lang="en">
<head>
    <title>Laporan Pencatatan Imunisasi Rutin {{$puskes->name}}</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <style>
        thead tr th{
            text-align: center;
            vertical-align: middle !important;
        }
    </style>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
    <h2>Laporan Pencatatan Imunisasi Rutin {{$puskes->name}}</h2>
    <form class="form-inline" method="get">
        <div class="form-group">
            <label for="email">Kelurahan : {{$kelurahan->name}}</label>

        </div>
        <div class="clearfix"></div>
        <div class="form-group">
            <label for="pwd">Bulan : {{$m}}</label>

        </div>
        <div class="clearfix"></div>
        <div class="form-group">
            <label for="pwd">Tahun : {{$y}}</label>
        </div>
        <div class="clearfix"></div>
    </form>
    <?php
    header("Content-type: application/vnd-ms-excel");
    header("Content-Disposition: attachment; filename=Laporan Pencatatan Rutin.xls");
    ?>
    <table class="table table-bordered table-striped">
        <thead>
        <tr>
            <th rowspan="2">No</th>
            <th rowspan="2">Nama</th>
            <th rowspan="2">JK</th>
            <th rowspan="2">Nama Orangtua</th>
            <th rowspan="2">Tanggal Lahir</th>
            <th rowspan="2">Alamat Lengap</th>
            <th colspan="2">HB0</th>
            <th rowspan="2">BCG</th>
            <th rowspan="2">Polio1</th>
            <th rowspan="2">Penta1</th>
            <th rowspan="2">Polio2</th>
            <th rowspan="2">Penta2</th>
            <th rowspan="2">Polio3</th>
            <th rowspan="2">Penta3</th>
            <th rowspan="2">Polio4</th>

            <th rowspan="2">IPV</th>
            <th rowspan="2">MR</th>
            <th colspan="2">BOOSTER</th>
        </tr>
        <tr>
            <th>< 7hr</th>
            <th>> 7hr</th>



            <th>Pentavalen</th>
            <th>MR</th>
        </tr>
        </thead>
        <tbody>
        @forelse($anak as $anak)
            <tr>
                <td>{{$c++}}</td>
                <td>{{$anak->name}}</td>
                <td>{{$anak->gender == 1?'L':'P'}}</td>
                <td>{{$anak->ortu->name}}</td>
                <td>{{date('d/m/Y', strtotime($anak->dob))}}</td>
                <td>{{$anak->address}}</td>
                @if($anak->history_imun->where('id_imunisasi',1)->where('umur','LIKE','%jam')->first() != null)
                    <td>{{$anak->history_imun->where('id_imunisasi',1)->first()->tanggal()}}</td>
                @elseif($anak->history_imun->where('id_imunisasi',1)->where('umur','LIKE','%hari')->first() != null)
                    <td>{{$anak->history_imun->where('id_imunisasi',1)->first()->tanggal()}}</td>
                @else
                    <td></td>
                    <td>{{$anak->history_imun->where('id_imunisasi',1)->first() != null?$anak->history_imun->where('id_imunisasi',1)->first()->tanggal() : ''}}</td>
                @endif
                <td>{{$anak->history_imun->where('id_imunisasi',2)->first() != null?$anak->history_imun->where('id_imunisasi',2)->first()->tanggal():''}}</td>
                <td>{{$anak->history_imun->where('id_imunisasi',3)->first() != null?$anak->history_imun->where('id_imunisasi',3)->first()->tanggal():''}}</td>
                <td>{{$anak->history_imun->where('id_imunisasi',4)->first() != null?$anak->history_imun->where('id_imunisasi',4)->first()->tanggal():''}}</td>
                <td>{{$anak->history_imun->where('id_imunisasi',5)->first() != null?$anak->history_imun->where('id_imunisasi',5)->first()->tanggal():''}}</td>
                <td>{{$anak->history_imun->where('id_imunisasi',6)->first() != null?$anak->history_imun->where('id_imunisasi',6)->first()->tanggal():''}}</td>
                <td>{{$anak->history_imun->where('id_imunisasi',7)->first() != null?$anak->history_imun->where('id_imunisasi',7)->first()->tanggal():''}}</td>
                <td>{{$anak->history_imun->where('id_imunisasi',8)->first() != null?$anak->history_imun->where('id_imunisasi',8)->first()->tanggal():''}}</td>
                <td>{{$anak->history_imun->where('id_imunisasi',9)->first() != null?$anak->history_imun->where('id_imunisasi',9)->first()->tanggal():''}}</td>
                <td>{{$anak->history_imun->where('id_imunisasi',10)->first() != null?$anak->history_imun->where('id_imunisasi',10)->first()->tanggal():''}}</td>
                <td>{{$anak->history_imun->where('id_imunisasi',11)->first() != null?$anak->history_imun->where('id_imunisasi',11)->first()->tanggal():''}}</td>
                <td>{{$anak->history_imun->where('id_imunisasi',13)->first() != null?$anak->history_imun->where('id_imunisasi',13)->first()->tanggal():''}}</td>
                <td>{{$anak->history_imun->where('id_imunisasi',16)->first() != null?$anak->history_imun->where('id_imunisasi',16)->first()->tanggal():''}}</td>
            </tr>
        @empty
        @endforelse

        </tbody>
    </table>
</div>

</body>
</html>
