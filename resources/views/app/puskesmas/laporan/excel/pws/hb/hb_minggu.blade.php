<!DOCTYPE html>
<html lang="en">
<head>
    <title>PWS Imunisasi</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <style>
        thead tr th{
            text-align: center;
            vertical-align: middle !important;
        }
    </style>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
    <h2>PWS Imunisasi HB >7 Hari</h2>
    {{--<p>The .table-striped class adds zebra-stripes to a table:</p>--}}
    <form class="form-inline" action="{{route('laporan.hb>7hari',$puskes->id)}}"method="get">

        <div class="clearfix"></div>
        <div class="form-group">
            <label for="pwd">Bulan : </label>
            <select class="form-control" name="bulan" id="">
                <option value="1"{{$m == '1' ? 'selected':''}}>Januari</option>
                <option value="2"{{$m == '2' ? 'selected':''}}>Februari</option>
                <option value="3"{{$m == '3' ? 'selected':''}}>Maret</option>
                <option value="4"{{$m == '4' ? 'selected':''}}>April</option>
                <option value="5"{{$m == '5' ? 'selected':''}}>Mei</option>
                <option value="6"{{$m == '6' ? 'selected':''}}>Juni</option>
                <option value="7"{{$m == '7' ? 'selected':''}}>Juli</option>
                <option value="8"{{$m == '8' ? 'selected':''}}>Agustus</option>
                <option value="9"{{$m == '9' ? 'selected':''}}>September</option>
                <option value="10"{{$m == '10' ? 'selected':''}}>Oktober</option>
                <option value="11"{{$m == '11' ? 'selected':''}}>November</option>
                <option value="12"{{$m == '12' ? 'selected':''}}>Desember</option>
            </select>
        </div>
        <div class="clearfix"></div>
        <div class="form-group">
            <label for="pwd">Tahun : </label>
            <input type="text" class="form-control" name="year" value="{{$y}}">
        </div>
        <button type="submit" class="btn btn-info">Rubah</button>
    </form>
    <?php
    header("Content-type: application/vnd-ms-excel");
    header("Content-Disposition: attachment; filename=Laporan Pencatatan PWS.xls");
    ?>
    <table class="table table-bordered table-striped">
        <thead>
        <tr>
            <th rowspan="3">No</th>
            <th rowspan="3">Puskesmas</th>
            <th rowspan="3">Kelurahan</th>
            <th colspan="3" rowspan="2">Bayi Baru Lahir</th>
            <th colspan="3" rowspan="2">Surviving Infant</th>
            <th colspan="12">HB >7 Hari</th>
        </tr>
        <tr>
            <th colspan="6">BLN {{$content}}</th>
            <th colspan="6">S/D {{$content}}</th>
        </tr>
        <tr>
            <th>L</th>
            <th>P</th>
            <th>&Sigma;</th>
            <th>L</th>
            <th>P</th>
            <th>&Sigma;</th>
            <th>L</th>
            <th>%</th>
            <th>P</th>
            <th>%</th>
            <th>&Sigma;</th>
            <th>%</th>
            <th>L</th>
            <th>%</th>
            <th>P</th>
            <th>%</th>
            <th>&Sigma;</th>
            <th>%</th>
        </tr>
        </thead>
        <tbody>
            <tr>
                <td rowspan="{{$puskes->kelurahan()->count()+1}}">{{$c}}</td>
                <td rowspan="{{$puskes->kelurahan()->count()+1}}">{{$puskes->name}}</td>
            </tr>
            @forelse($puskes->kelurahan()->get() as $kelurahan)
                <?php
                $pro = $kelurahan->proyeksi()->where('tahun',$y)->first();
                if (isset($pro)){
                    $a = $pro->PL;
                    $b = $pro->PP;
                    $d=$a+$b;
                    if (isset($pro->SL)){
                        $sl = $pro->SL;
                    }else{
                        $sl = 0;
                    }
                    if (isset($pro->SP)){
                        $sp = $pro->SP;
                    }else{
                        $sp = 0;
                    }
                    $ss = $sl+$sp;
                }
                else{
                    $a=0;
                    $b=0;
                    $d=0;
                    $sl=0;
                    $sp=0;
                    $ss=0;
                }
                $imun = $history->whereYear('created_at',$y)->whereMonth('created_at',$m)->get()->where('location',$kelurahan->id);
                $a1 = $imun->where('gender',1)->count();
                $b1 = $imun->where('gender',2)->count();
                $d1 = $imun->count();
                $imun2 = $sd->get()->where('location',$kelurahan->id);
                $a2 = $imun2->where('gender',1)->count();
                $b2 = $imun2->where('gender',2)->count();
                $d2 = $imun2->count();
                ?>
                <tr>
                    <td>{{$kelurahan->name}}</td>
                    <td>{{$a}}</td>
                    <td>{{$b}}</td>
                    <td>{{$d}}</td>
                    <td>{{$sl}}</td>
                    <td>{{$sp}}</td>
                    <td>{{$ss}}</td>
                    {{--<td>{{$kelurahan->anak()->whereBetween('created_at',[$dateS,$dateE])->where('gender',1)->count()}}</td>--}}
                    {{--<td>{{$kelurahan->anak()->whereBetween('created_at',[$dateS,$dateE])->where('gender',2)->count()}}</td>--}}
                    {{--<td>{{$kelurahan->anak()->whereBetween('created_at',[$dateS,$dateE])->count()}}</td>--}}
                    {{--<td>{{$a = $kelurahan->anak()->where('gender',1)->whereBetween('created_at',[$dateS,$dateE])->where('surviving',1)->count()}}</td>--}}
                    {{--<td>{{$b = $kelurahan->anak()->where('gender',2)->whereBetween('created_at',[$dateS,$dateE])->where('surviving',1)->count()}}</td>--}}
                    {{--<td>{{$d = $kelurahan->anak()->whereBetween('created_at',[$dateS,$dateE])->where('surviving',1)->count()}}</td>--}}
                    {{--dari bulan--}}
                    <td>{{$a1}}</td>
                    <td>{{$a != 0 ?$a1/$a*100:0}}%</td>
                    <td>{{$b1}}</td>
                    <td>{{$b != 0?$b1/$b*100:0}}%</td>
                    <td>{{$d1}}</td>
                    <td>{{$d != 0?$d1/$d*100:0}}%</td>
                    {{--sd bulan--}}
                    <td>{{$a2}}</td>
                    <td>{{$a != 0 ?$a2/$a*100:0}}%</td>
                    <td>{{$b2}}</td>
                    <td>{{$b != 0?$b2/$b*100:0}}%</td>
                    <td>{{$d2}}</td>
                    @if($d != 0)
                        @if($target == 0)
                            <td style="background-color: red">
                        @elseif($d2/$d*100<=$target)
                            <td style="background-color: red">
                        @else
                            <td style="background-color: green">
                        @endif
                    @else
                        <td>
                            @endif
                            @if($target != 0)
                                {{$d != 0?$d2/$d*100:0}}%
                            @else
                                Data Belum Disetting
                            @endif
                        </td>
                </tr>
            @empty
            @endforelse
        </tbody>
    </table>
</div>

</body>
</html>
