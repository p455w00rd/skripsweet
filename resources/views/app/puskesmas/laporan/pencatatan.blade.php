<!DOCTYPE html>
<html lang="en">
<head>
    <title>Laporan Pencatatan Imunisasi Rutin {{$puskes->name}}</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link href="{{asset('assets/global/plugins/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet" type="text/css" />
    <style>
        thead tr th{
            text-align: center;
            vertical-align: middle !important;
        }
    </style>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
    <h2>Laporan Pencatatan Imunisasi Rutin {{$puskes->name}}</h2>
    <form class="form-inline" action="{{route('laporan.pencatatan',$puskes->id)}}"method="get">
        <div class="form-group">
            <label for="email">Kelurahan : </label>
            <select name="kelurahan" id="" class="form-control">
                @forelse($puskes->kelurahan()->get() as $kelurahan2)
                    <option value="{{$kelurahan2->id}}" {{$kelurahan->id == $kelurahan2->id?'selected':''}}>{{$kelurahan2->name}}</option>
                    @empty
                @endforelse
            </select>
        </div>
        <div class="clearfix"></div>
        <div class="form-group">
            <label for="pwd">Bulan : </label>
            <select class="form-control" name="bulan" id="">
                <option value="1"{{$m == '1' ? 'selected':''}}>Januari</option>
                <option value="2"{{$m == '2' ? 'selected':''}}>Februari</option>
                <option value="3"{{$m == '3' ? 'selected':''}}>Maret</option>
                <option value="4"{{$m == '4' ? 'selected':''}}>April</option>
                <option value="5"{{$m == '5' ? 'selected':''}}>Mei</option>
                <option value="6"{{$m == '6' ? 'selected':''}}>Juni</option>
                <option value="7"{{$m == '7' ? 'selected':''}}>Juli</option>
                <option value="8"{{$m == '8' ? 'selected':''}}>Agustus</option>
                <option value="9"{{$m == '9' ? 'selected':''}}>September</option>
                <option value="10"{{$m == '10' ? 'selected':''}}>Oktober</option>
                <option value="11"{{$m == '11' ? 'selected':''}}>November</option>
                <option value="12"{{$m == '12' ? 'selected':''}}>Desember</option>
                <option value="13"{{$m == '13' ? 'selected':''}}>Semua</option>
            </select>
        </div>
        <div class="clearfix"></div>
        <div class="form-group">
            <label for="pwd">Tahun : </label>
            <input type="text" class="form-control" name="year" value="{{$y}}">
        </div>
        <div class="clearfix"></div>
        <div class="form-group">
            <label for="pwd">Data Berdasarkan : </label>
            <select class="form-control" name="data" id="">
                <option value="1"{{$data == '1' ? 'selected':''}}>Data Registrasi Anak</option>
                <option value="2"{{$data == '2' ? 'selected':''}}>Data Imunisasi Anak</option>
            </select>
        </div>
        <button type="submit" class="btn btn-info">Rubah</button>
        <button type="submit" class="btn btn-success" onclick="myFunction()"><i class="fa fa-file-excel-o"></i> Export Excel</button>
    </form>
<!--    --><?php
//    header("Content-type: application/vnd-ms-excel");
//    header("Content-Disposition: attachment; filename=hasil.xls");
//    ?>
    <table class="table table-bordered table-striped">
        <thead>
        <tr>
            <th rowspan="2">No</th>
            <th rowspan="2">Nama</th>
            <th rowspan="2">JK</th>
            <th rowspan="2">Nama Orangtua</th>
            <th rowspan="2">Tanggal Lahir</th>
            <th rowspan="2">Alamat Lengap</th>
            <th colspan="2">HB0</th>
            <th rowspan="2">BCG</th>
            <th rowspan="2">Polio1</th>
            <th rowspan="2">Penta1</th>
            <th rowspan="2">Polio2</th>
            <th rowspan="2">Penta2</th>
            <th rowspan="2">Polio3</th>
            <th rowspan="2">Penta3</th>
            <th rowspan="2">Polio4</th>

            <th rowspan="2">IPV</th>
            <th rowspan="2">MR</th>
            <th colspan="2">BOOSTER</th>
        </tr>
        <tr>
            <th>< 7hr</th>
            <th>> 7hr</th>
            <th>Pentavalen</th>
            <th>MR</th>
        </tr>
        </thead>
        <tbody>
        @forelse($anak as $anak)
            <tr>
                <td>{{$c++}}</td>
                <td>{{$anak->name}}</td>
                <td>{{$anak->gender == 1?'L':'P'}}</td>
                <td>{{$anak->ortu->name}}</td>
                <td>{{date('d/m/Y', strtotime($anak->dob))}}</td>
                <td>{{$anak->address}}</td>
                @if($anak->history_imun->where('id_imunisasi',1)->where('umur','LIKE','%jam')->first() != null)
                    <td>{{$anak->history_imun->where('id_imunisasi',1)->first()->tanggal()}}</td>
                @elseif($anak->history_imun->where('id_imunisasi',1)->where('umur','LIKE','%hari')->first() != null)
                    <td>{{$anak->history_imun->where('id_imunisasi',1)->first()->tanggal()}}</td>
                @else
                    <td></td>
                    <td>{{$anak->history_imun->where('id_imunisasi',1)->first() != null?$anak->history_imun->where('id_imunisasi',1)->first()->tanggal() : ''}}</td>
                @endif
                <td>{{$anak->history_imun->where('id_imunisasi',2)->first() != null?$anak->history_imun->where('id_imunisasi',2)->first()->tanggal():''}}</td>
                <td>{{$anak->history_imun->where('id_imunisasi',3)->first() != null?$anak->history_imun->where('id_imunisasi',3)->first()->tanggal():''}}</td>
                <td>{{$anak->history_imun->where('id_imunisasi',4)->first() != null?$anak->history_imun->where('id_imunisasi',4)->first()->tanggal():''}}</td>
                <td>{{$anak->history_imun->where('id_imunisasi',5)->first() != null?$anak->history_imun->where('id_imunisasi',5)->first()->tanggal():''}}</td>
                <td>{{$anak->history_imun->where('id_imunisasi',6)->first() != null?$anak->history_imun->where('id_imunisasi',6)->first()->tanggal():''}}</td>
                <td>{{$anak->history_imun->where('id_imunisasi',7)->first() != null?$anak->history_imun->where('id_imunisasi',7)->first()->tanggal():''}}</td>
                <td>{{$anak->history_imun->where('id_imunisasi',8)->first() != null?$anak->history_imun->where('id_imunisasi',8)->first()->tanggal():''}}</td>
                <td>{{$anak->history_imun->where('id_imunisasi',9)->first() != null?$anak->history_imun->where('id_imunisasi',9)->first()->tanggal():''}}</td>
                <td>{{$anak->history_imun->where('id_imunisasi',10)->first() != null?$anak->history_imun->where('id_imunisasi',10)->first()->tanggal():''}}</td>
                <td>{{$anak->history_imun->where('id_imunisasi',11)->first() != null?$anak->history_imun->where('id_imunisasi',11)->first()->tanggal():''}}</td>
                <td>{{$anak->history_imun->where('id_imunisasi',13)->first() != null?$anak->history_imun->where('id_imunisasi',13)->first()->tanggal():''}}</td>
                <td>{{$anak->history_imun->where('id_imunisasi',16)->first() != null?$anak->history_imun->where('id_imunisasi',16)->first()->tanggal():''}}</td>
            </tr>
            @empty
        @endforelse

        </tbody>
    </table>
</div>
<script>
    function myFunction() {
        var myWindow = window.open("pencatatan/excel?kelurahan={{$kelurahan->id}}&bulan={{$m}}&year={{$y}}&data={{$data}}", "", "width=200,height=100");
    }
</script>
</body>
</html>
