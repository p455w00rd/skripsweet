@extends('layouts.layout.app')

@section('content')

    <div class="page-content">
        <!-- BEGIN PAGE HEAD-->
        {{--<div class="page-head">--}}
            {{--<!-- BEGIN PAGE TITLE -->--}}
            {{--<div class="page-title">--}}
                {{--<h1>Pemetaan--}}
                {{--</h1>--}}
            {{--</div>--}}
            {{--<!-- END PAGE TITLE -->--}}

        {{--</div>--}}
        <!-- END PAGE HEAD-->

        <!-- BEGIN PAGE BASE CONTENT -->
        <div class="row">
            <div class="col-md-12">
                <div class="portlet light portlet-fit portlet-form bordered" style="margin-bottom: 5px">
                    <div class="portlet-body">
                        <form action="" class="form-horizontal" style="margin-bottom: 0px">
                            <div class="form-body" style="padding: 10px">
                                <div class="form-group" style="margin-bottom: 0px">
                                    <label class="col-md-2 control-label">Ubah Tahun</label>
                                    <div class="col-md-3">
                                        <input type="text" class="form-control" name="year" placeholder="Enter text" value="{{$y}}">
                                    </div>
                                    <div class="col-md-1">
                                        <button class="btn btn-primary" type="submit">Ubah</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <!-- BEGIN VALIDATION STATES-->
                <div class="portlet light portlet-fit portlet-form bordered">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class=" icon-layers font-green"></i>
                            <span class="caption-subject font-dark bold uppercase">Peta Wilayah Puskesmas {{$puskes->name}} Tahun
                            {{$y}}
                            </span>
                        </div>
                    </div>
                    <div class="portlet-body">
                        <!-- BEGIN FORM-->
                        <div id="map" style="height: 80%"></div>
                        <!-- END FORM-->
                        <a href="test">test</a>
                    </div>
                </div>
                <!-- END VALIDATION STATES-->
            </div>
        </div>
        <!-- END PAGE BASE CONTENT -->
    </div>
@endsection
@section('javascript')
    @parent

    <script src="{{asset('assets/apps/leaflet/leaflet.js')}}" type="text/javascript"></script>
    <script>

        // initialize the map
        var map = L.map('map',{
            center: [-7.257207,112.752014],
            zoom: 15
        });

        L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', {
            attribution: '&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
        }).addTo(map);

        var info = L.control();

        info.onAdd = function (map) {
            this._div = L.DomUtil.create('div', 'info'); // create a div with a class "info"
            this.update();
            return this._div;
        };

        // method that we will use to update the control based on feature properties passed
        info.update = function (props) {
            this._div.innerHTML = '<h4>Peta Wilayah Puskesmas {{$puskes->name}}</h4>' +  (props ?
                'Kelurahan <b>' + props.Name + '</b><br />'
                : 'Hover over a state');
        };

        info.addTo(map);
        function highlightFeature(e) {
            var layer = e.target;
            info.update(layer.feature.properties);
            layer.setStyle({
                weight: 5,
                color: '#94A0B2 ',
                dashArray: '',
                fillOpacity: 0.7
            });

            if (!L.Browser.ie && !L.Browser.opera && !L.Browser.edge) {
                layer.bringToFront();
            }
        }
        function resetHighlight(e) {
            geojson.resetStyle(e.target);
            info.update();
        }
        var geojson;

        function onEachFeature(feature, layer) {
            layer.on({
                mouseover: highlightFeature,
                mouseout: resetHighlight,
                click: zoomToFeature
            });
            if (feature.properties && feature.properties.Name) {
                layer.bindPopup("Kelurahan "+feature.properties.Name
                    +'<br><a href="pemetaan/details/{{$y}}/'+feature.properties.Name+'">Details</a>');
            }
        }
        function zoomToFeature(e) {
            map.fitBounds(e.target.getBounds());
        }
        // ... our listeners
        {{--geojson = L.geoJson("http://geojson.kojyouproject.com/?kelurahan={{$kel}}");--}}

        // load GeoJSON from an external file
        $.getJSON("http://geojson.kojyouproject.com/?kelurahan={{$kel}}",function(data){
            // add GeoJSON layer to the map once the file is loaded
            geojson = L.geoJson(data,{
                style: function(feature) {
                    switch (feature.properties.Name) {
                        @forelse($puskes->kelurahan()->get() as $kelurahan)
                            <?php
                                if (isset($kelurahan->proyeksi()->where('tahun',$y)->first()->PP)&&isset($kelurahan->proyeksi()->where('tahun',$y)->first()->PL)){
                                    $d=$kelurahan->proyeksi()->where('tahun',$y)->first()->PL+$kelurahan->proyeksi()->where('tahun',$y)->first()->PP;
                                }
                                else{
                                    $d=0;
                                }
                                $d2=$sd->get()->where('location',$kelurahan->id)->count();
                                if ($d!=0){
                                    $cd=($d2/$d)*100;
                                }
                                else{
                                    $cd=0;
                                }
                            ?>
                            case '{{$kelurahan->name}}': return {
{{--                                color: "{{$cd >=90? 'green':'red'}}",--}}
                                fillColor: "{{$cd >=90? 'green':'red'}}",
                                weight: 2,
                                opacity: 1,
                                color: 'white',
                                dashArray: '3',
                                fillOpacity: 0.7,
                                popupContent : 'This is where the Rockies play!'

                            };
                            @empty
                        @endforelse
                    }
                },
                onEachFeature: onEachFeature
            }).addTo(map);
        });
        var legend = L.control({position: 'bottomright'});

        legend.onAdd = function (map) {

            var div = L.DomUtil.create('div', 'info legend');

            div.innerHTML +=
                '<i style="background:' + 'green' + '"></i> ' +
                'Memenuhi Target Imunisasi'+'<br>';
            div.innerHTML +=
                '<i style="background:' + 'yellow' + '"></i> ' +
                'Belum Memenuhi Target imunisasi'+'<br>';
            div.innerHTML +=
                '<i style="background:' + 'red' + '"></i> ' +
                'Belum Memenuhi Target IDL'+'<br>';

            return div;
        };

        legend.addTo(map);



    </script>


@endsection

@section('stylesheet')
    @parent
    <link href="{{asset('assets/apps/leaflet/leaflet.css')}}" rel="stylesheet" type="text/css" />
    <style>
        .info {
            padding: 6px 8px;
            font: 14px/16px Arial, Helvetica, sans-serif;
            background: white;
            background: rgba(255,255,255,0.8);
            box-shadow: 0 0 15px rgba(0,0,0,0.2);
            border-radius: 5px;
        }
        .info h4 {
            margin: 0 0 5px;
            color: #777;
        }
        .legend {
            line-height: 18px;
            color: #555;
        }
        .legend i {
            width: 18px;
            height: 18px;
            float: left;
            margin-right: 8px;
            opacity: 0.7;
        }
    </style>

@endsection