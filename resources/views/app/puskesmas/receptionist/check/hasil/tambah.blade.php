<div class="modal fade" id="pendaftaran{{$anak->id}}" tabindex="-1" role="basic" aria-hidden="true" style="margin-top: 10%">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title text-center">Tambahkan Nomor Pendaftaran</h4>
            </div>
            @if(isset($case))
            @if($case ==2)
                @php
                $anak = $data->id
                @endphp
            @else
                @php
                $anak = $anak->id
                @endphp
            @endif
                @else
                @php
                    $anak = $data->anak()->first()->id
                @endphp
            @endif
{{--            {{$anak}}--}}
            <form action="{{route('receptionist.regis',[$puskes,$anak])}}" class="form-horizontal margin-bottom-40" role="form" method="post">
                <div class="modal-body">
                    <div class="form-group form-md-line-input">
                        <label for="inputEmail1" class="col-md-4 control-label">No.Pendaftaran</label>
                        <div class="col-md-8">
                            <input type="text" class="form-control" name="pendaftaran" id="form_control_1" placeholder="No Pendaftaran">
                            <div class="form-control-focus"> </div>
                        </div>
                    </div>
                {{csrf_field()}}
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn green">Tambah</button>
                    <button type="button" class="btn dark btn-outline" data-dismiss="modal">Kembali</button>
                </div>
            </form>
        </div>
    </div>

</div>

@section('stylesheet')
    @parent
    <style>
        .dashboard-stat .details .number{
            font-size: 30px !important;
        }
        a.decor{
            text-decoration: none;
        }
    </style>
@endsection
@section('javascript')
    @parent
    <script src="{{asset('assets/global/plugins/jquery-ui/jquery-ui.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/pages/scripts/ui-modals.min.js')}}" type="text/javascript"></script>
@endsection