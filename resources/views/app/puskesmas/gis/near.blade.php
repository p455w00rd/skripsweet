@extends('layouts.layout.app')

@section('content')

    <div class="page-content">
        <!-- BEGIN PAGE HEAD-->
        <div class="page-head">
            <!-- BEGIN PAGE TITLE -->
            <div class="page-title">
                <h1>New User Profile | Account
                    <small>user account page</small>
                </h1>
            </div>
            <!-- END PAGE TITLE -->
        </div>
        <!-- END PAGE HEAD-->
        <!-- BEGIN PAGE BREADCRUMB -->
        <ul class="page-breadcrumb breadcrumb">
            <li>
                <a href="index.html">Home</a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <span class="active">User</span>
            </li>
        </ul>
        <!-- END PAGE BREADCRUMB -->
        <!-- BEGIN PAGE BASE CONTENT -->
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN PROFILE SIDEBAR -->
                <div class="portlet light bordered">
                    <div class="portlet-title tabbable-line">
                        <div class="caption caption-md">
                            <i class="icon-globe theme-font hide"></i>
                            <span class="caption-subject font-blue-madison bold uppercase">Puskesmas {{$puskes->name}}</span>
                        </div>
                    </div>
                    <div class="portlet-body form">
                        {{--@forelse($dist as $dist)--}}

                            {{--{{$dist->parent()->first()->name}}--}}
                        {{--@empty--}}
                        {{--@endforelse--}}
                        @if($near!=null)
                        <div id="map" style="height: 80%"></div>
                        @else
                            <h2>Pilih Lokasi Puskesmas terlebih dahulu</h2>
                            <h4><a href="{{route('puskesmas.edit',[$puskes->id])}}">klik disini</a></h4>
                            @endif

                    </div>
                </div>
                <!-- END PROFILE CONTENT -->
            </div>
        </div>
        <!-- END PAGE BASE CONTENT -->
    </div>

@endsection

@section('stylesheet')
    @parent
    <link href="{{asset('assets/apps/leaflet/leaflet.css')}}" rel="stylesheet" type="text/css" />

@endsection

@section('javascript')
    @parent

    <script src="{{asset('assets/apps/leaflet/leaflet.js')}}" type="text/javascript"></script>
    <script>
        $('.profile').initial();
    </script>
    @if($near!=null)
    <script>
        var map = L.map('map',{
            center: [{{$near->getLat()}}, {{$near->getLng()}}],
            zoom: 15
        });
        L.marker([{{$near->getLat()}}, {{$near->getLng()}}]).addTo(map)
            .bindPopup('<b>{{$puskes->name}}</b>')
            .openPopup();
        L.circle([{{$near->getLat()}}, {{$near->getLng()}}], {
            color: 'red',
            fillColor: '#f03',
            fillOpacity: 0.5,
            radius: 2000
        }).addTo(map);

        var someFeatures = [
            @forelse($dist as $dist)
            {
            "type": "Feature",
            "properties": {
                "name": "{{$dist->parent()->first()->name}}"
            },
                "geometry": {
                    "type": "Point",
                    "coordinates": [{{$dist->location->getLng()}},{{$dist->location->getLat()}}]
                }
            },
            @empty
            @endforelse
        ];
        function onEachFeature(feature, layer) {
            // does this feature have a property named popupContent?
            if (feature.properties && feature.properties.name) {
                layer.bindPopup(feature.properties.name);
            }
        }

        L.geoJSON(someFeatures, {
            onEachFeature: onEachFeature
        }).addTo(map);
        //map rendering
        L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', {
            attribution: '&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
        }).addTo(map);


    </script>
    @endif
@endsection
