@extends('layouts.layout.app')

@section('content')

    <div class="page-content">
        <!-- BEGIN PAGE HEAD-->
        <div class="page-head">
            <!-- BEGIN PAGE TITLE -->
            <div class="page-title">
                <h1>Dashboard
                </h1>
            </div>
            <!-- END PAGE TITLE -->

        </div>
        <!-- END PAGE HEAD-->

        <!-- BEGIN PAGE BASE CONTENT -->
        <div class="row widget-row">
            <div class="col-md-3">
                <!-- BEGIN WIDGET THUMB -->
                <div class="widget-thumb widget-bg-color-white text-uppercase margin-bottom-20 bordered">
                    <h4 class="widget-thumb-heading">Total Pendaftar</h4>
                    <div class="widget-thumb-wrap">
                        <i class="widget-thumb-icon bg-green icon-bulb"></i>
                        <div class="widget-thumb-body">
                            <span class="widget-thumb-subtitle">Tahun Ini</span>
                            <span class="widget-thumb-body-stat" data-counter="counterup" data-value="7,644">{{$anakT}}</span>
                        </div>
                    </div>
                </div>
                <!-- END WIDGET THUMB -->
            </div>
            <div class="col-md-3">
                <!-- BEGIN WIDGET THUMB -->
                <div class="widget-thumb widget-bg-color-white text-uppercase margin-bottom-20 bordered">
                    <h4 class="widget-thumb-heading">Total Pendaftar</h4>
                    <div class="widget-thumb-wrap">
                        <i class="widget-thumb-icon bg-red icon-layers"></i>
                        <div class="widget-thumb-body">
                            <span class="widget-thumb-subtitle">Bulan Ini</span>
                            <span class="widget-thumb-body-stat" data-counter="counterup" data-value="1,293">{{$anakB}}</span>
                        </div>
                    </div>
                </div>
                <!-- END WIDGET THUMB -->
            </div>
            <div class="col-md-3">
                <!-- BEGIN WIDGET THUMB -->
                <div class="widget-thumb widget-bg-color-white text-uppercase margin-bottom-20 bordered">
                    <h4 class="widget-thumb-heading">Total Kedatangan</h4>
                    <div class="widget-thumb-wrap">
                        <i class="widget-thumb-icon bg-purple icon-screen-desktop"></i>
                        <div class="widget-thumb-body">
                            <span class="widget-thumb-subtitle">Bulan Ini</span>
                            <span class="widget-thumb-body-stat" data-counter="counterup" data-value="815">{{$datang}}</span>
                        </div>
                    </div>
                </div>
                <!-- END WIDGET THUMB -->
            </div>
            <div class="col-md-3">
                <!-- BEGIN WIDGET THUMB -->
                <div class="widget-thumb widget-bg-color-white text-uppercase margin-bottom-20 bordered">
                    <h4 class="widget-thumb-heading">Total Pegawai</h4>
                    <div class="widget-thumb-wrap">
                        <i class="widget-thumb-icon bg-blue icon-bar-chart"></i>
                        <div class="widget-thumb-body">
                            <span class="widget-thumb-subtitle">Pegawai</span>
                            <span class="widget-thumb-body-stat" data-counter="counterup" data-value="5,071">{{$pegawai}}</span>
                        </div>
                    </div>
                </div>
                <!-- END WIDGET THUMB -->
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 col-sm-6">
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption font-green">
                            <span class="caption-subject bold uppercase">Grafik Pendatang</span>
                            <span class="caption-helper">Setiap 7 Hari...</span>
                        </div>
                    </div>
                    <div class="portlet-body">
                        <div id="highchart_1" style="height:300px;"></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 col-sm-6">
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption">
                            <span class="caption-subject bold uppercase font-red">Panggilan Kembali</span>
                            <span class="caption-helper">Sampai 7 Hari Kemudian...</span>
                        </div>
                        <div class="actions">

                            <a class="btn green-haze btn-outline btn-circle btn-sm fullscreen" href="#"> Details <i class="icon-size-fullscreen"></i> </a>
                        </div>
                    </div>
                    <div class="portlet-body">
                        <table class="table table-bordered table-striped"id="sample_1" style="width: 200px!important;">
                            <thead>
                            <tr>
                                <td >nama Anak</td>
                                <td >Nama Ortu</td>
                                <td >Tanggal Kembali</td>
                                <td >Telepone</td>
                                <td>Alamat</td>
                                <td>Action</td>
                            </tr>
                            </thead>
                            <tbody>
                            @forelse($resB as $res)
                                <tr>
                                    <td>{{$res->anak()->first()->name}}</td>
                                    <td>{{$res->anak()->first()->ortu()->first()->name}}</td>
                                    <td>{{$res->tanggal_kembali}} <span class="label label-success">{{str_replace(['dari sekarang'], ['lagi'], $res->diff())}}</span></td>
                                    <td>{{$res->anak()->first()->ortu()->first()->phone}}</td>
                                    <td>{{$res->anak()->first()->address}}</td>
                                    <td><a href="" class="btn red">Delete</a></td>
                                </tr>
                            @empty
                            @endforelse
                            @forelse($resA as $res)
                                <tr>
                                    <td>{{$res->anak()->first()->name}}</td>
                                    <td>{{$res->anak()->first()->ortu()->first()->name}}</td>
                                    <td>{{$res->tanggal_kembali}} <span class="label label-danger">Lewat {{$res->diff()}}</span></td>
                                    <td>{{$res->anak()->first()->ortu()->first()->phone}}</td>
                                    <td>{{$res->anak()->first()->address}}</td>
                                    <td><a href="" class="btn red">Delete</a></td>
                                </tr>
                            @empty
                            @endforelse
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>


        <!-- END PAGE BASE CONTENT -->
    </div>

@endsection
@section('stylesheet')
    @parent

    <link href="{{asset('assets/global/plugins/datatables/datatables.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css')}}" rel="stylesheet" type="text/css" />
    <style>
        /*.table-bordered{*/
            /*width: 200px !important;*/
        /*}*/
        /*div.dataTables_wrapper{*/
        /*width: 535px !important;*/
        /*}*/
    </style>
@stop

@section('javascript')
    @parent

    <script src="{{asset('assets/global/scripts/datatable.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/global/plugins/datatables/datatables.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/global/plugins/highcharts/js/highcharts.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/pages/scripts/charts-highcharts.min.js')}}" type="text/javascript"></script>
    <script>
        var table = $('#sample_1');

        var oTable = table.dataTable({
            // "columnDefs": [
            //     { "width": "100%", "targets": 0 }
            // ],

            // Internationalisation. For more info refer to http://datatables.net/manual/i18n
            "language": {
                "aria": {
                    "sortAscending": ": activate to sort column ascending",
                    "sortDescending": ": activate to sort column descending"
                },
                "emptyTable": "No data available in table",
                "info": "Showing _START_ to _END_ of _TOTAL_ entries",
                // "infoEmpty": "No entries found",
                "infoFiltered": "(filtered1 from _MAX_ total entries)",
                "lengthMenu": "_MENU_ entries",
                "search": "Search:",
                "zeroRecords": "No matching records found"
            },


            // setup responsive extension: http://datatables.net/extensions/responsive/
            // responsive: true,

            "ordering": false,
            //"paging": false, disable pagination

//            "order": [
//                [0, 'asc']
//            ],

            "lengthMenu": [
                [5, 10, 15, 20, -1],
                [5, 10, 15, 20, "All"] // change per page values here
            ],
            // set the initial value
            "pageLength": 5,


            "dom": "<'row' <'col-md-12'>><'row'<'col-md-6 col-sm-12'><'col-md-6 col-sm-12'>r><'table-scrollable't><'row'<'col-md-5 col-sm-12'><'col-md-7 col-sm-12'p>>", // horizobtal scrollable datatable

            // Uncomment below line("dom" parameter) to fix the dropdown overflow issue in the datatable cells. The default datatable layout
            // setup uses scrollable div(table-scrollable) with overflow:auto to enable vertical scroll(see: assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js).
            // So when dropdowns used the scrollable div should be removed.
            //"dom": "<'row' <'col-md-12'T>><'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r>t<'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>",
        });
    </script>
    <script>
        jQuery(document).ready(function() {
            $('#highchart_1').highcharts({
                chart : {
                    style: {
                        fontFamily: 'Open Sans'
                    }
                },
                title: {
                    text: 'Grafik Kedatangan Anak {{$puskes->name}}',
                    x: -20 //center
                },
                subtitle: {
                    text: 'Per 7 Hari',
                    x: -20
                },
                xAxis: {
                    categories: [
                    @for($i = 0; $i < 7; $i++)
                            '{{str_replace(['-'], [''], $i-7)}} Minggu Lalu',
                    @endfor
                        'Minggu Ini'
                    ]
                },
                yAxis: {
                    title: {
                        text: 'Anak'
                    },
                    plotLines: [{
                        value: 0,
                        width: 1,
                        color: '#808080'
                    }]
                },
                tooltip: {
                    valueSuffix: ' Anak'
                },
                legend: {
                    layout: 'vertical',
                    align: 'right',
                    verticalAlign: 'middle',
                    borderWidth: 0
                },
                series: [{
                    name: 'Kedatangan',
                    data: [
                        @foreach(array_reverse($graf) as $graf)
                        {{$graf}},
                        @endforeach
                        {{$graf0}}
                    ]
                }]
            });
        });
    </script>
    {{--    <script src="{{asset('assets/pages/scripts/table-datatables-buttons.min.js')}}" type="text/javascript"></script>--}}
@stop