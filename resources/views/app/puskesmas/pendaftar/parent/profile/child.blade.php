@extends('layouts.layout.app')

@section('content')

    <div class="page-content">
        <!-- BEGIN PAGE HEAD-->
        <div class="page-head">
            <!-- BEGIN PAGE TITLE -->
            <div class="page-title">
                <h1>New User Profile | Account
                    <small>user account page</small>
                </h1>
            </div>
            <!-- END PAGE TITLE -->
        </div>
        <!-- END PAGE HEAD-->
        <!-- BEGIN PAGE BREADCRUMB -->
        <ul class="page-breadcrumb breadcrumb">
            <li>
                <a href="index.html">Home</a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <span class="active">User</span>
            </li>
        </ul>
        <!-- END PAGE BREADCRUMB -->
        <!-- BEGIN PAGE BASE CONTENT -->
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN PROFILE SIDEBAR -->
                <div class="profile-sidebar">
                    <!-- PORTLET MAIN -->
                    <div class="portlet light bordered profile-sidebar-portlet">
                        <!-- SIDEBAR USERPIC -->
                        <div class="profile-userpic">
                            <img data-name="{{$ortu->name}}" class="img-responsive profile" alt=""> </div>
                        <!-- END SIDEBAR USERPIC -->
                        <!-- SIDEBAR USER TITLE -->
                        <div class="profile-usertitle">
                            <div class="profile-usertitle-name"> {{$ortu->name}} </div>
                            <div class="profile-usertitle-job"> {{$ortu->user()->first()->role()->first()->type}} </div>
                        </div>
                        @include('app.puskesmas.pendaftar.parent.profile.sidebar')
                    </div>


                    <!-- END PORTLET MAIN -->
                    <!-- PORTLET MAIN -->

                    <!-- END PORTLET MAIN -->
                </div>
                <!-- END BEGIN PROFILE SIDEBAR -->
                <!-- BEGIN PROFILE CONTENT -->
                <div class="profile-content">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="portlet light bordered">
                                <div class="portlet-title tabbable-line">
                                    <div class="caption caption-md">
                                        <i class="icon-globe theme-font hide"></i>
                                        <span class="caption-subject font-blue-madison bold uppercase">Data Anak</span>
                                    </div>
                                </div>
                                <div class="portlet-body">
                                    <div id="accordion1" class="panel-group">
                                        @forelse($anaks as $anak)
                                            <div class="panel panel-default">
                                                <div class="panel-heading">
                                                    <h4 class="panel-title">
                                                        <a class="accordion-toggle accordion-toggle-styled" data-toggle="collapse" data-parent="#accordion1" href="#{{$anak->id}}">Nama anak : {{$anak->name}} </a>
                                                    </h4>
                                                </div>
                                                <div id="{{$anak->id}}" class="panel-collapse {{$loop->first == true ? 'in' : 'collapse'}}">
                                                    <div class="panel-body form">
                                                        <form class="form-horizontal" role="form">
                                                            <div class="form-group">
                                                                <label class="control-label col-md-4">NIK Anak :</label>
                                                                <div class="col-md-8">
                                                                    <p class="form-control-static"> {{$anak->nik}} </p>
                                                                </div>
                                                                @if(Auth::user()->role()->whereNotIn('users_roles.id',[2,4])->get()->count() != 0 )
                                                                @else
                                                                    @if(isset($puskes->id))
                                                                    <label class="control-label col-md-4">Nomor Pendaftaran :</label>
                                                                    <div class="col-md-8">
{{--                                                                        {{$anak->anak}}--}}

                                                                        <p class="form-control-static"> {{$anak->no()->where('id_puskes',$puskes->id)->first()==null?'Belum Terdaftar':$anak->no()->where('id_puskes',$puskes->id)->first()->no_regis}} </p>

                                                                    </div>
                                                                    @else
                                                                    @endif
                                                                @endif

                                                                <label class="control-label col-md-4">Nama Lengkap :</label>
                                                                <div class="col-md-8">
                                                                    <p class="form-control-static"> {{$anak->name}} </p>
                                                                </div>
                                                                <label class="control-label col-md-4">Tempat, Tanggal Lahir :</label>
                                                                <div class="col-md-8">
                                                                    <p class="form-control-static"> {{$anak->pob}}, {{date('d-m-Y', strtotime($anak->dob))}} </p>
                                                                </div>
                                                                <label class="control-label col-md-4">Umur :</label>
                                                                <div class="col-md-8">
                                                                    <p class="form-control-static"> {{str_replace(['yang lalu','dari sekarang'], [''], $anak->diff())}} </p>
                                                                </div>
                                                                <label class="control-label col-md-4">Imunisasi Terakhir :</label>
                                                                <div class="col-md-8">
                                                                    <p class="form-control-static">
                                                                        @if($anak->history_imun()->get()->last() != null)
                                                                            {{$anak->history_imun()->get()->last()->imun()->first()->name}}
                                                                        @else
                                                                            Belum Imunisasi
                                                                        @endif
                                                                    </p>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-md-6">
                                                                    <div class="row">
                                                                        <div class="col-md-offset-3 col-md-8">
                                                                            @if(isset($puskes->id))
                                                                            <a href="{{route('anak.profile',[$puskes->id,$anak->id])}}" type="submit" class="btn green">
                                                                                <i class="icon-magnifier"></i> Details</a>
                                                                                @else
                                                                                <a href="{{route('ortu.anak.profile',[$id,$anak->id])}}" type="submit" class="btn green">
                                                                                    <i class="icon-magnifier"></i> Details</a>
                                                                            @endif
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>

                                        @empty
                                        @endforelse
                                    </div>
                                    @if(isset($puskes->id))
                                    <a href="{{route('anak.create',[$puskes->id,$ortu->id])}}" class="btn blue">
                                        <i class="fa fa-plus"></i> Tambah Anak </a>
                                        @else
                                        <a href="{{route('ortu.anak.create',[$ortu->id])}}" class="btn blue pull-left">
                                            <i class="fa fa-plus"></i> Tambah Anak </a>
                                    @endif
                                    <div class="pull-right">
                                        {{ $anaks->links() }}
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- END PROFILE CONTENT -->
            </div>
        </div>
        <!-- END PAGE BASE CONTENT -->
    </div>

@endsection

@section('stylesheet')
    @parent
    <link href="{{asset('assets/pages/css/profile.min.css')}}" rel="stylesheet" type="text/css" />
    <style>
        .pagination
        {
            margin: 0px !important;
        }
    </style>
@endsection

@section('javascript')
    @parent
    <script src="{{asset('assets/global/plugins/jquery.sparkline.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/pages/scripts/profile.min.js')}}" type="text/javascript"></script>
    <script>
        $('.profile').initial();
    </script>
@endsection