<!-- SIDEBAR MENU -->
<div class="profile-usermenu">
    <ul class="nav">
        @if(isset($puskes->id))
        <li>
            <a href="{{route('ortu.profile',[$puskes->id,$ortu->id])}}">
                <i class="icon-home"></i> Overview </a>
        </li>
        <li>
            <a href="{{route('ortu.child',[$puskes->id,$ortu->id])}}">
                <i class="icon-users"></i> Anak </a>
        </li>
        <li>
            <a href="{{route('ortu.map',[$puskes->id,$ortu->id])}}">
                <i class="icon-map"></i> Ubah Peta </a>
        </li>
        <li>
            <a href="{{route('ortu.account',[$puskes->id,$ortu->id])}}">
                <i class="icon-settings"></i> Account Settings </a>
        </li>
            @else
            <li>
                <a href="{{route('ortu.profile.me',[$id])}}">
                    <i class="icon-home"></i> Overview </a>
            </li>
            <li>
                <a href="{{route('ortu.anak.me',[$ortu->id])}}">
                    <i class="icon-users"></i> Anak </a>
            </li>
            <li>
                <a href="{{route('ortu.map.me',[$ortu->id])}}">
                    <i class="icon-map"></i> Ubah Peta </a>
            </li>
            <li>
                <a href="{{route('ortu.account.me',[$ortu->id])}}">
                    <i class="icon-settings"></i> Account Settings </a>
            </li>
        @endif
    </ul>
</div>
<!-- END MENU -->