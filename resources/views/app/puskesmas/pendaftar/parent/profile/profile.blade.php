@extends('layouts.layout.app')

@section('content')

    <div class="page-content">
        <!-- BEGIN PAGE HEAD-->
        <div class="page-head">
            <!-- BEGIN PAGE TITLE -->
            <div class="page-title">
                <h1>New User Profile | Account
                    <small>user account page</small>
                </h1>
            </div>
            <!-- END PAGE TITLE -->
        </div>
        <!-- END PAGE HEAD-->
        <!-- BEGIN PAGE BREADCRUMB -->
        <ul class="page-breadcrumb breadcrumb">
            <li>
                <a href="index.html">Home</a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <span class="active">User</span>
            </li>
        </ul>
        <!-- END PAGE BREADCRUMB -->
        <!-- BEGIN PAGE BASE CONTENT -->
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN PROFILE SIDEBAR -->
                <div class="profile-sidebar">
                    <!-- PORTLET MAIN -->
                    <div class="portlet light bordered profile-sidebar-portlet">
                        <!-- SIDEBAR USERPIC -->
                        <div class="profile-userpic">
                            <img data-name="{{$ortu->name}}" class="img-responsive profile" alt="">
                        </div>
                        <!-- END SIDEBAR USERPIC -->
                        <!-- SIDEBAR USER TITLE -->
                        <div class="profile-usertitle">
                            <div class="profile-usertitle-name"> {{$ortu->name}} </div>
                            <div class="profile-usertitle-job"> {{$ortu->user()->first()->role()->first()->type}} </div>
                        </div>
                        @include('app.puskesmas.pendaftar.parent.profile.sidebar')
                    </div>


                    <!-- END PORTLET MAIN -->
                    <!-- PORTLET MAIN -->

                    <!-- END PORTLET MAIN -->
                </div>
                <!-- END BEGIN PROFILE SIDEBAR -->
                <!-- BEGIN PROFILE CONTENT -->
                <div class="profile-content">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="portlet light bordered">
                                <div class="portlet-title tabbable-line">
                                    <div class="caption caption-md">
                                        <i class="icon-globe theme-font hide"></i>
                                        <span class="caption-subject font-blue-madison bold uppercase">Profile Orang Tua</span>
                                    </div>
                                </div>
                                <div class="portlet-body form">
                                    <form class="form-horizontal form-bordered form-label-stripped" role="form">
                                        <div class="form-group">
                                            <label class="control-label col-md-3">NIK:</label>
                                            <div class="col-md-9">
                                                <p class="form-control-static"> {{$ortu->nik}} </p>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Nama Lengkap:</label>
                                            <div class="col-md-9">
                                                <p class="form-control-static"> {{$ortu->name}} </p>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Alamat:</label>
                                            <div class="col-md-9">
                                                <p class="form-control-static"> {{$ortu->address}} </p>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3">No. Telephone:</label>
                                            <div class="col-md-9">
                                                <p class="form-control-static"> {{$ortu->phone}} </p>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Wilayah Kecamatan:</label>
                                            <div class="col-md-9">
                                                <p class="form-control-static">
                                                    @if($ortu->location != null)
                                                    {{$ortu->kecamatan()->first()->name}}
                                                        @endif
                                                </p>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Lokasi Ortu:</label>
                                            <div class="col-md-9">
                                                <div id="map" style="height: 60%"></div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12 text-center">
                                                <a class="btn green margin-top-20" data-toggle="modal" href="#ubah">
                                                    <i class="fa fa-pencil"></i> Ubah</a>
                                            </div>
                                        </div>
                                    </form>
                                    <div class="modal fade" id="ubah" tabindex="-1" role="ubah" aria-hidden="true">
                                        <div class="modal-dialog">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                                                    <h4 class="modal-title">Ubah </h4>
                                                </div>
                                                @if(isset($puskes->id))
                                                    <form action="{{route('ortu.profile',[$puskes->id,$ortu->id])}}" class="form-horizontal" id="form_sample_1" method="post">
                                                    @else
                                                            <form action="{{route('ortu.profile.update',[$ortu->id])}}" class="form-horizontal" id="form_sample_1" method="post">
                                                    @endif
                                                    <div class="modal-body">
                                                        <div class="form-group form-md-line-input">
                                                            <label class="col-md-3 control-label" for="form_control_1">NIK
                                                                <span class="required">*</span>
                                                            </label>
                                                            <div class="col-md-9">
                                                                <input type="text" class="form-control" name="nik" readonly placeholder="" value="{{ $ortu->nik }}">
                                                                <div class="form-control-focus"> </div>
                                                                <span class="help-block">{{ $errors->has('nik') ? $error : 'Masukkan NIK Orang Tua' }}</span>
                                                            </div>
                                                        </div>
                                                        <div class="form-group form-md-line-input {{ $errors->has('name-parent') ? 'has-error' : '' }}">
                                                            <label class="col-md-3 control-label" for="form_control_1">Nama
                                                                <span class="required">*</span>
                                                            </label>
                                                            <div class="col-md-9">
                                                                <input type="text" class="form-control" name="name-parent" placeholder="" value="{{ $ortu->name }}">
                                                                <div class="form-control-focus"> </div>
                                                                <span class="help-block">{{ $errors->has('name-parent') ? $error : 'Masukkan Nama Orang Tua' }}</span>
                                                            </div>
                                                        </div>
                                                        <div class="form-group form-md-line-input {{ $errors->has('phone') ? 'has-error' : '' }}">
                                                            <label class="col-md-3 control-label" for="form_control_1">telephone
                                                                <span class="required">*</span>
                                                            </label>
                                                            <div class="col-md-9">
                                                                <input class="form-control" name="phone" value="{{$ortu->phone}}"/>
                                                                <div class="form-control-focus"> </div>
                                                                <span class="help-block">{{ $errors->has('phone') ? $error : 'Masukkan nomor Telephone Orang Tua' }}</span>
                                                            </div>
                                                        </div>
                                                        <div class="form-group form-md-line-input {{ $errors->has('address-parent') ? 'has-error' : '' }}">
                                                            <label class="col-md-3 control-label" for="form_control_1">Alamat
                                                                <span class="required">*</span>
                                                            </label>
                                                            <div class="col-md-9">
                                                                <textarea class="form-control" name="address-parent" rows="3">{{ $ortu->address }}</textarea>
                                                                <div class="form-control-focus"> </div>
                                                                <span class="help-block">{{ $errors->has('address-parent') ? $error : 'Masukkan Alamat Orang Tua' }}</span>
                                                            </div>
                                                        </div>
                                                        <div class="form-group form-md-line-input {{ $errors->has('location') ? 'has-error' : '' }}">
                                                            <label class="col-md-3 control-label" for="form_control_1">Kecamatan
                                                                <span class="required">*</span>
                                                            </label>
                                                            <div class="col-md-9">
                                                                <select class="bs-select form-control" name="location" data-live-search="true" data-size="8">
                                                                    <option value="">Pilih Kecamatan</option>
                                                                    @forelse($kecamatan as $kecamatan)
                                                                    <option {{ $ortu->location == $kecamatan->id? 'selected' : '' }} value="{{$kecamatan->id}}">{{$kecamatan->name}}</option>
                                                                    @empty
                                                                    Tidak Ada Data
                                                                    @endforelse
                                                                </select>
                                                                <div class="form-control-focus"> </div>
                                                                <span class="help-block">{{ $errors->has('location') ? $error : 'Pilih Kecamatan' }}</span>
                                                            </div>
                                                        </div>
                                                        {{ csrf_field() }}
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn dark btn-outline" data-dismiss="modal">Close</button>
                                                        <button type="submit" class="btn green">Save changes</button>
                                                    </div>
                                                </form>
                                            </div>
                                            <!-- /.modal-content -->
                                        </div>
                                        <!-- /.modal-dialog -->
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- END PROFILE CONTENT -->
            </div>
        </div>
        <!-- END PAGE BASE CONTENT -->
    </div>

@endsection

@section('stylesheet')
    @parent
    <link href="{{asset('assets/global/plugins/bootstrap-select/css/bootstrap-select.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('assets/apps/leaflet/leaflet.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('assets/pages/css/profile.min.css')}}" rel="stylesheet" type="text/css" />
@endsection

@section('javascript')
    @parent
    <script src="{{asset('assets/global/plugins/jquery.sparkline.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/pages/scripts/profile.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/global/plugins/bootstrap-select/js/bootstrap-select.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/pages/scripts/components-bootstrap-select.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/apps/leaflet/leaflet.js')}}" type="text/javascript"></script>
    <script>
        $('.profile').initial();
    </script>
    <script>
        var map = L.map('map',{
            @if(isset($ortu->place()->first()->id))
            center: [{{$ortu->place()->first()->location->getLat()}}, {{$ortu->place()->first()->location->getLng()}}],
            @else
            center: [-7.253339, 112.772028],
            @endif
            zoom: 15
        });


        //map rendering
        L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', {
            attribution: '&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
        }).addTo(map);

        @if(isset($ortu->place()->first()->id))
        L.marker([{{$ortu->place()->first()->location->getLat()}}, {{$ortu->place()->first()->location->getLng()}}]).addTo(map)
            .bindPopup('<b>{{$ortu->name}}</b><br> {{$ortu->address}}')
            .openPopup();
        @endif


    </script>
@endsection