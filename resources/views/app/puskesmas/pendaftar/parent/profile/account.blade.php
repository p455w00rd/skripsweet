@extends('layouts.layout.app')

@section('content')

    <div class="page-content">
        <!-- BEGIN PAGE HEAD-->
        <div class="page-head">
            <!-- BEGIN PAGE TITLE -->
            <div class="page-title">
                <h1>New User Profile | Account
                    <small>user account page</small>
                </h1>
            </div>
            <!-- END PAGE TITLE -->
        </div>
        <!-- END PAGE HEAD-->
        <!-- BEGIN PAGE BREADCRUMB -->
        <ul class="page-breadcrumb breadcrumb">
            <li>
                <a href="index.html">Home</a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <span class="active">User</span>
            </li>
        </ul>
        <!-- END PAGE BREADCRUMB -->
        <!-- BEGIN PAGE BASE CONTENT -->
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN PROFILE SIDEBAR -->
                <div class="profile-sidebar">
                    <!-- PORTLET MAIN -->
                    <div class="portlet light bordered profile-sidebar-portlet">
                        <!-- SIDEBAR USERPIC -->
                        <div class="profile-userpic">
                            <img data-name="{{$ortu->name}}" class="img-responsive profile" alt=""> </div>
                        <!-- END SIDEBAR USERPIC -->
                        <!-- SIDEBAR USER TITLE -->
                        <div class="profile-usertitle">
                            <div class="profile-usertitle-name"> {{$ortu->name}} </div>
                            <div class="profile-usertitle-job"> {{$ortu->user()->first()->role()->first()->type}} </div>
                        </div>
                        @include('app.puskesmas.pendaftar.parent.profile.sidebar')
                    </div>


                    <!-- END PORTLET MAIN -->
                    <!-- PORTLET MAIN -->

                    <!-- END PORTLET MAIN -->
                </div>
                <!-- END BEGIN PROFILE SIDEBAR -->
                <!-- BEGIN PROFILE CONTENT -->
                <div class="profile-content">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="portlet light bordered">
                                <div class="portlet-title tabbable-line">
                                    <div class="caption caption-md">
                                        <i class="icon-globe theme-font hide"></i>
                                        <span class="caption-subject font-blue-madison bold uppercase">Edit Akun</span>
                                    </div>
                                </div>
                                <div class="portlet-body">
                                    @if(isset($puskes->id))
                                    <form action="{{route('ortu.account',[$puskes->id,$ortu->id])}}" class="form-horizontal" id="form_sample_1" method="post">
                                    @else
                                            <form action="{{route('ortu.account.me',[$ortu->id])}}" class="form-horizontal" id="form_sample_1" method="post">
                                        @endif
                                        <div class="form-body">
                                            {{--<div class="alert alert-danger display-hide">--}}
                                            {{--<button class="close" data-close="alert"></button> You have some form errors. Please check below. </div>--}}
                                            {{--<div class="alert alert-success display-hide">--}}
                                            {{--<button class="close" data-close="alert"></button> Your form validation is successful! </div>--}}
                                            {{--<h4 class="form-title-h4">Account</h4>--}}

                                            <div class="form-group form-md-line-input {{ $errors->has('username') ? 'has-error' : '' }}">
                                                <label class="col-md-3 control-label" for="form_control_1">Username
                                                    <span class="required">*</span>
                                                </label>
                                                <div class="col-md-9">

                                                    <input type="text" class="form-control" readonly placeholder="" name="username"  value="{{$ortu->user()->first()->username}}">
                                                    <div class="form-control-focus"> </div>
                                                    <span class="help-block">{{ $errors->has('username') ? $error : 'masukkan Username' }}</span>
                                                </div>
                                            </div>
                                            <div class="form-group form-md-line-input {{ $errors->has('email') ? 'has-error' : '' }}">
                                                <label class="col-md-3 control-label" for="form_control_1">Email
                                                    <span class="required">*</span>
                                                </label>
                                                <div class="col-md-9">
                                                    <input type="email" class="form-control" placeholder="" name="email" value="{{$ortu->user()->first()->email}}">
                                                    <div class="form-control-focus"> </div>
                                                    <span class="help-block">{{ $errors->has('email') ? $error : 'Masukkan Email' }}</span>
                                                </div>
                                            </div>

                                            <div class="form-group form-md-line-input {{ $errors->has('password_confirm') ? 'has-error' : '' }}">
                                                <label class="col-md-3 control-label" for="form_control_1">Password
                                                    <span class="required">*</span>
                                                </label>
                                                <div class="col-md-9">
                                                    <input type="password" class="form-control" placeholder="" name="password" value="">
                                                    <div class="form-control-focus"> </div>
                                                    <span class="help-block">{{ $errors->has('password_confirmation') ? $error : 'Ketik Ulang Password untuk ubah Password' }}</span>
                                                </div>
                                            </div>
                                            <div class="form-group form-md-line-input {{ $errors->has('password_confirm') ? 'has-error' : '' }}">
                                                <label class="col-md-3 control-label" for="form_control_1">Ulangi Password
                                                    <span class="required">*</span>
                                                </label>
                                                <div class="col-md-9">
                                                    <input type="password" class="form-control" placeholder="" name="password_confirm" value="">
                                                    <div class="form-control-focus"> </div>
                                                    <span class="help-block">{{ $errors->has('password_confirm') ? $error : 'Masukkan kembali Password Baru' }}</span>
                                                </div>
                                            </div>
                                            <hr>


                                            <input type="hidden" name="id_user" value="{{ $ortu->user()->first()->id}}">
                                                <input type="hidden" name="_token" value="{{ csrf_token()}}">
                                        </div>
                                        <div class="form-actions">
                                            <div class="row">
                                                <div class="col-md-offset-3 col-md-9">
                                                    <button type="submit" class="btn green">Ubah</button>
                                                    <button type="reset" class="btn default">Batal</button>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- END PROFILE CONTENT -->
            </div>
        </div>
        <!-- END PAGE BASE CONTENT -->
    </div>

@endsection

@section('stylesheet')
    @parent
    <link href="{{asset('assets/pages/css/profile.min.css')}}" rel="stylesheet" type="text/css" />
@endsection

@section('javascript')
    @parent
    <script src="{{asset('assets/global/plugins/jquery.sparkline.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/pages/scripts/profile.min.js')}}" type="text/javascript"></script>
    <script>
        $('.profile').initial();
    </script>
@endsection