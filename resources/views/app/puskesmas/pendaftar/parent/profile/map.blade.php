@extends('layouts.layout.app')

@section('content')

    <div class="page-content">
        <!-- BEGIN PAGE HEAD-->
        <div class="page-head">
            <!-- BEGIN PAGE TITLE -->
            <div class="page-title">
                <h1>New User Profile | Account
                    <small>user account page</small>
                </h1>
            </div>
            <!-- END PAGE TITLE -->
        </div>
        <!-- END PAGE HEAD-->
        <!-- BEGIN PAGE BREADCRUMB -->
        <ul class="page-breadcrumb breadcrumb">
            <li>
                <a href="index.html">Home</a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <span class="active">User</span>
            </li>
        </ul>
        <!-- END PAGE BREADCRUMB -->
        <!-- BEGIN PAGE BASE CONTENT -->
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN PROFILE SIDEBAR -->
                <div class="profile-sidebar">
                    <!-- PORTLET MAIN -->
                    <div class="portlet light bordered profile-sidebar-portlet">
                        <!-- SIDEBAR USERPIC -->
                        <div class="profile-userpic">
                            <img data-name="{{$ortu->name}}" class="img-responsive profile" alt="">
                        </div>
                        <!-- END SIDEBAR USERPIC -->
                        <!-- SIDEBAR USER TITLE -->
                        <div class="profile-usertitle">
                            <div class="profile-usertitle-name"> {{$ortu->name}} </div>
                            <div class="profile-usertitle-job"> {{$ortu->user()->first()->role()->first()->type}} </div>
                        </div>
                        @include('app.puskesmas.pendaftar.parent.profile.sidebar')
                    </div>


                    <!-- END PORTLET MAIN -->
                    <!-- PORTLET MAIN -->

                    <!-- END PORTLET MAIN -->
                </div>
                <!-- END BEGIN PROFILE SIDEBAR -->
                <!-- BEGIN PROFILE CONTENT -->
                <div class="profile-content">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="portlet light bordered">
                                <div class="portlet-title tabbable-line">
                                    <div class="caption caption-md">
                                        <i class="icon-globe theme-font hide"></i>
                                        <span class="caption-subject font-blue-madison bold uppercase">Edit Lokasi Orangtua</span>
                                    </div>
                                </div>
                                <div class="portlet-body form">
                                    @if(isset($puskes->id))
                                        <form action="{{route('ortu.editmap',[$puskes->id,$ortu->id])}}" class="form-horizontal" method="post">
                                            @else
                                                <form action="{{route('ortu.edit.map',[$ortu->id])}}" class="form-horizontal" id="form_sample_1" method="post">
                                                    @endif

                                        <div class="form-body">
                                            <div id="map" style="height: 80%"></div>
                                            @if(isset($ortu->place()->first()->id))
                                                <input type="text" hidden id="lat" name="lat" value="{{$ortu->place()->first()->location->getLat()}}">
                                                <input type="text" hidden id="long" name="long" value="{{$ortu->place()->first()->location->getLng()}}">
                                            @else
                                                <input type="text" hidden id="lat" name="lat" value="">
                                                <input type="text" hidden id="long" name="long" value="">
                                            @endif
                                            {{ csrf_field() }}
                                        </div>
                                        <div class="form-actions">
                                            <div class="row">
                                                <div class="col-md-offset-3 col-md-9">
                                                    <button type="submit" class="btn green">Ubah</button>
                                                    <button type="reset" class="btn default">Batal</button>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- END PROFILE CONTENT -->
            </div>
        </div>
        <!-- END PAGE BASE CONTENT -->
    </div>

@endsection

@section('stylesheet')
    @parent
    <link href="{{asset('assets/global/plugins/bootstrap-select/css/bootstrap-select.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('assets/apps/leaflet/leaflet.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('assets/pages/css/profile.min.css')}}" rel="stylesheet" type="text/css" />
@endsection

@section('javascript')
    @parent
    <script src="{{asset('assets/global/plugins/jquery.sparkline.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/pages/scripts/profile.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/global/plugins/bootstrap-select/js/bootstrap-select.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/pages/scripts/components-bootstrap-select.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/apps/leaflet/leaflet.js')}}" type="text/javascript"></script>
    <script>
        $('.profile').initial();
    </script>
    <script>
        var map = L.map('map',{
            @if(isset($ortu->place()->first()->id))
            center: [{{$ortu->place()->first()->location->getLat()}}, {{$ortu->place()->first()->location->getLng()}}],
            @else
            center: [-7.253339, 112.772028],
            @endif
            zoom: 15
        });

        //map rendering
        L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', {
            attribution: '&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
        }).addTo(map);
                @if(isset($ortu->place()->first()->id))
        var lat = [{{$ortu->place()->first()->location->getLat()}}];
        var lng = [{{$ortu->place()->first()->location->getLng()}}];
        var marker1=L.marker([lat[0],lng[0]]).addTo(map);
        @else
            marker1=L.marker([1,1]).addTo(map);
        @endif
        //map input
        map.on('click',
            function(e){
                map.removeLayer(marker1);
                var coord = e.latlng.toString().split(',');
                var lat = coord[0].split('(');
                var lng = coord[1].split(')');
                console.log("You clicked the map at latitude: " + lat[1] + " and longitude:" + lng[0]);
                var marker = L.marker([lat[1],lng[0]]).addTo(map);
                map.on('click', function () {
                    map.removeLayer(marker);

                });
                document.getElementById("lat").value = lat[1];
                document.getElementById("long").value = lng[0];
            });
    </script>
@endsection
