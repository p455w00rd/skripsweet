@extends('layouts.layout.app')

@section('content')
    <div class="page-content">
        <!-- BEGIN PAGE HEAD-->
        <div class="page-head">
            <!-- BEGIN PAGE TITLE -->
            <div class="page-title">
                <h1>Profile Anak
                    <small>{{$child->name}}</small>
                </h1>
            </div>
            <!-- END PAGE TITLE -->

        </div>
        <!-- END PAGE HEAD-->
        <!-- BEGIN PAGE BREADCRUMB -->
        <ul class="page-breadcrumb breadcrumb">
            <li>
                <a href="index.html">Home</a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <span class="active">User</span>
            </li>
        </ul>
        <!-- END PAGE BREADCRUMB -->
        <!-- BEGIN PAGE BASE CONTENT -->
        <div class="profile">
            <div class="tabbable-line tabbable-full-width">
                @include('app.puskesmas.pendaftar.child.profile.sidebar')
                <div class="tab-content">
                    <div class="tab-pane active" id="tab_1_1">
                        <div class="row">
                            <div class="col-md-3">
                                <ul class="list-unstyled profile-nav">
                                    <li>
                                        <img data-name="{{$child->name}}" class="img-responsive full-width img-circle profile" alt="">
                                    </li>
                                </ul>
                            </div>
                            <div class="col-md-9">
                                <div class="row">
                                    <div class="col-md-6 profile-info">
                                        <h1 class="{{$child->surviving == 1 ? 'font-green':'font-grey-cascade'}} sbold uppercase">{{$child->name}}</h1>
                                        <p> {{$child->keterangan == null? 'Tidak Ada Keterangan':$child->keterangan}}</p>
                                        <p>{{$child->surviving == 1 ? '':'Telah Menginggal'}}</p>
                                        <p> Nama Orang Tua :
                                            @if(isset($puskes->id))
                                            <a href="{{route('ortu.profile',[$puskes->id,$child->ortu()->first()->id])}}">
                                                {{$child->ortu()->first()->name}}
                                            </a>
                                                @else
                                                <a href="{{route('ortu.profile.me',[$id])}}">
                                                    {{$child->ortu()->first()->name}}
                                                </a>
                                            @endif
                                        </p>
                                        <ul class="list-inline">

                                            <li>
                                                <i class="fa fa-calendar"></i> {{\Carbon\Carbon::parse($child->dob)->format('d M Y')}} </li>

                                            <li>
                                                <i class="fa fa-{{$child->gender == 1 ? 'mars' : "venus"}}"></i> {{$child->gender == 1 ? 'Laki-Laki' : "Perempuan"}}
                                            </li>
                                            <li>
                                                <i class="fa fa-home"></i> {{$child->address}}
                                            </li>
                                        </ul>
                                        @if(isset($puskes->id))
                                        @if( $child->no()->get()->where('id_puskes',$puskes->id)->first() != null)
                                            <a class="btn green" href="{{ route('receptionist.add',[$puskes->id,$child->id]) }}">

                                            <i class="icon-note"></i> Tambah ke Antrian
                                            </a>
                                            <a class="btn purple" href="{{ route('imunization.add',[$puskes->id,$child->id]) }}">

                                                <i class="glyphicon glyphicon-plus"></i> Tambah Imunisasi
                                            </a>
                                        @else
                                            <p class="font-red font-lg bold uppercase" style="font-size: 20px"><b>Belum Terdaftar</b></p>

                                        @endif
                                            @else
                                            <a class="btn green" href="{{route('ortu.puskesmas.index',$id)}}">

                                                <i class="icon-note"></i> Tambah ke Antrian Puskesmas
                                            </a>
                                            @endif

                                    </div>
                                    <!--end col-md-8-->
                                    <div class="col-md-6">
                                        <div class="portlet sale-summary">
                                            @if(isset($puskes->id))
                                            <div class="portlet-title">
                                                <div class="caption font-red sbold text-center">
                                                    @if($child->no()->get()->where('id_puskes',$puskes->id)->first() != null )
                                                        {{$child->no()->get()->where('id_puskes',$puskes->id)->first()->no_regis}}
                                                    @else
                                                        <button class="btn purple" data-toggle="modal" href="#pendaftaran">Daftarkan</button>
                                                    @endif
                                                </div>
                                            </div>
                                            @endif
                                            <div class="portlet-body">
                                                <ul class="list-unstyled">
                                                    <li>
                                                                    <span class="sale-info"> Umur :
                                                                    </span>
{{--                                                        {{$child->dob->diffInMonths()}}--}}
                                                        @if($dob == 0)
                                                            <span class="sale-num"> {{str_replace(['yang lalu','dari sekarang'], [''], $child->diff())}} </span>
                                                        @else
                                                            <span class="sale-num">
                                                                @if(floor($dob/12) != 0)
                                                                    {{floor($dob/12)}} Tahun
                                                                @endif
                                                                @if($dob%12 != 0)
                                                                    {{$dob%12}} Bulan
                                                                @endif
                                                            </span>
                                                        @endif
                                                    </li>

                                                    <li>
                                                                    <span class="sale-info"> Imunisasi Terakhir :
                                                                    </span>
                                                        <span class="sale-num">
                                                            @if($child->history_imun()->get()->last() != null)
                                                                {{$child->history_imun()->get()->last()->imun()->first()->name}}
                                                            @else
                                                                Belum Imunisasi
                                                            @endif
                                                        </span>
                                                    </li>
                                                    <li>
                                                                    <span class="sale-info"> Tanggal Imunisasi :
                                                                    </span>
                                                        <span class="sale-num">
                                                            @if($child->history_imun()->get()->last() != null)
                                                                {{\Carbon\Carbon::parse($child->history_imun()->get()->last()->created_at)->format('d F Y')}}
                                                            @else
                                                                Belum Imunisasi
                                                            @endif
                                                        </span>
                                                    </li>
                                                    <li>
                                                                    <span class="sale-info"> Jadwal Berikutnya :
                                                                    </span>
                                                        <span class="sale-num">
                                                            @if($child->kembali_imun()->get()->last()!= null)
                                                                {{\Carbon\Carbon::parse($child->kembali_imun()->get()->last()->tanggal_kembali)->format('d F Y')}}
                                                            @else
                                                                Belum Imunisasi
                                                            @endif

                                                        </span>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>

                                    <!--end col-md-4-->
                                </div>
                                <!--end row-->

                            </div>
                        </div>
                        @if(isset($puskes->id))
                        <div class="modal fade" id="pendaftaran" tabindex="-1" role="basic" aria-hidden="true" style="margin-top: 10%">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h4 class="modal-title text-center">Tambahkan Nomor Pendaftaran</h4>
                                    </div>
                                    <form action="{{route('anak.regis',[$puskes->id,$child->id])}}" class="form-horizontal margin-bottom-40" role="form" method="post">
                                        <div class="modal-body">
                                            <div class="form-group form-md-line-input">
                                                <label for="inputEmail1" class="col-md-4 control-label">No.Pendaftaran</label>
                                                <div class="col-md-8">
                                                    <input type="text" class="form-control" name="pendaftaran" id="form_control_1" placeholder="No Pendaftaran">
                                                    <div class="form-control-focus"> </div>
                                                </div>
                                            </div>
                                            {{csrf_field()}}
                                        </div>
                                        <div class="modal-footer">
                                            <button type="submit" class="btn green">Tambah</button>
                                            <button type="button" class="btn dark btn-outline" data-dismiss="modal">Kembali</button>
                                        </div>
                                    </form>
                                </div>
                            </div>

                        </div>
                        @endif
                        <div class="row">
                            <div class="col-md-12">
                                <div class="tabbable-line tabbable-custom-profile">
                                    <ul class="nav nav-tabs">
                                        <li class="active">
                                            <a > History Imunisasi </a>
                                        </li>

                                    </ul>
                                    <div class="tab-content">
                                        <div class="tab-pane active">
                                            <div class="portlet-body">
                                                <table class="table table-striped table-bordered table-advance table-hover">
                                                    <thead>
                                                    <tr>
                                                        <th>
                                                            <i class="fa fa-calendar"></i> Tanggal Imunisasi </th>
                                                        <th>
                                                            <i class="fa fa-plus-square"></i> Jenis Imunisasi </th>
                                                        <th>
                                                            Umur
                                                        <th>
                                                            Berat Badan
                                                        </th>
                                                        <th>
                                                            <i class="fa fa-hospital-o"></i> Pada Puskesmas
                                                        </th>
                                                        @if(Auth::user()->role()->first()->id == 2 || Auth::user()->role()->first()->id == 4)
                                                        <th>
                                                            Action
                                                        </th>
                                                        @endif
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    @forelse($child->history_imun()->orderBy('created_at', 'desc')->get() as $history)
                                                        <tr>
                                                            <td>
                                                                {{\Carbon\Carbon::parse($history->created_at)->format('d F Y')}}
                                                            </td>
                                                            <td >
                                                                {{$history->imun()->first()->name}}
                                                            </td>
                                                            <td>
                                                                {{$history->umur}}
                                                            </td>
                                                            <td>
                                                                {{$history->berat()->first()->berat}} Kg
                                                            </td>
                                                            <td>
                                                                {{$history->puskes()->first()->name}}
                                                            </td>
                                                            @if(Auth::user()->role()->first()->id == 2)
                                                            <td>
                                                                {{--<button type="button" class="btn btn-primary"  data-target="#exampleModal" data-whatever="@mdo">Open modal for @mdo</button>--}}

                                                                <button class="btn yellow" data-target="#edit_imun{{$history->imun()->first()->id}}" data-toggle="modal"
                                                                   data-id-hs="{{$history->id}}" data-date-hs="{{\Carbon\Carbon::parse($history->created_at)->format('d F Y')}}"
                                                                   data-jenis-hs="{{$history->imun()->first()->name}}">
                                                                    <i class="fa fa-pencil"></i>
                                                                </button>
                                                                @endif
                                                                @if(Auth::user()->role()->first()->id == 2)
                                                                <a href="{{route('imunization.record.delete',[$puskes->id,$child->id,$history->id])}}" class="btn red"><i class="fa fa-trash"></i></a>

                                                            </td>
                                                            @endif
                                                            @if(Auth::user()->role()->first()->id == 2)
                                                            @include ('app.puskesmas.pendaftar.child.profile.modal.hitory_imun')
                                                                @endif
                                                        </tr>
                                                    @empty
                                                    @endforelse

                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                        <!--tab-pane-->

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--tab_1_2-->
                </div>
            </div>
        </div>
        <!-- END PAGE BASE CONTENT -->
    </div>

@endsection

@section('stylesheet')
    @parent
    <link href="{{asset('assets/global/plugins/bootstrap-select/css/bootstrap-select.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('assets/pages/css/profile-2.min.css')}}" rel="stylesheet" type="text/css" />
    {{--    <link href="{{asset('assets/pages/css/profile.min.css')}}" rel="stylesheet" type="text/css" />--}}
    <style>
        .caption{
            float: none !important;
            display: block !important;
        }
    </style>
@endsection

@section('javascript')
    @parent
    <script src="{{asset('assets/global/plugins/jquery.sparkline.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/pages/scripts/profile.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/global/plugins/bootstrap-select/js/bootstrap-select.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/pages/scripts/components-bootstrap-select.min.js')}}" type="text/javascript"></script>
    <script>
        $('.profile').initial();

        // $('#edit_imun').on('show.bs.modal', function (event) {
        //     var button = $(event.relatedTarget); // Button that triggered the modal
        //     // var recipient = "abs";
        //     var id_hs = button.data('id-hs');
        //     var date_hs = button.data('date-hs');
        //     var jenis_hs = button.data('jenis-hs');
        //     // Extract info from data-* attributes
        //     // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
        //     // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
        //     var modal = $(this);
        //     modal.find('.modal-body input.id-hs').val(id_hs);
        //     // modal.find('.id-hs').val(id_hs);
        //     modal.find('.modal-body input.date-hs').val(date_hs);
        //     modal.find('.modal-body input.jenis-hs').val(jenis_hs);
        //     hs.value = date_hs;
        // })
    </script>
@endsection