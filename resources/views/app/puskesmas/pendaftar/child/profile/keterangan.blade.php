@extends('layouts.layout.app')

@section('content')

    <div class="page-content">
        <!-- BEGIN PAGE HEAD-->
        <div class="page-head">
            <!-- BEGIN PAGE TITLE -->
            <div class="page-title">
                <h1>Profile Anak
                    <small>{{$child->name}}</small>
                </h1>
            </div>
            <!-- END PAGE TITLE -->

        </div>
        <!-- END PAGE HEAD-->
        <!-- BEGIN PAGE BREADCRUMB -->
        <ul class="page-breadcrumb breadcrumb">
            <li>
                <a href="index.html">Home</a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <span class="active">User</span>
            </li>
        </ul>
        <!-- END PAGE BREADCRUMB -->
        <!-- BEGIN PAGE BASE CONTENT -->
        <div class="profile">
            <div class="tabbable-line tabbable-full-width">
                @include('app.puskesmas.pendaftar.child.profile.sidebar')
                <div class="tab-content">
                    <!--tab_1_2-->
                    <div class="tab-pane active" >
                        <div class="row">
                            <div class="col-md-8">
                                @if(isset($puskes->id))
                                <form action="{{route('anak.update.details',[$puskes->id,$child->id])}}" class="form">
                                    @else
                                        <form action="{{route('ortu.anak.details',[$id,$child->id])}}" class="form" method="post">
                                        @endif
                                <div class="form-group">
                                    <label for="recipient-name">Keterangan Details Anak</label>
                                    <textarea class="form-control" name="keterangan" rows="10">{{$child->keterangan == null? 'Tidak Ada Keterangan':$child->keterangan}}</textarea>
                                    {{--<input readonly type="text" class="form-control jenis-hs" value="" >--}}
                                </div>
                                <div class="form-group">
                                    <label >
                                        Surviving Invant
                                    </label>
                                </div>
                                <div class="form-group">
                                    <input type="checkbox" name="surviving" class="make-switch form-control" data-on-text="Survive" data-off-text="Meninggal"
                                           {{$child->surviving == 1 ? 'checked':''}}>
                                </div>


                                <button type="submit" class="btn green btn-outline">Simpan</button>
                                {{ csrf_field() }}
                                </form>
                            </div>
                        </div>
                    </div>
                    <!--end tab-pane-->

                </div>
            </div>
        </div>
        <!-- END PAGE BASE CONTENT -->
    </div>

@endsection

@section('stylesheet')
    @parent
    <link href="{{asset('assets/pages/css/profile-2.min.css')}}" rel="stylesheet" type="text/css" />

    <style>
        label{
            margin-top: 0px !important;
        }
        .bootstrap-switch-container{
            height: 32px;
            height: 32px;
        }
        .switchh{
            margin-top: 3px;
        }
    </style>
@endsection

@section('javascript')
    @parent
    <script src="{{asset('assets/global/plugins/jquery.sparkline.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/pages/scripts/profile.min.js')}}" type="text/javascript"></script>

@endsection