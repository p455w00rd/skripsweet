@if(isset($puskes->id))
<ul class="nav nav-tabs">
    <li >
        <a href="{{route('anak.profile',[$puskes->id,$child->id])}}"> Overview </a>
    </li>
    <li>
        <a href="{{route('anak.height',[$puskes->id,$child->id])}}"> History Berat Badan </a>
    </li>
    <li>
        <a href="{{route('anak.account',[$puskes->id,$child->id])}}"> Edit Profile </a>
    </li>
    <li>
        <a href="{{route('anak.keterangan',[$puskes->id,$child->id])}}"> Keterangan Anak </a>
    </li>
</ul>
    @else
    <ul class="nav nav-tabs">
        <li >
            <a href="{{route('ortu.anak.profile',[$id,$child->id])}}"> Overview </a>
        </li>
        <li>
            <a href="{{route('ortu.anak.height',[$id,$child->id])}}"> History Berat Badan </a>
        </li>
        <li>
            <a href="{{route('ortu.anak.account',[$id,$child->id])}}"> Account Setting </a>
        </li>
        <li>
            <a href="{{route('ortu.anak.details',[$id,$child->id])}}"> Keterangan Anak </a>
        </li>
    </ul>
@endif