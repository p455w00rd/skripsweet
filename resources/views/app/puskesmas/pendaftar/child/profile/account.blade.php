@extends('layouts.layout.app')

@section('content')

    <div class="page-content">
        <!-- BEGIN PAGE HEAD-->
        <div class="page-head">
            <!-- BEGIN PAGE TITLE -->
            <div class="page-title">
                <h1>Profile Anak
                    <small>{{$child->name}}</small>
                </h1>
            </div>
            <!-- END PAGE TITLE -->

        </div>
        <!-- END PAGE HEAD-->
        <!-- BEGIN PAGE BREADCRUMB -->
        <ul class="page-breadcrumb breadcrumb">
            <li>
                <a href="index.html">Home</a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <span class="active">User</span>
            </li>
        </ul>
        <!-- END PAGE BREADCRUMB -->
        <!-- BEGIN PAGE BASE CONTENT -->
        <div class="profile">
            <div class="tabbable-line tabbable-full-width">
                @include('app.puskesmas.pendaftar.child.profile.sidebar')
                <div class="tab-content">
                    <!--tab_1_2-->
                    <div class="tab-pane active" >
                        <div class="row profile-account">
                            <div class="col-md-3">
                                <ul class="ver-inline-menu tabbable margin-bottom-10">
                                    <li class="active">
                                        <a data-toggle="tab" href="#tab_1-1">
                                            <i class="fa fa-cog"></i> Personal info </a>
                                        <span class="after"> </span>
                                    </li>
                                    <li>
                                        <a data-toggle="tab" href="#tab_2-2">
                                            <i class="fa fa-picture-o"></i> Change Avatar </a>
                                    </li>
                                </ul>
                            </div>
                            <div class="col-md-9">
                                <div class="tab-content">
                                    <div id="tab_1-1" class="tab-pane active">
                                        <div class="row">
                                            <div class="col-md-9">
                                                @if(isset($puskes->id))
                                                    <form role="form" action="{{route('anak.update',[$puskes->id,$child->id])}}" method="post">
                                                        @else
                                                            <form role="form" action="{{route('ortu.anak.account',[$id,$child->id])}}" method="post">
                                                                @endif

                                                                <div class="form-body">
                                                                    @if(isset($puskes->id))
                                                                        <div class="form-group form-md-line-input">

                                                                            @if($child->no()->get()->where('id_puskes',$puskes->id)->first() != null )
                                                                                <input type="text" class="form-control" value="{{$child->no()->get()->where('id_puskes',$puskes->id)->first()->no_regis}}" readonly id="form_control_1">
                                                                            @else
                                                                                <input type="text" class="form-control" value="Belum Terdaftar" readonly id="form_control_1">
                                                                            @endif


                                                                            <label for="form_control_1">No. Pendaftaran</label>
                                                                            <span class="help-block"></span>
                                                                        </div>
                                                                    @endif
                                                                    <div class="form-group form-md-line-input">
                                                                        <input type="text" class="form-control" name="nik" id="form_control_1" value="{{$child->nik}}" >
                                                                        <label for="form_control_1">NIK</label>
                                                                        <span class="help-block">Apabila Ada</span>
                                                                    </div>
                                                                    <div class="form-group form-md-line-input">
                                                                        <input type="text" class="form-control" name="name" id="form_control_1" value="{{$child->name}}">
                                                                        <label for="form_control_1">Nama Lengkap</label>
                                                                        <span class="help-block">Some help goes here...</span>
                                                                    </div>
                                                                    <div class="form-group form-md-line-input">
                                                                        <input type="text" class="form-control" name="address" id="form_control_1" value="{{$child->address}}">
                                                                        <label for="form_control_1">Alamat</label>
                                                                    </div>
                                                                    <div class="form-group form-md-line-input ">
                                                                        <select class="bs-select form-control" name="location" data-live-search="true" data-size="8">
                                                                            <option value="">Pilih Kecamatan</option>
                                                                            @forelse($kecamatan as $kecamatan)
                                                                                <option {{ $child->location == $kecamatan->id? 'selected' : '' }} value="{{$kecamatan->id}}">{{$kecamatan->name}}</option>
                                                                            @empty
                                                                                Tidak Ada Data
                                                                            @endforelse
                                                                        </select>
                                                                        <label for="form_control_1">Kecamatan</label>
                                                                    </div>
                                                                    <div class="form-group form-md-line-input ">
                                                                        <input type="text" class="form-control" name="pob" id="form_control_1" value="{{$child->pob}}">
                                                                        <label for="form_control_1">Tempat Lahir</label>
                                                                    </div>
                                                                    <div class="form-group input-group form-md-line-input input-medium date date-picker" data-date="{{\Carbon\Carbon::parse($child->dob)->format('d M Y')}}" data-date-viewmode="years">
                                                                        <label for="form_control_1">Tanggal Lahir</label>
                                                                        <input type="text" readonly name="dob" class="form-control" value="{{\Carbon\Carbon::parse($child->dob)->format('d M Y')}}"/>
                                                                        <span class="input-group-btn" style="padding-top: 27px">
                                                            <button class="btn default" disabled type="button">
                                                                <i class="fa fa-calendar"></i>
                                                            </button>

                                                        </span>

                                                                    </div>
                                                                    <div class="form-group form-md-line-input " style="margin-top: -23px">
                                                                        <label for="form_control_1">Jenis Kelamin</label>
                                                                        <div class="md-radio-list">
                                                                            <div class="md-radio font-green">
                                                                                <input type="radio" id="checkbox-child1" name="gender" value="1"  class="md-radiobtn"{{ $child->gender == 1? 'checked' : '' }}>
                                                                                <label for="checkbox-child1">
                                                                                    <span></span>
                                                                                    <span class="check"></span>
                                                                                    <span class="box"></span> Laki Laki </label>
                                                                            </div>
                                                                            <div class="md-radio font-red-mint">
                                                                                <input type="radio" id="checkbox-child2" name="gender" value="2" class="md-radiobtn"{{ $child->gender == 2? 'checked' : '' }}>
                                                                                <label for="checkbox-child2">
                                                                                    <span></span>
                                                                                    <span class="check"></span>
                                                                                    <span class="box"></span> Perempuan </label>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    {{ csrf_field() }}
                                                                </div>
                                                                <div class="form-actions noborder">
                                                                    <button type="submit" class="btn blue">Submit</button>
                                                                    <a class="btn default">Cancel</a>
                                                                </div>
                                                            </form>
                                            </div>
                                        </div>
                                    </div>
                                    <div id="tab_2-2" class="tab-pane">
                                        <p> Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod.
                                        </p>
                                        <form action="#" role="form">
                                            <div class="form-group">
                                                <div class="fileinput fileinput-new" data-provides="fileinput">
                                                    <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
                                                        <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" alt="" /> </div>
                                                    <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"> </div>
                                                    <div>
                                                                    <span class="btn default btn-file">
                                                                        <span class="fileinput-new"> Select image </span>
                                                                        <span class="fileinput-exists"> Change </span>
                                                                        <input type="file" name="..."> </span>
                                                        <a href="javascript:;" class="btn default fileinput-exists" data-dismiss="fileinput"> Remove </a>
                                                    </div>
                                                </div>
                                                <div class="clearfix margin-top-10">
                                                    <span class="label label-danger"> NOTE! </span>
                                                    <span> Attached image thumbnail is supported in Latest Firefox, Chrome, Opera, Safari and Internet Explorer 10 only </span>
                                                </div>
                                            </div>
                                            <div class="margin-top-10">
                                                <a href="javascript:;" class="btn green"> Submit </a>
                                                <a href="javascript:;" class="btn default"> Cancel </a>
                                            </div>
                                        </form>
                                    </div>
                                    <div id="tab_3-3" class="tab-pane">
                                        <form action="#">
                                            <div class="form-group">
                                                <label class="control-label">Current Password</label>
                                                <input type="password" class="form-control" /> </div>
                                            <div class="form-group">
                                                <label class="control-label">New Password</label>
                                                <input type="password" class="form-control" /> </div>
                                            <div class="form-group">
                                                <label class="control-label">Re-type New Password</label>
                                                <input type="password" class="form-control" /> </div>
                                            <div class="margin-top-10">
                                                <a href="javascript:;" class="btn green"> Change Password </a>
                                                <a href="javascript:;" class="btn default"> Cancel </a>
                                            </div>
                                        </form>
                                    </div>
                                    <div id="tab_4-4" class="tab-pane">
                                        <form action="#">
                                            <table class="table table-bordered table-striped">
                                                <tr>
                                                    <td> Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus.. </td>
                                                    <td>
                                                        <label class="uniform-inline">
                                                            <input type="radio" name="optionsRadios1" value="option1" /> Yes </label>
                                                        <label class="uniform-inline">
                                                            <input type="radio" name="optionsRadios1" value="option2" checked/> No </label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td> Enim eiusmod high life accusamus terry richardson ad squid wolf moon </td>
                                                    <td>
                                                        <label class="uniform-inline">
                                                            <input type="checkbox" value="" /> Yes </label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td> Enim eiusmod high life accusamus terry richardson ad squid wolf moon </td>
                                                    <td>
                                                        <label class="uniform-inline">
                                                            <input type="checkbox" value="" /> Yes </label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td> Enim eiusmod high life accusamus terry richardson ad squid wolf moon </td>
                                                    <td>
                                                        <label class="uniform-inline">
                                                            <input type="checkbox" value="" /> Yes </label>
                                                    </td>
                                                </tr>
                                            </table>
                                            <!--end profile-settings-->
                                            <div class="margin-top-10">
                                                <a href="javascript:;" class="btn green"> Save Changes </a>
                                                <a href="javascript:;" class="btn default"> Cancel </a>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                            <!--end col-md-9-->
                        </div>
                    </div>
                    <!--end tab-pane-->

                </div>
            </div>
        </div>
        <!-- END PAGE BASE CONTENT -->
    </div>

@endsection

@section('stylesheet')
    @parent
    <link href="{{asset('assets/global/plugins/bootstrap-select/css/bootstrap-select.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('assets/pages/css/profile-2.min.css')}}" rel="stylesheet" type="text/css" />
    {{--    <link href="{{asset('assets/pages/css/profile.min.css')}}" rel="stylesheet" type="text/css" />--}}
    <link href="{{asset('assets/global/plugins/bootstrap-daterangepicker/daterangepicker.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css')}}" rel="stylesheet" type="text/css" />


    <style>
        label{
            margin-top: 0px !important;
        }
    </style>
@endsection

@section('javascript')
    @parent
    <script src="{{asset('assets/global/plugins/jquery.sparkline.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/pages/scripts/profile.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/global/plugins/bootstrap-select/js/bootstrap-select.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/pages/scripts/components-bootstrap-select.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/global/plugins/moment.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/global/plugins/bootstrap-daterangepicker/daterangepicker.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js')}}" type="text/javascript"></script>

    <script>
        $('.date-picker').datepicker({
            rtl: App.isRTL(),
            orientation: "right",
            autoclose: true,
            currentText: "Today:",
            format: "dd-MM-yyyy"
        });
        $('.profile').initial();
    </script>
@endsection