<div class="modal fade" id="edit_imun{{$history->imun()->first()->id}}" tabindex="-1" role="basic" aria-hidden="true" style="margin-top: 10%">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title text-center">Edit Imunisasi anak {{$child->name}}</h4>
            </div>
            <form action="{{route('imunization.record.update',[$puskes->id,$child->id,$history->id])}}" class="form-horizontal" method="post">
            <div class="modal-body" style="padding: 25px">
                <div class="row">
                    <div class="col-md-12">

                            <div class="form-group">
                                <label for="recipient-name" class="col-form-label">Jenis Imunisasi:</label>
                                <input readonly type="text" class="form-control jenis-hs" value="{{$history->imun()->first()->name}}" >
                            </div>
                            <div class="form-group">
                                <label for="recipient-name" class="col-form-label">Tanggal Imunisasi:</label>
                                <input type="text" name="date" class="datepicker2 form-control date-hs" id="hs" value="{{\Carbon\Carbon::parse($history->created_at)->format('d F Y')}}">
                            </div>
                            <input  type="text" name="id" class="hidden form-control id-hs">
                        {{ csrf_field() }}
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn green btn-outline">Simpan</button>
                <button type="button" class="btn dark btn-outline" data-dismiss="modal">Batal</button>
            </div>
            </form>

        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
@section('stylesheet')
    @parent
    <link href="{{asset('assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css')}}" rel="stylesheet" type="text/css" />
    <style>
        .dashboard-stat .details .number{
            font-size: 30px !important;
        }
        a.decor{
            text-decoration: none;
        }
    </style>
@endsection
@section('javascript')
    @parent
    <script src="{{asset('assets/global/plugins/jquery-ui/jquery-ui.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/pages/scripts/ui-modals.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js')}}" type="text/javascript"></script>
    <script>
        // var hs = document.getElementById('hs').value;
        $('.datepicker2').datepicker({
            rtl: App.isRTL(),
            autoclose: true,
            currentText: "Today:",
            format: "dd-MM-yyyy"
        });
        // var datepicker2 =  $('.datepicker2').datepicker('getDate');
        // alert(datepicker2);
    </script>
@endsection