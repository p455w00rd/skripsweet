@extends('layouts.layout.app')

@section('content')

    <div class="page-content">
        <!-- BEGIN PAGE HEAD-->
        <div class="page-head">
            <!-- BEGIN PAGE TITLE -->
            <div class="page-title">
                <h1>Dashboard
                </h1>
            </div>
            <!-- END PAGE TITLE -->

        </div>
        <!-- END PAGE HEAD-->

        <!-- BEGIN PAGE BASE CONTENT -->
        <div class="row">
            <div class="col-md-8">
                <!-- BEGIN VALIDATION STATES-->
                <div class="portlet light portlet-fit portlet-form bordered">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class=" icon-layers font-green"></i>
                            <span class="caption-subject font-dark bold uppercase">Create New Child</span>
                        </div>
                    </div>
                    <div class="portlet-body">
                        <!-- BEGIN FORM-->
                        @if(isset($puskes->id))
                            <form action="{{route('anak.create',[$puskes->id,$ortu->id])}}" class="form-horizontal" method="post">
                                @else
                                    <form action="{{route('ortu.anak.create',[$ortu->id])}}" class="form-horizontal" method="post">
                                        @endif

                                        <div class="form-body">

                                            @if(count( $errors ) > 0)
                                                @foreach ($errors->all() as $error)
                                                    <div>{{ $error }}</div>
                                                @endforeach
                                            @endif

                                            <h4 class="form-title-h4-2"> Data Anak</h4>
                                            <div class="form-group form-md-line-input {{ $errors->has('nik') ? 'has-error' : '' }}">
                                                <label class="col-md-3 control-label" for="form_control_1">Nama Orang Tua
                                                </label>
                                                <div class="col-md-9">
                                                    <input type="text" class="form-control" readonly placeholder="" value="{{$ortu->name}}">
                                                    <div class="form-control-focus"> </div>
                                                    <span class="help-block">{{ $errors->has('nik-anak') ? $error : 'Masukkan Nik Anak bila ada' }}</span>
                                                </div>
                                            </div>
                                            <div class="form-group form-md-line-input {{ $errors->has('nik') ? 'has-error' : '' }}">
                                                <label class="col-md-3 control-label" for="form_control_1">No. NIK
                                                </label>
                                                <div class="col-md-9">
                                                    <input type="text" class="form-control" name="nik" placeholder="" value="{{ old('nik') }}">
                                                    <div class="form-control-focus"> </div>
                                                    <span class="help-block">{{ $errors->has('nik-anak') ? $error : 'Masukkan Nik Anak bila ada' }}</span>
                                                </div>
                                            </div>
                                            @if(isset($puskes->id))
                                                <div class="form-group form-md-line-input {{ $errors->has('pendaftaran') ? 'has-error' : '' }}">
                                                    <label class="col-md-3 control-label" for="form_control_1">No. Pendaftaran
                                                        <span class="required">*</span>
                                                    </label>
                                                    <div class="col-md-9">
                                                        <input type="text" class="form-control" name="pendaftaran" placeholder="" value="{{ old('pendaftaran') }}">
                                                        <div class="form-control-focus"> </div>
                                                        <span class="help-block">{{ $errors->has('pendaftaran') ? $error : 'Masukkan nomor pendaftaran' }}</span>
                                                    </div>
                                                </div>
                                            @endif

                                            <div class="form-group form-md-line-input {{ $errors->has('name') ? 'has-error' : '' }}">
                                                <label class="col-md-3 control-label" for="form_control_1">Nama Anak
                                                    <span class="required">*</span>
                                                </label>
                                                <div class="col-md-9">
                                                    <input type="text" class="form-control" name="name" placeholder="" value="{{ old('name') }}">
                                                    <div class="form-control-focus"> </div>
                                                    <span class="help-block">{{ $errors->has('name') ? $error : 'Masukkan Nama Anak' }}</span>
                                                </div>
                                            </div>
                                            <div class="form-group form-md-line-input {{ $errors->has('pob') ? 'has-error' : '' }}">
                                                <label class="col-md-3 control-label" for="form_control_1">Tempat Lahir Anak
                                                    <span class="required">*</span>
                                                </label>
                                                <div class="col-md-9">
                                                    <input type="text" class="form-control" name="pob" value="{{ old('pob') }}">
                                                    <div class="form-control-focus"> </div>
                                                    <span class="help-block">{{ $errors->has('dob') ? $error : 'Masukkan Kota Tempat Lahir Anak' }}</span>
                                                </div>
                                            </div>
                                            <div class="form-group form-md-line-input {{ $errors->has('dob') ? 'has-error' : '' }}">
                                                <label class="col-md-3 control-label" for="form_control_1">Tanggal Lahir Anak
                                                    <span class="required">*</span>
                                                </label>
                                                <div class="col-md-9">
                                                    <div class="input-group input-medium date date-picker" data-date="now" data-date-viewmode="years">
                                                        <input type="text" name="dob" class="form-control" value="{{ old('dob') }}"/>
                                                        <span class="input-group-btn">
                                                            <button class="btn default" type="button">
                                                                <i class="fa fa-calendar"></i>
                                                            </button>
                                                        </span>
                                                    </div>
                                                    <span class="help-block"> {{ $errors->has('pob') ? $error : 'Select date' }} </span>
                                                </div>
                                            </div>
                                            <div class="form-group form-md-line-input">
                                                <label class="col-md-3 control-label" for="form_control_1">Jenis Kelamin Anak
                                                    <span class="required">*</span>
                                                </label>
                                                <div class="col-md-9">
                                                    <div class="md-radio-list">
                                                        <div class="md-radio font-green">
                                                            <input type="radio" id="checkbox1" name="gender" value="1"  class="md-radiobtn"{{ old('gender') == 1? 'checked' : '' }}>
                                                            <label for="checkbox1">
                                                                <span></span>
                                                                <span class="check"></span>
                                                                <span class="box"></span> Laki Laki </label>
                                                        </div>
                                                        <div class="md-radio font-red-mint">
                                                            <input type="radio" id="checkbox2" name="gender" value="2" class="md-radiobtn"{{ old('gender') == 2? 'checked' : '' }}>
                                                            <label for="checkbox2">
                                                                <span></span>
                                                                <span class="check"></span>
                                                                <span class="box"></span> Perempuan </label>
                                                        </div>
                                                    </div>
                                                </div>
                                                {{ csrf_field() }}
                                            </div>
                                        </div>
                                        <div class="form-actions">
                                            <div class="row">
                                                <div class="col-md-offset-3 col-md-9">
                                                    <button type="submit" class="btn green">Tambah</button>
                                                    <button type="reset" class="btn default">Batal</button>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                    <!-- END FORM-->
                    </div>
                </div>
                <!-- END VALIDATION STATES-->
            </div>
        </div>
        <!-- END PAGE BASE CONTENT -->
    </div>
@endsection
@section('javascript')
    @parent
    <script src="{{asset('assets/global/plugins/bootstrap-select/js/bootstrap-select.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/pages/scripts/components-bootstrap-select.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/global/plugins/moment.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/global/plugins/bootstrap-daterangepicker/daterangepicker.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js')}}" type="text/javascript"></script>

    <script>
        $('.date-picker').datepicker({
            rtl: App.isRTL(),
            orientation: "right",
            autoclose: true,
            currentText: "Today:",
            format: "dd-MM-yyyy"
        });
    </script>
@endsection

@section('stylesheet')
    @parent
    <link href="{{asset('assets/global/plugins/bootstrap-select/css/bootstrap-select.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('assets/global/plugins/bootstrap-daterangepicker/daterangepicker.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css')}}" rel="stylesheet" type="text/css" />
@endsection