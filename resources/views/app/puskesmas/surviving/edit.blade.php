@extends('layouts.layout.app')

@section('content')

    <div class="page-content">
        <!-- BEGIN PAGE HEAD-->
        <div class="page-head">
            <!-- BEGIN PAGE TITLE -->
            <div class="page-title">
                <h1>Super Admin
                </h1>
            </div>
            <!-- END PAGE TITLE -->

        </div>
        <!-- END PAGE HEAD-->

        <!-- BEGIN PAGE BASE CONTENT -->
        <div class="row">
            <div class="col-md-8">
                <!-- BEGIN VALIDATION STATES-->
                <div class="portlet light portlet-fit portlet-form bordered">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class=" icon-layers font-green"></i>
                            <span class="caption-subject font-dark bold uppercase">Data Proyeksi Baru</span>
                        </div>
                    </div>
                    <div class="portlet-body">
                        <!-- BEGIN FORM-->
                        <form action="{{route('surviving.update',$puskes->id)}}" class="form-horizontal" method="post">
                            <div class="form-body">

                                @if(count( $errors ) > 0)
                                    @foreach ($errors->all() as $error)
                                        <div>{{ $error }}</div>
                                    @endforeach
                                @endif
                                <div class="form-group form-md-line-input {{ $errors->has('name') ? 'has-error' : '' }}">
                                    <label class="col-md-3 control-label" for="form_control_1">Tahun
                                        <span class="required">*</span>
                                    </label>
                                    <div class="col-md-9">
                                        <input type="text" class="form-control " disabled name="tahun" placeholder="" value="{{ $proyeksi->tahun }}">
                                        <div class="form-control-focus"> </div>
                                        <span class="help-block">{{ $errors->has('name') ? $error : 'Masukkan Tahun Proyeksi' }}</span>
                                    </div>
                                </div>


                                <div class="form-group form-md-line-input {{ $errors->has('pl') ? 'has-error' : '' }}">
                                    <label class="col-md-3 control-label" for="form_control_1">Proyeksi Anak Laki-laki
                                        <span class="required">*</span>
                                    </label>
                                    <div class="col-md-9">
                                        <input type="text" class="form-control" disabled name="pl" placeholder="" value="{{ $proyeksi->PL }}">
                                        <div class="form-control-focus"> </div>
                                        <span class="help-block">{{ $errors->has('pl') ? $error : 'Masukkan Proyeksi anak Laki laki' }}</span>
                                    </div>
                                </div>
                                <div class="form-group form-md-line-input {{ $errors->has('pp') ? 'has-error' : '' }}">
                                    <label class="col-md-3 control-label" for="form_control_1">Proyeksi Anak Perempuan
                                        <span class="required">*</span>
                                    </label>
                                    <div class="col-md-9">
                                        <input type="text" class="form-control" disabled name="pp" placeholder="" value="{{ $proyeksi->PP }}">
                                        <div class="form-control-focus"> </div>
                                        <span class="help-block">{{ $errors->has('pp') ? $error : 'Masukkan Proyeksi anak Perempuan' }}</span>
                                    </div>
                                </div>
                                    <div class="form-group form-md-line-input {{ $errors->has('pl') ? 'has-error' : '' }}">
                                        <label class="col-md-3 control-label" for="form_control_1">Surviving Anak Laki-laki
                                            <span class="required">*</span>
                                        </label>
                                        <div class="col-md-9">
                                            <input type="text" class="form-control"  name="sl" placeholder="" value="{{ $proyeksi->SL }}">
                                            <div class="form-control-focus"> </div>
                                            <span class="help-block">{{ $errors->has('pl') ? $error : 'Masukkan surviving anak Laki laki' }}</span>
                                        </div>
                                    </div>
                                    <div class="form-group form-md-line-input {{ $errors->has('pp') ? 'has-error' : '' }}">
                                        <label class="col-md-3 control-label" for="form_control_1">Surviving Anak Perempuan
                                            <span class="required">*</span>
                                        </label>
                                        <div class="col-md-9">
                                            <input type="text" class="form-control"  name="sp" placeholder="" value="{{ $proyeksi->SP }}">
                                            <div class="form-control-focus"> </div>
                                            <span class="help-block">{{ $errors->has('pp') ? $error : 'Masukkan Surviving anak Perempuan' }}</span>
                                        </div>
                                    </div>
                                    <input type="text" hidden name="id" value="{{$proyeksi->id}}">

                                {{ csrf_field() }}
                            </div>
                            <div class="form-actions">
                                <div class="row">
                                    <div class="col-md-offset-3 col-md-12">
                                        <button type="submit" class="btn green">Tambah</button>
                                        <button type="reset" class="btn default">Batal</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                        <!-- END FORM-->
                    </div>
                </div>
                <!-- END VALIDATION STATES-->
            </div>
        </div>
        <!-- END PAGE BASE CONTENT -->
    </div>
@endsection
@section('javascript')
    @parent
    <script src="{{asset('assets/global/plugins/bootstrap-select/js/bootstrap-select.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/pages/scripts/components-bootstrap-select.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/global/plugins/moment.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/global/plugins/bootstrap-daterangepicker/daterangepicker.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/global/plugins/select2/js/select2.full.min.js')}}" type="text/javascript"></script>

    <script>
        $('.date-picker').datepicker({
            rtl: App.isRTL(),
            orientation: "right",
            autoclose: true,
            currentText: "Today:",
            format: "dd-MM-yyyy"
        });
        $(document).ready(function() {
            $('.select2-multiple').select2();
        });
    </script>
    <script>
        $('#kecamatan').on('change', function(e){
            console.log(e);
            var state_id = e.target.value;

            $.get('{{ route('find.kelurahan') }}?id_kecamatan=' + state_id, function(data) {
                console.log(data);
                $('#kelurahan').empty();
                $.each(data, function(index,subCatObj){
                    // $('#kelurahan').append(''+subCatObj.name+'');
                    $('#kelurahan').append('<option value="' + subCatObj.id + '">' + subCatObj.name + '</option>');
                });
                $('#kelurahan').selectpicker('refresh');
            });
        });
    </script>
@endsection

@section('stylesheet')
    @parent
    <link href="{{asset('assets/global/plugins/select2/css/select2.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('assets/global/plugins/select2/css/select2-bootstrap.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('assets/global/plugins/bootstrap-select/css/bootstrap-select.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('assets/global/plugins/bootstrap-daterangepicker/daterangepicker.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css')}}" rel="stylesheet" type="text/css" />
@endsection