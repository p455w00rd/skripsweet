@extends('layouts.layout.app')

@section('content')

    <div class="page-content">
        <!-- BEGIN PAGE HEAD-->
        <div class="page-head">
            <!-- BEGIN PAGE TITLE -->
            <div class="page-title">
                <h1>Add Record Imunisasi
                </h1>
            </div>
            <!-- END PAGE TITLE -->

        </div>
        <!-- END PAGE HEAD-->
        <!-- BEGIN PAGE BREADCRUMB -->
        <ul class="page-breadcrumb breadcrumb">
            <li>
                <a href="">Home</a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <span class="active">Add Record Imunisasi</span>
            </li>
        </ul>
        <!-- END PAGE BREADCRUMB -->
        <!-- BEGIN PAGE BASE CONTENT -->
        <div class="row">
            <div class="col-md-12">

                <div class="portlet box purple-seance">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-gift"></i>Data Anak</div>

                    </div>
                    <div class="portlet-body">
                        <form class="form-horizontal" role="form">
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-body">
                                        <div class="form-group">
                                            <label class="col-md-4 control-label">Nama Anak</label>
                                            <div class="col-md-8">
                                                <h4 class="form-control-static"> {{$anak->name}} </h4>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-body">
                                        <div class="form-group">
                                            <label class="col-md-4 control-label">Nama Ortu</label>
                                            <div class="col-md-8">
                                                <h4 class="form-control-static"> {{$anak->ortu()->first()->name}} </h4>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-body">
                                        <div class="form-group">
                                            <label class="col-md-4 control-label">Jenis Kelamin</label>
                                            <div class="col-md-8">
                                                <h4 class="form-control-static"> {{$anak->gender == 1 ? 'Laki-laki' : "Perempuan"}} </h4>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-body">
                                        <div class="form-group">
                                            <label class="col-md-4 control-label">Umur</label>
                                            <div class="col-md-8">
                                                <h4 class="form-control-static"> {{str_replace(['yang lalu','dari sekarang'], [''], $anak->diff())}} </h4>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-8">
                                    <div class="form-body">
                                        <div class="form-group">
                                            <label class="col-md-4 control-label">Tanggal Lahir</label>
                                            <div class="col-md-8">
                                                <h4 class="form-control-static"> {{\Carbon\Carbon::parse($anak->dob)->format('d M Y')}} </h4>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-body">
                                        <div class="form-group">
                                            <label class="col-md-4 control-label">Tanggal Terakhir Imunsasi</label>
                                            <div class="col-md-8">
                                                <h4 class="form-control-static">
                                                    @if($anak->history_imun()->get()->last() != null)
                                                        {{\Carbon\Carbon::parse($anak->history_imun()->get()->last()->created_at)->format('d M Y')}}
                                                    @else
                                                        Belum Imunisasi
                                                    @endif
                                                </h4>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-body">
                                        <div class="form-group">
                                            <label class="col-md-4 control-label">Imunisasi Terakhir</label>
                                            <div class="col-md-8">
                                                <h4 class="form-control-static">
                                                    @if($anak->history_imun()->get()->last() != null)
                                                        {{$anak->history_imun()->get()->last()->imun()->first()->name}}
                                                    @else
                                                        Belum Imunisasi
                                                    @endif
                                                </h4>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>

                <form class="form-horizontal" action="{{route('imunization.record.post',[$puskes->id,$anak->id])}}" method="post" role="form">
                    <div class="portlet box green-turquoise">
                        <div class="portlet-title">
                            <div class="caption">
                                <i class="fa fa-gift"></i>Add Record Imunisasi</div>
                        </div>
                        <div class="portlet-body">
                            <div class="row">
                                <div class="col-md-6">

                                    {{--<div class="form-group form-md-line-input">--}}
                                    {{--<label class="col-md-4 control-label" for="form_control_1">Imunisasi--}}
                                    {{--<span class="required">*</span>--}}
                                    {{--</label>--}}
                                    {{--<div class="col-md-7" >--}}
                                    {{--<select name="imun[]" class="form-control select2-multiple" multiple>--}}
                                    {{--@forelse($imun as $imun)--}}
                                    {{--<option {{$anak->history_imun()->where('id_imunisasi',$imun->id)->first() ? 'disabled': ''}} value="{{$imun->id}}">{{$imun->name}}{{$anak->history_imun()->where('id_imunisasi',$imun->id)->first() ? ' - Sudah dapat': ''}}</option>--}}
                                    {{--@empty--}}
                                    {{--@endforelse--}}
                                    {{--</select>--}}
                                    {{--<div class="form-control-focus"> </div>--}}
                                    {{--<span class="help-block">{{ $errors->has('location') ? $error : 'Pilih Imunisasi' }}</span>--}}
                                    {{--</div>--}}

                                    {{--</div>--}}
                                    <div class="form-group form-md-line-input {{ $errors->has('dob') ? 'has-error' : '' }}">
                                        <label class="col-md-4 control-label" for="form_control_1">Tanggal Kembali
                                            <span class="required">*</span>
                                        </label>
                                        <div class="col-md-8">
                                            <div class="input-group input-medium date date-picker" data-date="now" data-date-viewmode="years">
                                                <input type="text" name="back" class="form-control" value="{{ old('back') }}"/>
                                                <span class="input-group-btn">
                                                            <button class="btn default" type="button">
                                                                <i class="fa fa-calendar"></i>
                                                            </button>
                                                        </span>
                                            </div>
                                            <span class="help-block"> {{ $errors->has('pob') ? $error : 'Select date' }} </span>
                                        </div>
                                    </div>

                                    <div class="form-group form-md-line-input">
                                        <label class="col-md-4 control-label" for="form_control_1">Berat Badan
                                            <span class="required">*</span>
                                        </label>
                                        <div class="col-md-3" >
                                            <input type="text" name="berat" class="form-control" placeholder="KG">
                                            <div class="form-control-focus"> </div>
                                            <span class="help-block">{{ $errors->has('location') ? $error : 'KG' }}</span>
                                        </div>
                                        @isset($recept)
                                        <input type="text" name="recept" value="{{$recept}}" hidden>
                                        @endisset
                                        <input type="text" name="gender" value="{{$anak->gender}}" hidden>
                                        <input type="text" name="umur" value="{{str_replace(['yang lalu','dari sekarang'], [''], $anak->diff())}}" hidden>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group form-md-line-input">
                                        <label class="col-md-4 control-label" for="form_control_1">Kelurahan
                                            <span class="required">*</span>
                                        </label>
                                        <div class="col-md-7" >
                                            <select name="kelurahan" id="" data-live-search="true" data-size="8" class="bs-select  form-control">
                                                <option value="">Pilih Kelurahan</option>
                                                @forelse($puskes->kelurahan()->get() as $kelurahan)
                                                    <option value="{{$kelurahan->id}}">{{$kelurahan->name}}</option>
                                                @empty
                                                @endforelse
                                            </select>
                                            <div class="form-control-focus"> </div>
                                            <span class="help-block">{{ $errors->has('location') ? $error : 'Pilih Imunisasi' }}</span>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-4 control-label" for="form_control_1">
                                            <span class="required"></span>
                                        </label>
                                        <div class="col-md-8">
                                            <input type="checkbox" name="idl" class="make-switch" data-on-text="<i class='fa fa-check switchh'></i>" data-off-text="<i class='fa fa-times switchh'></i>">
                                            <div class="form-control-focus"> </div>
                                            <span class="help-block"> Centang apabila sudah IDL </span>
                                        </div>


                                    </div>
                                </div>
                                <div class="col-md-8">

                                </div>
                                <div class="col-md-12 text-center">

                                    <button type="submit" name="submit" class="btn btn-info"> Simpan Rekam Imunisasi</button>
                                    {{csrf_field()}}
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="portlet box blue-steel">
                        <div class="portlet-title">
                            <div class="caption">
                                <i class="fa fa-gift"></i>Add Record Imunisasi</div>

                        </div>
                        <div class="portlet-body">
                            <div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <table class="table" id="dynamic_field">
                                            <tr>
                                                <td>
                                                    <select id="name" name="imun" data-live-search="true" data-size="8" class="bs-select form-control " >
                                                        @forelse($imun as $imun)                                                            <option {{$anak->history_imun()->where('id_imunisasi',$imun->id)->first() ? 'disabled': ''}}  class="{{$imun->name}}" value="{{$imun->id}}">{{$imun->name}}{{$anak->history_imun()->where('id_imunisasi',$imun->id)->first() ? ' - Sudah dapat': ''}}</option>
                                                            {{--<input id="name" type="text" hidden value="{{$imun->value}}">--}}
                                                        @empty
                                                        @endforelse

                                                    </select>

                                                </td>
                                                <td>
                                                    <div  class="input-group date date-picker" data-date="now" data-date-viewmode="years">
                                                        <input id="date" type="text" class="form-control" value="{{ old('back') }}"/>
                                                        <span class="input-group-btn">
                                                            <button class="btn default" type="button">
                                                                <i class="fa fa-calendar"></i>
                                                            </button>
                                                        </span>
                                                    </div>
                                                </td>
                                                <td><button type="button" name="add" id="add" class="btn btn-success">Tambah Imunisasi</button></td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>

                            </div>

                        </div>
                    </div>
                </form>
            </div>
            <!-- END PAGE BASE CONTENT -->

        </div>
    </div>

@endsection
@section('stylesheet')
    @parent
    <link href="{{asset('assets/global/plugins/select2/css/select2.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('assets/global/plugins/select2/css/select2-bootstrap.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('assets/global/plugins/bootstrap-select/css/bootstrap-select.min.css')}}" rel="stylesheet" type="text/css" />
    {{--    <link href="{{asset('assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css')}}" rel="stylesheet" type="text/css" />--}}
    <link href="{{asset('assets/global/plugins/bootstrap-daterangepicker/daterangepicker.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css')}}" rel="stylesheet" type="text/css" />
    <style>
        .bootstrap-switch-container{
            height: 32px;
            height: 32px;
        }
        .switchh{
            margin-top: 3px;
        }
    </style>

@stop

@section('javascript')
    @parent
    <script src="{{asset('assets/global/plugins/select2/js/select2.full.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/pages/scripts/components-bootstrap-switch.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/global/plugins/moment.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/global/plugins/bootstrap-daterangepicker/daterangepicker.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/global/plugins/bootstrap-select/js/bootstrap-select.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/pages/scripts/components-bootstrap-select.min.js')}}" type="text/javascript"></script>
    <script>
        $(document).ready(function() {
            $('.select2-multiple').select2();
        });
        $(document).ready(function(){
            var i=1;
            $('#add').click(function(){
                i++;
                var input = document.getElementById("name");
                var date = document.getElementById("date");
                $('#dynamic_field').append('<tr id="row'+i+'" class="dynamic-added">' +
                    '<td><input required readonly value="'+$( "#name option:selected" ).text()+'" type="text"  class="form-control name_list" /></td>' +
                    '<td><input required readonly value="'+date.value+'" type="text" name="date[]" class="form-control name_list" /></td>' +
                    '<td class="hidden"><input required value="'+input.value+'" type="hidden" name="imun[]" class="hidden invisible form-control name_list" /></td>' +
                    '<td><button type="button" name="remove" id="'+i+'" class="btn btn-danger btn_remove">Hapus</button></td>' +
                    '</tr>');
            });
            $(document).on('click', '.btn_remove', function(){
                var button_id = $(this).attr("id");
                $('#row'+button_id+'').remove();
            });
        });
        $('.date-picker').datepicker({
            rtl: App.isRTL(),
            orientation: "right",
            autoclose: true,
            currentText: "Today:",
            format: "dd-MM-yyyy"
        });
    </script>

@stop