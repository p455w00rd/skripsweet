@extends('layouts.layout.auth')

@section('auth')
    <div class="user-login-5">
        <div class="row bs-reset">
            <div class="col-md-6 bs-reset">
                <div class="login-bg" style="background-image:url(assets/image/Puskesmas.png)">
                </div>
            </div>
            <div class="col-md-6 login-container bs-reset">
                <div class="login-content" style="margin-top: 15%;">
                    <h1>SI Imun Registrasi</h1>
                    <p>Sistem Informasi Imunisasi Puskesmas </p>
                    <form action="{{route('ortu_post_reg')}}" class="form-horizontal" method="post">
                        <div class="form-body">

                            @if(count( $errors ) > 0)
                                @foreach ($errors->all() as $error)
                                    <div>{{ $error }}</div>
                                @endforeach
                            @endif
                                <div class="form-group form-md-line-input {{ $errors->has('name') ? 'has-error' : '' }}">
                                    <label class="col-md-4 control-label" for="form_control_1">Nama Orang Tua
                                        <span class="required">*</span>
                                    </label>
                                    <div class="col-md-8">

                                        <input type="text" class="form-control " placeholder="" name="name" value="{{ old('name') }}">
                                        <div class="form-control-focus"> </div>
                                        <span class="help-block">{{ $errors->has('name') ? $error : 'masukkan Nama' }}</span>
                                    </div>
                                </div>
                            <div class="form-group form-md-line-input {{ $errors->has('username') ? 'has-error' : '' }}">
                                <label class="col-md-4 control-label" for="form_control_1">Username
                                    <span class="required">*</span>
                                </label>
                                <div class="col-md-8">

                                    <input type="text" class="form-control " placeholder="" name="username" value="{{ old('username') }}">
                                    <div class="form-control-focus"> </div>
                                    <span class="help-block">{{ $errors->has('username') ? $error : 'masukkan Username' }}</span>
                                </div>
                            </div>
                            <div class="form-group form-md-line-input {{ $errors->has('email') ? 'has-error' : '' }}">
                                <label class="col-md-4 control-label" for="form_control_1">Email
                                    <span class="required">*</span>
                                </label>
                                <div class="col-md-8">
                                    <input type="text" class="form-control" placeholder="" name="email" value="{{ old('email') }}">
                                    <div class="form-control-focus"> </div>
                                    <span class="help-block">{{ $errors->has('email') ? $error : 'Masukkan Email' }}</span>
                                </div>
                            </div>

                            <div class="form-group form-md-line-input {{ $errors->has('password_confirm') ? 'has-error' : '' }}">
                                <label class="col-md-4 control-label" for="form_control_1">Password
                                    <span class="required">*</span>
                                </label>
                                <div class="col-md-8">
                                    <input type="password" class="form-control" placeholder="" name="password" value="">
                                    <div class="form-control-focus"> </div>
                                    <span class="help-block">{{ $errors->has('password_confirmation') ? $error : 'Masukkan Password' }}</span>
                                </div>
                            </div>
                            <div class="form-group form-md-line-input {{ $errors->has('password_confirm') ? 'has-error' : '' }}">
                                <label class="col-md-4 control-label" for="form_control_1">Ulangi Password
                                    <span class="required">*</span>
                                </label>
                                <div class="col-md-8">
                                    <input type="password" class="form-control" placeholder="" name="password_confirm" value="">
                                    <div class="form-control-focus"> </div>
                                    <span class="help-block">{{ $errors->has('password_confirm') ? $error : 'Masukkan kembali Password' }}</span>
                                </div>
                            </div>
                            <hr>

                                {{ csrf_field() }}
                        </div>
                        <div class="form-actions">
                            <div class="row">
                                <div class="col-md-offset-3 col-md-9">
                                    <button type="submit" class="btn green">Daftar</button>
                                    <a href="{{route('login')}}" type="reset" class="btn default">Kembali</a>
                                </div>
                            </div>
                        </div>
                    </form>
                    <!-- END FORGOT PASSWORD FORM -->
                </div>
                <div class="login-footer">
                    <div class="row bs-reset">
                        <div class="col-xs-5 bs-reset">
                            {{--<a class="btn btn-link" href="{{ route('password.request') }}">--}}
                                {{--Forgot Your Password?--}}
                            {{--</a>--}}
                        </div>
                        <div class="col-xs-7 bs-reset">
                            <div class="login-copyright text-right">
                                <div class="page-footer-inner"> 2018 &copy; Kojyou project.
                                    <a href="http://apollo16team.com" title="Visit Us! Apollo 16 Team" target="_blank">apollo16team.com</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    {{--<div class="container">--}}
    {{--<div class="row">--}}
    {{--<div class="col-md-8 col-md-offset-2">--}}
    {{--<div class="panel panel-default">--}}
    {{--<div class="panel-heading">Login</div>--}}

    {{--<div class="panel-body">--}}
    {{--<form class="form-horizontal" method="POST" action="{{ route('login') }}">--}}
    {{--{{ csrf_field() }}--}}

    {{--<div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">--}}
    {{--<label for="email" class="col-md-4 control-label">E-Mail Address</label>--}}

    {{--<div class="col-md-6">--}}
    {{--<input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required autofocus>--}}

    {{--@if ($errors->has('email'))--}}
    {{--<span class="help-block">--}}
    {{--<strong>{{ $errors->first('email') }}</strong>--}}
    {{--</span>--}}
    {{--@endif--}}
    {{--</div>--}}
    {{--</div>--}}

    {{--<div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">--}}
    {{--<label for="password" class="col-md-4 control-label">Password</label>--}}

    {{--<div class="col-md-6">--}}
    {{--<input id="password" type="password" class="form-control" name="password" required>--}}

    {{--@if ($errors->has('password'))--}}
    {{--<span class="help-block">--}}
    {{--<strong>{{ $errors->first('password') }}</strong>--}}
    {{--</span>--}}
    {{--@endif--}}
    {{--</div>--}}
    {{--</div>--}}

    {{--<div class="form-group">--}}
    {{--<div class="col-md-6 col-md-offset-4">--}}
    {{--<div class="checkbox">--}}
    {{--<label>--}}
    {{--<input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> Remember Me--}}
    {{--</label>--}}
    {{--</div>--}}
    {{--</div>--}}
    {{--</div>--}}

    {{--<div class="form-group">--}}
    {{--<div class="col-md-8 col-md-offset-4">--}}
    {{--<button type="submit" class="btn btn-primary">--}}
    {{--Login--}}
    {{--</button>--}}

    {{--<a class="btn btn-link" href="{{ route('password.request') }}">--}}
    {{--Forgot Your Password?--}}
    {{--</a>--}}
    {{--</div>--}}
    {{--</div>--}}
    {{--</form>--}}
    {{--</div>--}}
    {{--</div>--}}
    {{--</div>--}}
    {{--</div>--}}
    {{--</div>--}}
@endsection
