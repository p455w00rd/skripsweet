<!DOCTYPE html>

<html>

<head>
    <link rel="stylesheet" href="http://cdn.leafletjs.com/leaflet-0.7.3/leaflet.css" />

    <script src="http://cdn.leafletjs.com/leaflet-0.7.3/leaflet.js"></script>

    <title>Leaflet Web Map</title>

    <style>
        #map {
            width: 960px;
            height: 500px;
        }

    </style>

</head>

<body>
<input type="text" value="" id="asd">
<div id="map"></div>

<script>
    var map = L.map('map',{
        center: [-7.253339, 112.772028],
        zoom: 15
    });


    L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', {
        attribution: '&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
    }).addTo(map);


    var latlngs = [
        [-7.2564761,112.7589512],
        [-7.2634578,112.7711391],
        [-7.2518783,112.7764606],
        [-7.2478765,112.761097]];
    var polygon = L.polygon(latlngs, {color: 'blue'}).addTo(map);
    polygon.bindPopup("I am a polygon.");
    // zoom the map to the polygon
    map.fitBounds(polygon.getBounds());
    var lat, lng;

    map.addEventListener('mousemove', function(ev) {
        lat = ev.latlng.lat;
        lng = ev.latlng.lng;
    });

    map.on('click',
        function(e){
            var coord = e.latlng.toString().split(',');
            var lat = coord[0].split('(');
            var lng = coord[1].split(')');
            console.log("You clicked the map at latitude: " + lat[1] + " and longitude:" + lng[0]);
            var marker = L.marker([lat[1],lng[0]]).addTo(map);
            map.on('click', function () {
                map.removeLayer(marker);
            });
            document.getElementById("asd").value = lat[1];
        });
</script>

</body>

</html>