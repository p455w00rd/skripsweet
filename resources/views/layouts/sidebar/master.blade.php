<!-- DOC: Apply "page-sidebar-menu-light" class right after "page-sidebar-menu" to enable light sidebar menu style(without borders) -->
<!-- DOC: Apply "page-sidebar-menu-hover-submenu" class right after "page-sidebar-menu" to enable hoverable(hover vs accordion) sub menu mode -->
<!-- DOC: Apply "page-sidebar-menu-closed" class right after "page-sidebar-menu" to collapse("page-sidebar-closed" class must be applied to the body element) the sidebar sub menu mode -->
<!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
<!-- DOC: Set data-keep-expand="true" to keep the submenues expanded -->
<!-- DOC: Set data-auto-speed="200" to adjust the sub menu slide up/down speed -->
<?php $idRole = Auth::user()->role()->first()->id; ?>
<ul class="page-sidebar-menu   " data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200">
    <li class="heading" style="padding-top: 20px">
        <h3 class="uppercase awal" style="text-align: center;font-weight: 700;padding-top: 0px;">
            @if($idRole == 3)
                Selamat Datang
            @elseif($idRole == 1 || $idRole == 5)
                Dinkes
            @else
                {{isset($puskes) ? $puskes->name :'Dinkes'}}

            @endif
        </h3>
    </li>
    @if($idRole == 3)
        @if(isset(Auth::user()->ortu()->first()->address))
            @php
                $id = Auth::user()->ortu()->first()->id;
            @endphp
            <li class="nav-item">
                <a href="{{route('ortu.dashboard',$id)}}" class="nav-link">
                    <i class="icon-grid"></i>
                    <span class="title">Dashboard</span>
                </a>
            </li>
            <li class="nav-item">
                <a href="{{route('ortu.calendar.index',$id)}}" class="nav-link">
                    <i class="icon-calendar"></i>
                    <span class="title">Jadwal Imunisasi</span>
                </a>
            </li>
            <li class="nav-item">
                <a href="{{route('ortu.anak.me',$id)}}" class="nav-link">
                    <i class="icon-user"></i>
                    <span class="title">Anak</span>
                </a>
            </li>
            <li class="nav-item">
                <a href="{{route('ortu.puskesmas.index',$id)}}" class="nav-link">
                    <i class="fa fa-hospital-o"></i>
                    <span class="title">Daftar ke Puskesmas</span>
                </a>
            </li>
            <li class="nav-item">
                <a href="{{route('ortu.near',$id)}}" class="nav-link">
                    <i class="fa fa-map-o"></i>
                    <span class="title">Puskesmas Terdekat</span>
                </a>
            </li>
            <li class="nav-item">
                <a href="{{route('ortu.puskesmas.queue.list',$id)}}" class="nav-link">
                    <i class="icon-note"></i>
                    <span class="title">Kartu Antrian</span>
                </a>
            </li>
        @endif
    @endif
    @if($idRole == 2 ||$idRole == 4)
        <li class="nav-item start ">
            <a href="{{route('puskesmas.dashboard',$puskes->id)}}" class="nav-link">
                <i class="icon-home"></i>
                <span class="title">Dashboard</span>
            </a>
        </li>
        <li class="nav-item">
            <a href="{{route('receptionist.index',$puskes->id)}}" class="nav-link">
                <i class="icon-note"></i>
                <span class="title">Kedatangan</span>
            </a>
        </li>
        <li class="heading">
            <h3 class="uppercase">Master Data Puskesmas</h3>
        </li>
        <li class="nav-item">
            <a href="{{route('anak.index',$puskes->id)}}" class="nav-link ">
                <i class="icon-folder-alt"></i>
                <span class="title">Data Anak</span>
                <span class="selected"></span>
            </a>
        </li>
        @if($idRole != 4)
            <li class="nav-item">
                <a href="{{route('puskesmas.user.index',$puskes->id)}}" class="nav-link">
                    <i class="icon-users"></i>
                    <span class="title">Pegawai</span>
                </a>
            </li>
            <li class="nav-item">
                <a href="{{route('puskesmas.edit',[$puskes->id])}}" class="nav-link">
                    <i class="fa fa-hospital-o"></i>
                    <span class="title">Puskesmas</span>
                </a>
            </li>
            <li class="nav-item">
                <a href="{{route('surviving.index',[$puskes->id])}}" class="nav-link">
                    <i class="icon-folder-alt"></i>
                    <span class="title">Surviving Invant</span>
                </a>
            </li>
        @endif
        <li class="nav-item">
            <a href="{{route('gis.near',[$puskes->id])}}" class="nav-link">
                <i class="fa fa-map-o"></i>
                <span class="title">Pasien Terdekat</span>
            </a>
        </li>
        <li class="heading">
            <h3 class="uppercase">Master Data ( semua )</h3>
        </li>
        <li class="nav-item">
            <a href="javascript:;" class="nav-link nav-toggle">
                <i class="icon-folder-alt"></i>
                <span class="title">Data Pasien</span>
                <span class="selected"></span>
                <span class="arrow open"></span>
            </a>
            <ul class="sub-menu">
                <li class="nav-item  ">
                    <a href="{{route('ortu.index',$puskes->id)}}" class="nav-link ">
                        <span class="title">Data Ortu</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{route('anak.all',$puskes->id)}}" class="nav-link ">
                        <span class="title">Data Anak</span>
                        <span class="selected"></span>
                    </a>
                </li>
            </ul>
        </li>
        <li class="nav-item">
            <a href="{{route('puskesmas.imunisasi',$puskes->id)}}" class="nav-link">
                <i class="icon-folder-alt"></i>
                <span class="title">Imunisasi</span>
            </a>
        </li>
        <li class="heading">
            <h3 class="uppercase">Laporan</h3>
        </li>
        <li class="nav-item">
            <a href="javascript:" class="nav-link nav-toggle">
                <i class="icon-folder-alt"></i>
                <span class="title">laporan Puskesmas</span>
                <span class="selected"></span>
                <span class="arrow open"></span>
            </a>
            <ul class="sub-menu">
                {{--<li class="nav-item  ">--}}
                {{--<a href="" class="nav-link ">--}}
                {{--<span class="title">Kohort</span>--}}
                {{--</a>--}}
                {{--</li>--}}
                <li class="nav-item  ">
                    <a href="{{route('laporan.pencatatan',$puskes->id)}}" target="_blank" class="nav-link ">
                        <span class="title">Laporan Pencatatan Imunisasi Rutin</span>
                    </a>
                </li>
                <li class="nav-item  ">
                    <a href="{{route('laporan.pws',$puskes->id)}}" class="nav-link ">
                        <span class="title">Laporan PWS Imunisasi</span>
                    </a>
                </li>
                {{--<li class="nav-item  ">--}}
                {{--<a href="" class="nav-link ">--}}
                {{--<span class="title">Laporan KIPI</span>--}}
                {{--</a>--}}
                {{--</li>--}}

            </ul>
        </li>
        <li class="nav-item">
            <a href="{{route('pemetaan',$puskes->id)}}" class="nav-link">
                <i class="icon-map"></i>
                <span class="title">Pemetaan</span>
            </a>
        </li>
        @if($idRole == 2)
        <li class="nav-item">
            <a href="{{route('laporan.log',$puskes->id)}}" class="nav-link">
                <i class="icon-notebook"></i>
                <span class="title">Log</span>
            </a>
        </li>
            @endif
    @endif
    @if($idRole == 1)
        {{--<li class="nav-item">--}}
            {{--<a href="{{route('puskesmas.dinkes')}}" class="nav-link">--}}
                {{--<i class="fa fa-home"></i>--}}
                {{--<span class="title">Dashboard</span>--}}
            {{--</a>--}}
        {{--</li>--}}
        <li class="heading">
            <h3 class="uppercase">Super Admin</h3>
        </li>
        <li class="nav-item">
            <a href="{{route('puskesmas.dinkes')}}" class="nav-link">
                <i class="fa fa-hospital-o"></i>
                <span class="title">Master Data Puskesmas</span>
            </a>
        </li>
        <li class="nav-item">
            <a href="{{route('imunisasi.dinkes')}}" class="nav-link">
                <i class="fa fa-folder-o"></i>
                <span class="title">Master Data Imunisasi</span>
            </a>
        </li>
        <li class="nav-item">
            <a href="{{route('proyeksi')}}" class="nav-link">
                <i class="fa fa-folder-o"></i>
                <span class="title">Master Data Proyeksi</span>
            </a>
        </li>
        <li class="nav-item">
            <a href="javascript:" class="nav-link nav-toggle">
                <i class="icon-user"></i>
                <span class="title">Master Data User</span>
                <span class="selected"></span>
                <span class="arrow open"></span>
            </a>
            <ul class="sub-menu">

                <li class="nav-item  ">
                    <a href="{{route('dinkes.user.puskesmas')}}" class="nav-link ">
                        <span class="title">Admin Puskesmas</span>
                    </a>
                </li>
                <li class="nav-item  ">
                    <a href="{{route('dinkes.user')}}" class="nav-link ">
                        <span class="title">Pegawai Dinkes</span>
                    </a>
                </li>
            </ul>
        </li>
    @endif
    @if($idRole == 5)
        <li class="heading">
            <h3 class="uppercase">Laporan</h3>
        </li>
        <li class="nav-item">
            <a href="{{route('laporan.dinkes')}}" class="nav-link">
                <i class="fa fa-folder-o"></i>
                <span class="title">Laporan</span>
            </a>
        </li>
        <li class="nav-item">
            <a href="{{route('laporan.imunisasi.dinkes.gis')}}" class="nav-link">
                <i class="icon-map"></i>
                <span class="title">Pemetaan</span>
            </a>
        </li>
    @endif
</ul>