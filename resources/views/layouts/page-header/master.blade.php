<div class="page-header navbar navbar-fixed-top">
    <!-- BEGIN HEADER INNER -->
    <div class="page-header-inner ">
        <!-- BEGIN LOGO -->
        <div class="page-logo">
            <a href="" style="text-decoration: none">
                <h3 class="logo" style="margin-left: 40px"><b><i class="fa fa-hospital-o"></i> SIDL</b></h3>
            </a>
            <div class="menu-toggler sidebar-toggler">
                <!-- DOC: Remove the above "hide" to enable the sidebar toggler button on header -->

            </div>

        </div>
        <!-- END LOGO -->
        <!-- BEGIN RESPONSIVE MENU TOGGLER -->
        <a href="javascript:;" class="menu-toggler responsive-toggler" data-toggle="collapse" data-target=".navbar-collapse"> </a>
        <!-- END RESPONSIVE MENU TOGGLER -->
        <!-- BEGIN PAGE ACTIONS -->
        <!-- DOC: Remove "hide" class to enable the page header actions -->
    {{--<div class="page-actions">--}}
    {{--<div class="btn-group">--}}
    {{--<button type="button" class="btn red-haze btn-sm dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">--}}
    {{--<span class="hidden-sm hidden-xs">Actions&nbsp;</span>--}}
    {{--<i class="fa fa-angle-down"></i>--}}
    {{--</button>--}}
    {{--<ul class="dropdown-menu" role="menu">--}}
    {{--<li>--}}
    {{--<a href="javascript:;">--}}
    {{--<i class="icon-docs"></i> New Post </a>--}}
    {{--</li>--}}
    {{--<li>--}}
    {{--<a href="javascript:;">--}}
    {{--<i class="icon-tag"></i> New Comment </a>--}}
    {{--</li>--}}
    {{--<li>--}}
    {{--<a href="javascript:;">--}}
    {{--<i class="icon-share"></i> Share </a>--}}
    {{--</li>--}}
    {{--<li class="divider"> </li>--}}
    {{--<li>--}}
    {{--<a href="javascript:;">--}}
    {{--<i class="icon-flag"></i> Comments--}}
    {{--<span class="badge badge-success">4</span>--}}
    {{--</a>--}}
    {{--</li>--}}
    {{--<li>--}}
    {{--<a href="javascript:;">--}}
    {{--<i class="icon-users"></i> Feedbacks--}}
    {{--<span class="badge badge-danger">2</span>--}}
    {{--</a>--}}
    {{--</li>--}}
    {{--</ul>--}}
    {{--</div>--}}
    {{--</div>--}}
    <!-- END PAGE ACTIONS -->
        <!-- BEGIN PAGE TOP -->
        <div class="page-top">
            <!-- BEGIN HEADER SEARCH BOX -->
            <!-- DOC: Apply "search-form-expanded" right after the "search-form" class to have half expanded search box -->
        {{--<form class="search-form" action="page_general_search_2.html" method="GET">--}}
        {{--<div class="input-group">--}}
        {{--<input type="text" class="form-control input-sm" placeholder="Search..." name="query">--}}
        {{--<span class="input-group-btn">--}}
        {{--<a href="javascript:;" class="btn submit">--}}
        {{--<i class="icon-magnifier"></i>--}}
        {{--</a>--}}
        {{--</span>--}}
        {{--</div>--}}
        {{--</form>--}}
        <!-- END HEADER SEARCH BOX -->
            <!-- BEGIN TOP NAVIGATION MENU -->
            <div class="top-menu">
                <ul class="nav navbar-nav pull-right">
                    <li class="separator hide"> </li>

                    <li class="dropdown dropdown-user dropdown-dark">
                        <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                            @if(Auth::user()->role()->first()->id != 3)
                                <span class="username username-hide-on-mobile"> {{Auth::user()->user_data()->first()->name}} </span>
                                <!-- DOC: Do not remove below empty space(&nbsp;) as its purposely used -->
                                <img data-name="{{Auth::user()->user_data()->first()->name}}" class="img-circle profile-foto-sidebar" alt="">
                                {{--<img alt="" class="img-circle" src="../assets/layouts/layout4/img/avatar9.jpg" /> --}}
                            @else
                                @if(isset(Auth::user()->ortu()->first()->name))
                                    <span class="username username-hide-on-mobile"> {{Auth::user()->ortu()->first()->name}} </span>
                                    <!-- DOC: Do not remove below empty space(&nbsp;) as its purposely used -->
                                    <img data-name="{{Auth::user()->ortu()->first()->name}}" class="img-circle profile-foto-sidebar" alt="">
                                    {{--<img alt="" class="img-circle" src="../assets/layouts/layout4/img/avatar9.jpg" /> --}}
                                @else
                                    <span class="username username-hide-on-mobile"> {{Auth::user()->username}} </span>
                                    <!-- DOC: Do not remove below empty space(&nbsp;) as its purposely used -->
                                    <img data-name="{{Auth::user()->username}}" class="img-circle profile-foto-sidebar" alt="">
                                @endif
                            @endif
                        </a>
                        <ul class="dropdown-menu dropdown-menu-default">
                            <li>
                                @if(Auth::user()->role()->first()->id == 3)
                                    @if(isset(Auth::user()->ortu()->first()->id))
                                        <a href="{{route('ortu.profile.me',Auth::user()->ortu()->first()->id)}}">
                                            <i class="icon-user"></i> My Profile </a>
                                    @endif
                                @elseif(Auth::user()->role()->first()->id == 2|Auth::user()->role()->first()->id == 4)
                                    <a href="{{route('puskesmas.user.edit',[$puskes->id,Auth::user()->id,1])}}">
                                        <i class="icon-user"></i> My Profile </a>
                                @else
                                    <a href="{{route('dinkes.user.edit',[Auth::user()->id,Auth::user()->user_data->id,1])}}">
                                        <i class="icon-user"></i> My Profile </a>
                                @endif
                            </li>
                            <li>
                                <a href="{{ route('logout') }}"
                                   onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                    <i class="icon-key"></i> Logout
                                </a>

                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                    {{ csrf_field() }}
                                </form>

                            </li>
                        </ul>
                    </li>

                </ul>
            </div>
            <!-- END TOP NAVIGATION MENU -->
        </div>
        <!-- END PAGE TOP -->
    </div>
    <!-- END HEADER INNER -->
</div>
@section('stylesheet')
    @parent
    <style>
        h3.logo{
            margin-top: 26px;
        }
    </style>
@endsection
@section('javascript')
    @parent
    <script>
        $('.profile-foto-sidebar').initial();
    </script>
@endsection