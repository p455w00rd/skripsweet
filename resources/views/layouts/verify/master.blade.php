@if(Auth::user()->role()->first()->id != 3)
@elseif(Auth::user()->role()->first()->id == 3 && isset(Auth::user()->veri()->first()->id))
    @php
        $id = Auth::user()->ortu()->first()->id;
    @endphp
    @if(Auth::user()->veri()->first()->confirmed == 0)
        <div class="note note-info bg-red-sunglo">
            <p>
                Email Belum di Verifikasi, Harap Verifikasi Email terlebih dahulu, klik
                <a href="{{route('ortu.verify.send',$id)}}" class="btn btn-info btn-sm kirim_verify">Verifikasi Email</a>
            </p>
        </div>
    @endif


@else
    @php
        $id = Auth::user()->ortu()->first()->id;
    @endphp
    <div class="note note-info bg-red-sunglo">
        <p>
            Email Belum di Verifikasi, Harap Verifikasi Email terlebih dahulu, klik
            <a href="{{route('ortu.verify.send',$id)}}" class="btn btn-info btn-sm kirim_verify">Verifikasi Email</a>
        </p>
    </div>
@endif
@section('javascript')
    @parent
    <script src="{{asset('assets/global/plugins/bootbox/bootbox.min.js')}}" type="text/javascript"></script>
    <script>
        $('.kirim_verify').click(function(){
            bootbox.alert("Check Inbox / Spam pada Email {{Auth::user()->email}} Anda");
        });
    </script>
    {{--<script src="{{asset('assets/pages/scripts/ui-bootbox.min.js')}}" type="text/javascript"></script>--}}
    @endsection