<div class="modal fade" id="basic" tabindex="-1" role="basic" aria-hidden="true" style="margin-top: 10%">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title text-center">Pilih Pendaftar</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-6 margin-top-20">
                        <a href="{{route('receptionist.check',$puskes->id)}}" class="dashboard-stat blue decor">
                            <div class="visual">
                                <i class="icon-magnifier fa-icon-medium"></i>
                            </div>
                            <div class="details">
                                <div class="number"> Cari Data </div>
                                <div class="desc"> Pernah Daftar </div>

                            </div>
                            <div class="more" href="javascript:;"> Cari Data
                                <i class="m-icon-swapright m-icon-white"></i>
                            </div>
                        </a>
                    </div>
                    <div class="col-md-6 margin-top-20">
                        <a href="{{route('ortu.create',$puskes->id)}}" class="dashboard-stat green decor">
                            <div class="visual">
                                <i class="fa fa-user-plus fa-icon-medium"></i>
                            </div>
                            <div class="details">
                                <div class="number"> Tambah Data Baru </div>
                                <div class="desc"> Data Baru </div>
                            </div>
                            <div class="more" href="javascript:;"> Buat Data Baru
                                <i class="m-icon-swapright m-icon-white"></i>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn dark btn-outline" data-dismiss="modal">Kembali</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
@section('stylesheet')
    @parent
    <style>
        .dashboard-stat .details .number{
            font-size: 30px !important;
        }
        a.decor{
            text-decoration: none;
        }
    </style>
    @endsection
@section('javascript')
    @parent
    <script src="{{asset('assets/global/plugins/jquery-ui/jquery-ui.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/pages/scripts/ui-modals.min.js')}}" type="text/javascript"></script>
    @endsection