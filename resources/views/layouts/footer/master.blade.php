<div class="page-footer">
    <div class="page-footer-inner"> 2018 &copy; Kojyou project.
        <a href="http://apollo16team.com" title="Visit Us! Apollo 16 Team" target="_blank">apollo16team.com</a>
    </div>
    <div class="scroll-to-top">
        <i class="icon-arrow-up"></i>
    </div>
</div>