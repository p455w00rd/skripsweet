<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Ortu extends Model
{
    use SoftDeletes;

    protected $table = 'parent';

    protected $fillable = ['name','id_user','phone','nik','address','location'];

    public function user()
    {
        return $this->hasOne(User::class, 'id' , 'id_user');
    }
    public function kecamatan()
    {
        return $this->hasOne(Kecamatan::class, 'id' , 'location');
    }
    public function anak()
    {
        return $this->hasMany(Anak::class, 'id_parent', 'id' );
    }
    public function place()
    {
        return $this->belongsTo(ParentPlace::class, 'id', 'id_parent' );
    }

}
