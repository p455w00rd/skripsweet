<?php

namespace App\Providers;

use Illuminate\Support\Facades\Route;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;

class RouteServiceProvider extends ServiceProvider
{
    /**
     * This namespace is applied to your controller routes.
     *
     * In addition, it is set as the URL generator's root namespace.
     *
     * @var string
     */
    protected $namespace = 'App\Http\Controllers';

    /**
     * Define your route model bindings, pattern filters, etc.
     *
     * @return void
     */
    public function boot()
    {
        //

        parent::boot();
    }

    /**
     * Define the routes for the application.
     *
     * @return void
     */
    public function map()
    {
        $this->mapApiRoutes();

        $this->mapWebRoutes();

        $this->ortuWebRoutes();
        $this->puskesmasWebRoutes();
        $this->receptionistWebRoutes();
        $this->anakWebRoutes();
        $this->dinkesWebRoutes();
        $this->adminPuskesmasWebRoutes();
        $this->ImunizationWebRoutes();
        $this->LaporanWebRoutes();
        $this->DinkesAdminPuskesRoutes();
        $this->GISPuskesRoutes();
        $this->OrtuRoutes();
    }

    /**
     * Define the "web" routes for the application.
     *
     * These routes all receive session state, CSRF protection, etc.
     *
     * @return void
     */
    protected function mapWebRoutes()
    {
        Route::middleware('web')
             ->namespace($this->namespace)
             ->group(base_path('routes/web.php'));
    }

    /**
     * Define the "api" routes for the application.
     *
     * These routes are typically stateless.
     *
     * @return void
     */
    protected function mapApiRoutes()
    {
        Route::prefix('api')
             ->middleware('api')
             ->namespace($this->namespace)
             ->group(base_path('routes/api.php'));
    }
    protected function ortuWebRoutes()
    {
        Route::middleware(['web','auth'])
            ->namespace($this->namespace)
            ->group(base_path('app/Http/routes/pendaftar/ortu.php'));
    }
    protected function puskesmasWebRoutes()
    {
        Route::middleware(['web','auth'])
            ->namespace($this->namespace)
            ->group(base_path('app/Http/routes/puskesmas/dashboard.php'));
    }
    protected function anakWebRoutes()
    {
        Route::middleware(['web','auth'])
            ->namespace($this->namespace)
            ->group(base_path('app/Http/routes/pendaftar/anak.php'));
    }
    protected function receptionistWebRoutes()
    {
        Route::middleware(['web','auth'])
            ->namespace($this->namespace)
            ->group(base_path('app/Http/routes/puskesmas/receptionist.php'));
    }
    protected function dinkesWebRoutes()
    {
        Route::middleware(['web','auth'])
            ->namespace($this->namespace)
            ->group(base_path('app/Http/routes/admin/dinkes.php'));
    }
    protected function adminPuskesmasWebRoutes()
    {
        Route::middleware(['web','auth'])
            ->namespace($this->namespace)
            ->group(base_path('app/Http/routes/admin/puskesmas.php'));
    }
    protected function ImunizationWebRoutes()
    {
        Route::middleware(['web','auth'])
            ->namespace($this->namespace)
            ->group(base_path('app/Http/routes/puskesmas/imunization.php'));
    }
    protected function LaporanWebRoutes()
    {
        Route::middleware(['web','auth'])
            ->namespace($this->namespace)
            ->group(base_path('app/Http/routes/puskesmas/laporan.php'));
    }
    protected function DinkesAdminPuskesRoutes()
    {
        Route::middleware(['web','auth'])
            ->namespace($this->namespace)
            ->group(base_path('app/Http/routes/admin/user.php'));
    }

    protected function GISPuskesRoutes()
    {
        Route::middleware(['web','auth'])
            ->namespace($this->namespace)
            ->group(base_path('app/Http/routes/puskesmas/gis.php'));
    }
    protected function OrtuRoutes()
    {
        Route::middleware(['web','auth'])
            ->namespace($this->namespace)
            ->group(base_path('app/Http/routes/ortu/ortu.php'));
    }
}
