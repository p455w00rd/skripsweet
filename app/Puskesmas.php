<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Puskesmas extends Model
{
    use SoftDeletes;
    protected $table = 'puskesmas';

    protected $fillable = ['name','address', 'phone', 'type', 'location'];

    public function kecamatan()
    {
        return $this->hasOne(Kecamatan2::class, 'id' , 'location');
    }
    public function coordinate()
    {
        return $this->belongsTo(Puskes_place::class , 'id','id_puskesmas');
    }
    public function user()
    {
        return $this->belongsToMany(User::class, 'assigment', 'puskesmas_id' , 'user_id');
    }
    public function anak()
    {
        return $this->belongsToMany(Anak::class, 'regis_child', 'id_puskes' , 'id_child');
    }
    public function receptionist()
    {
        return $this->hasMany(Receptionist::class, 'id_puskes', 'id');
    }
    public function kelurahan()
    {
        return $this->belongsToMany(Kecamatan::class, 'puskes_kelurahan', 'puskesmas_id','kelurahan_id');
    }
    public function anak_created()
    {
        return $this->belongsToMany(Anak::class, 'regis_child', 'id_puskes','id_child')
            ->withPivot(['no_regis','created_at']);
    }

}
