<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Proyeksi extends Model
{
    protected $table = 'representation_surviving';

    protected $fillable = ['tahun','PL','PP','SL','SP','kelurahan','kecamatan'];

    public function kecamatan()
    {
        return $this->hasOne(Kecamatan2::class, 'id' , 'kecamatan');
    }
    public function kelurahan()
    {
        return $this->hasOne(Kecamatan::class, 'id' , 'kelurahan');
    }
}

