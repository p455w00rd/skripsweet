<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Kecamatan extends Model
{
    //database kelurahan
    protected $table = 'kecamatan';

    protected $fillable = ['name','id_kecamatan'];

    public function kecamatan()
    {
        return $this->belongsTo(Kecamatan::class, 'id' , 'id_kecamatan');
    }
    public function anak()
    {
        return $this->hasMany(Anak::class, 'location', 'id');
    }
    public function proyeksi()
    {
        return $this->hasOne(Proyeksi::class, 'kelurahan', 'id')->orderBy('created_at', 'DESC');
    }
    public function idl()
    {
        return $this->hasMany(Idl::class, 'location', 'id');
    }
    public function imunisasi()
    {
        return $this->hasMany(History_imun::class, 'location', 'id');
    }
    public function pro()
    {
        return $this->hasMany(Proyeksi::class, 'kelurahan', 'id');
    }
}
