<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Target extends Model
{
    protected $table = 'target_imunisasi';

    protected $fillable = ['target','id_imunisasi','tahun'];

    public function imun()
    {
        return $this->belongsTo(Imunisasi::class, 'id_imunisasi', 'id' );
    }
}
