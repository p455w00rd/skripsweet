<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Receptionist extends Model
{
    use SoftDeletes;

    protected $table = 'receptionist';

    protected $fillable = ['id_anak','id_puskes', 'queue','status'];

    public function anak()
    {
        return $this->hasOne(Anak::class, 'id' , 'id_anak');
    }
    public function puskesmas()
    {
        return $this->belongsTo(Puskesmas::class, 'id_puskes','id');
    }
}
