<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class History_imun extends Model
{
    use SoftDeletes;
    protected $table = 'history_imunisasi';

    protected $fillable = ['id_anak','id_puskesmas','id_imunisasi','location','gender','created_at','id_peg'];

    public function imun()
    {
        return $this->hasOne(Imunisasi::class, 'id' , 'id_imunisasi');
    }
    public function berat()
    {
        return $this->hasOne(Berat::class, 'id' , 'id_berat');
    }
    public function puskes()
    {
        return $this->hasOne(Puskesmas::class, 'id' , 'id_puskesmas');
    }
    public function anak()
    {
        return $this->hasOne(Anak::class, 'id' , 'id_anak')->first();
    }
    public function tanggal()
    {
        return date('d/m/Y', strtotime($this->attributes['created_at']));
    }
    public function pegawai()
    {
        return $this->hasOne(UserData::class, 'id' , 'id_peg');
    }
}
