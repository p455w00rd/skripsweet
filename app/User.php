<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Role;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'username', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
    public function setPasswordAttribute($password)
    {
        $this->attributes['password'] = bcrypt($password);
    }
    public function role()
    {
        return $this->belongsToMany(Role::class, 'users_roles', 'user_id' , 'role_id');
    }
    public function puskesmas()
    {
        return $this->belongsToMany(Puskesmas::class, 'assigment', 'user_id' , 'puskesmas_id');
    }
    public function user_data()
    {
        return $this->hasOne(UserData::class, 'user_id' , 'id');
    }
    public function ortu()
    {
        return $this->hasOne(Ortu::class, 'id_user' , 'id');
    }
    public function veri()
    {
        return $this->hasOne(MailVerification::class, 'user_id' , 'id');
    }
}
