<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Reschedule_imun extends Model
{
    protected $table = 'reschedule_imun';

    protected $fillable = ['id_anak','tanggal_kembali','status','id_puskesmas'];

    public function diff()
    {
        Carbon::setLocale('id');
        return Carbon::createFromTimeStamp(strtotime($this->attributes['tanggal_kembali']) )->diffForHumans();
    }

    public function anak()
    {
        return $this->hasOne(Anak::class, 'id', 'id_anak' );
    }
    public function puskesmas()
    {
        return $this->belongsTo(Puskesmas::class, 'id_puskesmas','id' );
    }
}
