<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserRole extends Model
{
    public $timestamps = false;
    protected $table = 'users_roles';

    protected $fillable = ['user_id','role_id'];
}
