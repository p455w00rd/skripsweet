<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Assigment extends Model
{
    public $timestamps = false;
    protected $table = 'assigment';

    protected $fillable = ['user_id','puskesmas_id'];
}
