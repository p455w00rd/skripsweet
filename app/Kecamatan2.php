<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Kecamatan2 extends Model
{
//    database kecamatan
    protected $table = 'kecamatan2';

    protected $fillable = ['name'];

    public function kelurahan()
    {
        return $this->hasMany(Kecamatan::class, 'id_kecamatan' , 'id');
    }
    public function puskesmas()
    {
        return $this->hasMany(Puskesmas::class,'location','id');
    }
}
