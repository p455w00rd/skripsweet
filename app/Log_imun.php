<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Log_imun extends Model
{
    use SoftDeletes;
    protected $table = 'log_imunization';

    protected $fillable = ['id_imunisasi','id_puskesmas','id_peg','id_anak'];

    public function puskesmas()
    {
        return $this->hasOne(Puskesmas::class,'id','id_puskesmas');
    }
    public function imunisasi()
    {
        return $this->hasOne(Imunisasi::class,'id','id_imunisasi');
    }
    public function peg()
    {
        return $this->hasOne(UserData::class,'id','id_peg');
    }
    public function anak()
    {
        return $this->hasOne(Anak::class,'id','id_anak');
    }
}
