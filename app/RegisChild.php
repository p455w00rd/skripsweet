<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RegisChild extends Model
{
    protected $table = 'regis_child';

    protected $fillable = ['id_child','id_puskes', 'no_regis'];

    public function anak1()
    {
        return $this->hasOne(Anak::class, 'id' , 'id_child');
    }
}
