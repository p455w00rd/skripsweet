<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Ortu;
use Carbon\Carbon;

class Anak extends Model
{
    protected $table = 'child';

    protected $fillable = ['name','id_parent','gender','pob','dob','address','location','keterangan','surviving'];


    public function diff()
    {
        Carbon::setLocale('id');
        return Carbon::createFromTimeStamp(strtotime($this->attributes['dob']) )->diffForHumans();
    }

    public function ortu()
    {
        return $this->hasOne(Ortu::class, 'id' , 'id_parent');
    }
    public function no()
    {
        return $this->hasMany(RegisChild::class, 'id_child' , 'id');
    }
    public function history_imun()
    {
        return $this->hasMany(History_imun::class, 'id_anak' , 'id');
    }
    public function kembali_imun()
    {
        return $this->hasMany(Reschedule_imun::class, 'id_anak' , 'id');
    }
    public function berat()
    {
        return $this->hasMany(Berat::class, 'id_anak' , 'id');
    }
    public function idl()
    {
        return $this->hasOne(Idl::class, 'id_anak' , 'id');
    }
    public function puskesmas()
    {
        return $this->belongsToMany(Puskesmas::class, 'regis_child', 'id_child','id_puskes');
    }
    public function receptionist()
    {
        return $this->hasMany(Receptionist::class, 'id_anak' , 'id');
    }
//    public function anak_created()
//    {
//        return $this->belongsToMany(Anak::class, 'regis_child', 'id_puskes' , 'id_child')->withPivot('no_regis');
//    }
    public function anak_created()
    {
        return $this->belongsToMany(Puskesmas::class, 'regis_child', 'id_child','id_puskes')
            ->withPivot(['no_regis','created_at']);
    }

}
