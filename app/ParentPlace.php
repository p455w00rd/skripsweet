<?php

namespace App;

use Grimzy\LaravelMysqlSpatial\Eloquent\SpatialTrait;
use Illuminate\Database\Eloquent\Model;

class ParentPlace extends Model
{
    use SpatialTrait;
    protected $table = 'point_parent';

    protected $fillable = [
        'id_parent'
    ];

    protected $spatialFields = [
        'location'
    ];
    public function parent()
    {
        return $this->belongsTo(Ortu::class, 'id_parent' , 'id');
    }
}
