<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PuskesKelurahan extends Model
{
    public $timestamps = false;
    protected $table = 'puskes_kelurahan';

    protected $fillable = ['kecamatan_id','puskesmas_id'];
}
