<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserData extends Model
{
    protected $table = 'user_data';

    protected $fillable = ['name','id_user','phone','nip'];

    public function imun()
    {
        return $this->hasMany(History_imun::class, 'id_peg' , 'id');
    }
}
