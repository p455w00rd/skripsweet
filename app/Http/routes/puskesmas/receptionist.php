<?php

Route::prefix('puskesmas/{pusdat}/receptionist')->group(function ()
{
    Route::get('', 'ReceptionistController@index')->name('receptionist.index');
    Route::get('check', 'ReceptionistController@check')->name('receptionist.check');
    Route::post('delete/{recept}', 'ReceptionistController@delete')->name('receptionist.delete');
    Route::post('cari', 'ReceptionistController@cari')->name('receptionist.cari');
//    Route::post('add/{id}', 'ReceptionistController@add')->name('receptionist.add');
    Route::get('add/{id}', 'ReceptionistController@add')->name('receptionist.add');
    Route::post('add/{id}/regis', 'ReceptionistController@regis')->name('receptionist.regis');

});

//Route::get('test', 'OrtuController@index')->name('test');