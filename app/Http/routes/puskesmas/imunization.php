<?php
Route::prefix('puskesmas/{pusdat}/imunisasi')->group(function ()
{
    Route::get('record/{id}/add', 'ImunizationController@create_langsung')->name('imunization.add');
    Route::get('record/{id}/urut/{recept}', 'ImunizationController@create')->name('imunization.record');
    Route::post('record/{id}', 'ImunizationController@post')->name('imunization.record.post');
    Route::get('record/{id}/delete/{i}', 'ImunizationController@delete')->name('imunization.record.delete');
    Route::post('record/{id}/update/{i}', 'ImunizationController@update')->name('imunization.record.update');

});
