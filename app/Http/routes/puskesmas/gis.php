<?php
Route::prefix('puskesmas/{pusdat}/pemetaan')->group(function ()
{
    Route::get('/', 'GISController@index')->name('pemetaan');
    Route::get('near', 'GISController@near')->name('gis.near');
    Route::get('details/{y}/{kel}', 'GISController@details')->name('pemetaan.details');

});
