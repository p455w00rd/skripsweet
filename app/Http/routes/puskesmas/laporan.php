<?php

Route::prefix('puskesmas/{pusdat}/laporan')->group(function ()
{
    Route::get('pws', 'LaporanPuskesController@pws')->name('laporan.pws');
    Route::get('pencatatan', 'LaporanPuskesController@pencatatan')->name('laporan.pencatatan');
    Route::get('pencatatan/excel', 'LaporanPuskesController@pencatatanExcel')->name('laporan.pencatatan.excel');
    Route::get('hb24jam', 'LaporanPuskesController@hb')->name('laporan.hb24jam');
    Route::get('hb24jam/excel', 'LaporanPuskesController@hbExcel')->name('laporan.hb24jam.excel');
    Route::get('hb1-7hari', 'LaporanPuskesController@hb_hari')->name('laporan.hb1-7hari');
    Route::get('hb1-7hari/excel', 'LaporanPuskesController@hb_hariExcel')->name('laporan.hb1-7hari.excel');
    Route::get('hb>7hari', 'LaporanPuskesController@hb_minggu')->name('laporan.hb>7hari');
    Route::get('hb>7hari/excel', 'LaporanPuskesController@hb_mingguExcel')->name('laporan.hb>7hari.excel');
    Route::get('imunisasi/{id}', 'LaporanPuskesController@other')->name('laporan.other');
    Route::get('imunisasi/{id}/excel', 'LaporanPuskesController@otherExcel')->name('laporan.other.excel');
    Route::get('idl', 'LaporanPuskesController@idl')->name('laporan.idl');
    Route::get('idl/excel', 'LaporanPuskesController@idlExcel')->name('laporan.idl.excel');
    Route::get('log', 'LogController@index')->name('laporan.log');
});