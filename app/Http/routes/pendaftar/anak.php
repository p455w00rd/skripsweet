<?php
Route::prefix('puskesmas/{pusdat}/anak')->group(function ()
{
    Route::get('', 'AnakController@index')->name('anak.index');
    Route::get('all', 'AnakController@semua')->name('anak.all');
    Route::get('no_regis', 'AnakController@no_regis')->name('anak.no_regis');
    Route::get('{id}/profile', 'AnakController@profile')->name('anak.profile');
    Route::get('{id}/account', 'AnakController@account')->name('anak.account');
    Route::get('{id}/height', 'AnakController@height')->name('anak.height');
    Route::get('{id}/details', 'AnakController@details')->name('anak.keterangan');
    Route::post('{id}/update', 'AnakController@update')->name('anak.update');
    Route::get('{id}/create', 'AnakController@create')->name('anak.create');
    Route::post('{id}/create', 'AnakController@post')->name('anak.create');
    Route::post('{id}/regis', 'AnakController@regis')->name('anak.regis');
    Route::get('{id}/update/details', 'AnakController@update_details')->name('anak.update.details');
});