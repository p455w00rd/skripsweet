<?php

Route::prefix('puskesmas/{pusdat}/ortu')->group(function ()
{
    Route::get('', 'OrtuController@index')->name('ortu.index');
    Route::get('create', 'OrtuController@create')->name('ortu.create');
    Route::post('post', 'OrtuController@post')->name('ortu.post');
    Route::get('{id}/profile', 'OrtuController@profile')->name('ortu.profile');
    Route::get('{id}/child', 'OrtuController@anak')->name('ortu.child');
    Route::get('{id}/account', 'OrtuController@account')->name('ortu.account');
    Route::post('{id}/account', 'OrtuController@update_account')->name('ortu.account');
    Route::post('{id}/profile', 'OrtuController@update_profile')->name('ortu.profile');
    Route::get('{id}/map', 'OrtuController@map')->name('ortu.map');
    Route::post('{id}/editmap', 'OrtuController@editmap')->name('ortu.editmap');
});

//Route::get('test', 'OrtuController@index')->name('test');
