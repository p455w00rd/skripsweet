<?php
Route::prefix('ortu/{id}')->group(function ()
{
    Route::get('', 'OrtuController@dashboard')->name('ortu.dashboard');
    Route::get('data', 'OrtuController@ortu_data')->name('lengkapi');
    Route::post('data', 'OrtuController@post_ortu_data')->name('lengkapi_post');
    Route::get('near', 'OrtuController@near')->name('ortu.near');
    Route::get('profile', 'OrtuController@profile_me')->name('ortu.profile.me');
    Route::post('profile/update', 'OrtuController@profile_update_me')->name('ortu.profile.update');
    Route::get('account', 'OrtuController@account_me')->name('ortu.account.me');
    Route::post('account', 'OrtuController@account_me_update')->name('ortu.account.me');
    Route::get('map', 'OrtuController@map_me')->name('ortu.map.me');
    Route::post('map/edit', 'OrtuController@map_me_update')->name('ortu.edit.map');
    Route::get('anak', 'OrtuController@anak_me')->name('ortu.anak.me');
    Route::get('create/anak', 'OrtuAnakController@create')->name('ortu.anak.create');
    Route::post('create/anak', 'OrtuAnakController@post')->name('ortu.anak.create');

});
Route::prefix('ortu/{id}/anak/{anak}')->group(function (){
    Route::get('profile', 'OrtuAnakController@profile')->name('ortu.anak.profile');
    Route::get('height', 'OrtuAnakController@height')->name('ortu.anak.height');
    Route::get('details', 'OrtuAnakController@details')->name('ortu.anak.details');
    Route::post('details', 'OrtuAnakController@update_details')->name('ortu.anak.details');
    Route::get('account', 'OrtuAnakController@account')->name('ortu.anak.account');
    Route::post('account', 'OrtuAnakController@update')->name('ortu.anak.account');
});
Route::prefix('ortu/{id}/puskesmas')->group(function (){
    Route::get('', 'OrtuPuskesController@index')->name('ortu.puskesmas.index');
    Route::post('queue/add', 'OrtuPuskesController@add')->name('ortu.puskesmas.queue.add');
    Route::get('queue/list', 'OrtuPuskesController@show')->name('ortu.puskesmas.queue.list');
    Route::get('queue/{puskesmas}/puskesmas', 'OrtuPuskesController@queue')->name('ortu.puskesmas.queue');
});
Route::prefix('ortu/{id}/calendar')->group(function (){
    Route::get('', 'CalendarController@index')->name('ortu.calendar.index');
    Route::get('{calendar}/show', 'CalendarController@show')->name('ortu.calendar.show');
    Route::get('{calendar}/andro', 'CalendarController@andro')->name('ortu.calendar.andro');
});
Route::prefix('ortu/{id}/verify')->group(function (){
    Route::get('send', 'OrtuController@send_verify')->name('ortu.verify.send');

});