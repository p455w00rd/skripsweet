<?php
Route::prefix('admin/puskesmas/{pusdat}/user')->group(function ()
{
    Route::get('', 'AdminPuskesController@index')->name('puskesmas.user.index');
    Route::get('create', 'AdminPuskesController@create')->name('puskesmas.user.create');
    Route::post('post', 'AdminPuskesController@post')->name('puskesmas.user.post');
    Route::get('{id}/edit/{code}', 'AdminPuskesController@editProfile')->name('puskesmas.user.edit');
    Route::post('{id}/{userData}/update/{code}', 'AdminPuskesController@update')->name('puskesmas.user.update');
});
Route::prefix('admin/puskesmas/{pusdat}')->group(function ()
{
    Route::get('edit', 'AdminPuskesController@edit')->name('puskesmas.edit');
    Route::get('imunisasi', 'AdminPuskesController@imunisasi')->name('puskesmas.imunisasi');

});
Route::prefix('admin/puskesmas/surviving/{pusdat}')->group(function ()
{
    Route::get('index', 'SurvivingController@index')->name('surviving.index');
    Route::get('edit/{id}', 'SurvivingController@edit')->name('surviving.edit');
    Route::post('update', 'SurvivingController@update')->name('surviving.update');

});