<?php
Route::prefix('dinkes/user/puskesmas')->group(function ()
{
    Route::get('', 'DinkesUserPuskesController@index')->name('dinkes.user.puskesmas');
    Route::get('create', 'DinkesUserPuskesController@create')->name('dinkes.user.puskesmas.create');
    Route::post('post', 'DinkesUserPuskesController@post')->name('dinkes.user.puskesmas.post');
});
Route::prefix('dinkes/user/dinkes')->group(function ()
{
    Route::get('', 'DinkesUserController@index')->name('dinkes.user');
    Route::get('create', 'DinkesUserController@create')->name('dinkes.user.create');
    Route::post('post', 'DinkesUserController@post')->name('dinkes.user.post');
    Route::get('edit/{id}/{userData}/edit/{code}', 'DinkesUserController@edit')->name('dinkes.user.edit');
    Route::post('edit/{id}/{userData}/update/{code}', 'DinkesUserController@update')->name('dinkes.user.update');
});