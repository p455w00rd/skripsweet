<?php
Route::prefix('dinkes/puskesmas')->group(function ()
{
    Route::get('', 'PuskesmasController@index')->name('puskesmas.dinkes');
    Route::get('create', 'PuskesmasController@create')->name('puskesmas.dinkes.create');
    Route::post('store', 'PuskesmasController@store')->name('puskesmas.dinkes.store');
    Route::get('{id}/edit', 'PuskesmasController@edit')->name('puskesmas.dinkes.edit');
    Route::post('{id}/update', 'PuskesmasController@update')->name('puskesmas.dinkes.update');

});
Route::prefix('dinkes/imunisasi')->group(function ()
{
    Route::get('', 'ImunisasiController@index')->name('imunisasi.dinkes');
    Route::get('create', 'ImunisasiController@create')->name('imunisasi.create.dinkes');
    Route::post('post', 'ImunisasiController@post')->name('imunisasi.post.dinkes');
    Route::get('{id}/edit', 'ImunisasiController@edit')->name('imunisasi.edit.dinkes');
    Route::post('{id}/update', 'ImunisasiController@update')->name('imunisasi.update.dinkes');
    Route::get('{id}/show', 'ImunisasiController@show')->name('imunisasi.show.dinkes');
    Route::get('{id}/target/add', 'ImunisasiController@target_add')->name('imunisasi.add.target.dinkes');
    Route::post('{id}/target/add', 'ImunisasiController@target_post')->name('imunisasi.add.target.dinkes');
    Route::post('{id}/target/edit', 'ImunisasiController@target_edit')->name('imunisasi.edit.target.dinkes');
});
Route::prefix('dinkes/laporan')->group(function ()
{
    Route::get('', 'LaporanDinkesController@index')->name('laporan.dinkes');
    Route::get('{id}/imunisasi', 'LaporanDinkesController@laporan')->name('laporan.imunisasi.dinkes');
    Route::get('gis', 'LaporanDinkesController@gis')->name('laporan.imunisasi.dinkes.gis');
    Route::get('idl', 'LaporanDinkesController@idl')->name('laporan.imunisasi.dinkes.idl');
    Route::get('gis/{y}/{kel}', 'LaporanDinkesController@gisdetail')->name('laporan.imunisasi.dinkes.gis.detail');


});
Route::prefix('dinkes/proyeksi')->group(function ()
{
    Route::get('', 'ProyeksiController@index')->name('proyeksi');
    Route::get('process/{id}', 'ProyeksiController@proses')->name('proses.proyeksi');
    Route::get('create', 'ProyeksiController@create')->name('create.proyeksi');
    Route::post('post', 'ProyeksiController@post')->name('post.proyeksi');
    Route::post('store', 'ProyeksiController@postCreate')->name('store.proyeksi');
});