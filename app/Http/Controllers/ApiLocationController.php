<?php

namespace App\Http\Controllers;

use App\Kecamatan2;
//use Illuminate\Http\Request;
//use Illuminate\Http\Response;
use Illuminate\Support\Facades\Input;

class ApiLocationController extends Controller
{
    public function findKelurahan(){
        $input = Input::get('id_kecamatan');
        $maker = Kecamatan2::find($input);
        $models = $maker->kelurahan()->get();
        return response()->json($models);
    }

}
