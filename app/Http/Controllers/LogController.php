<?php

namespace App\Http\Controllers;

use App\Puskesmas;
use Illuminate\Http\Request;

class LogController extends Controller
{
    public function index($pusdat)
    {
        $puskes = Puskesmas::findOrFail($pusdat);

        return view('app.puskesmas.laporan.log',['puskes'=>$puskes]);
    }
}
