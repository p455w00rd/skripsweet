<?php

namespace App\Http\Controllers;

use App\Kecamatan2;
use App\Proyeksi;
use Illuminate\Http\Request;

class ProyeksiController extends Controller
{
    public function index()
    {
        $proyeksi=Proyeksi::all();

        return view('app.dinkes.proyeksi.index',['proyeksi'=>$proyeksi]);
    }
    public function proses($id)
    {
        $proyeksi1=Proyeksi::findOrFail($id);
        $proyeksi2=Proyeksi::where('tahun',$proyeksi1->tahun-1)
            ->where('kelurahan',$proyeksi1->kelurahan)
            ->first();

        //Menghitung R
        $RL=($proyeksi1->PL/$proyeksi2->PL)-1;
        $RP=($proyeksi1->PP/$proyeksi2->PP)-1;

        //Menghitung Nilai Proyeksi
        $PL=round($proyeksi1->PL*(1+$RL));
        $PP=round($proyeksi1->PL*(1+$RP));

        return view('app.dinkes.proyeksi.proses',['proyeksi1'=>$proyeksi1,'proyeksi2'=>$proyeksi2,'RL'=>$RL,'RP'=>$RP,
            'PL'=>$PL,'PP'=>$PP]);
    }
    public function post(Request $request)
    {
        $id=$request->input('id');
        $proyeksi1=Proyeksi::findOrFail($id);

        $proyeksi = new Proyeksi();
        $proyeksi->tahun = $proyeksi1->tahun+1;
        $proyeksi->PL =$request->input('PL');
        $proyeksi->PP =$request->input('PP');
        $proyeksi->kelurahan =$proyeksi1->kelurahan;
        $proyeksi->kecamatan =$proyeksi1->kecamatan;

        $proyeksi1->exp =1;
        $proyeksi1->save();
        $proyeksi->save();

        return redirect(route('proyeksi'));


    }
    public function create()
    {
        $kecamatan = Kecamatan2::all();

        return view('app.dinkes.proyeksi.create',['kecamatan'=>$kecamatan]);
    }
    public function postCreate(Request $request)
    {

        $proyeksi = new Proyeksi();
        $proyeksi->tahun = $request->input('tahun');
        $proyeksi->PL =$request->input('pl');
        $proyeksi->PP =$request->input('pp');
        $proyeksi->kelurahan =$request->input('kelurahan');
        $proyeksi->kecamatan =$request->input('kecamatan');

        $proyeksi->save();

        return redirect(route('proyeksi'));


    }
}
