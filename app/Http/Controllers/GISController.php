<?php

namespace App\Http\Controllers;

use App\Idl;
use App\Imunisasi;
use App\Kecamatan;
use App\ParentPlace;
use App\Puskes_place;
use App\Puskesmas;
use App\Target;
use Carbon\Carbon;
use Illuminate\Http\Request;

class GISController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index($pusdat, Request $request)
    {
        if ($request->input('year')==null){
            $y = Carbon::now()->year;
        }
        else{
            $y = $request->input('year');
        }

        $puskes=Puskesmas::findOrFail($pusdat);

        $data=$puskes->kelurahan()->get();
        foreach ($data as $key=>$datas){
            $kel[$key]=$datas->name;
        }
        $kel=collect($kel)->implode(',');

        $sd = Idl::where('id_puskesmas','=',$pusdat)
            ->whereYear('created_at',$y);

        return view('app.puskesmas.laporan.gis.index',['puskes'=>$puskes,'kel'=>$kel,'sd'=>$sd,'y'=>$y]);
    }
    public function near($pusdat)
    {

        $puskes=Puskesmas::findOrFail($pusdat);
        $near=$puskes->coordinate()->first();
//        dd($near);
        if(isset($near)) {
            $near=$puskes->coordinate()->first()->location;
            $distsp = ParentPlace::distanceExcludingSelf('location', $near, 0.02)->get();

            $get_distance_range = ParentPlace::select('*')
                ->selectRaw('(GLength(LineStringFromWKB(LineString(`location`,GeomFromText(\'POINT(' . $near->getLng() . ' ' . $near->getLat() . ')\'))))) AS distance')
                ->get();
//        return [$distsp,$near->getLat()];
        }
        else{
            $distsp = null;
            $near = null;
        }

        return view('app.puskesmas.gis.near',['puskes'=>$puskes,'near'=>$near,'dist'=>$distsp]);
    }

    public function details($pusdat,$y,$kel, Request $request)
    {
        $puskes=Puskesmas::findOrFail($pusdat);
        $imunisasi=Imunisasi::whereNotIn('id',[12,13,15,14,16])
            ->with(array('target' => function($q) use($y)
            {
                $q->where('tahun',$y);
            }))
            ->get();
        $target = Target::where('tahun',$y)->get();
        $kelurah = Kecamatan::where('Name',$kel)
            ->with(array('idl' => function($q) use($pusdat,$y)
            {
                $q->where('id_puskesmas',$pusdat)
                    ->whereYear('created_at',$y);
            }))
            ->with(array('imunisasi' => function($q) use($pusdat,$y)
            {
                $q->where('id_puskesmas',$pusdat)
                    ->whereYear('created_at',$y);
            }))
            ->with(array('pro' => function($q) use($y)
            {
                $q->where('tahun',$y);
            }))
            ->first();
        return view('app.puskesmas.laporan.gis.details',['puskes'=>$puskes,'data'=>$kelurah,'y'=>$y,
            'kelurahan'=>$kel,'imunisasi'=>$imunisasi,'target'=>$target]);
    }
}
