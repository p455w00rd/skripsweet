<?php

namespace App\Http\Controllers;

use App\Ortu;
use App\Puskes_place;
use App\Puskesmas;
use App\Receptionist;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;

class OrtuPuskesController extends Controller
{
    public function index($id)
    {
        $ortu=Ortu::findOrFail($id);
        $puskesall =Puskesmas::all();

        $near=$ortu->place()->first();
        if ($near != null){
            $near = $near->location;


        $get_distance_range = Puskes_place::select('*')
            ->selectRaw('(GLength(LineStringFromWKB(LineString(`location`,GeomFromText(\'POINT('.$near->getLng().' '.$near->getLat().')\'))))) AS distance')
            ->orderBy('distance','asc')
            ->limit(6)
            ->get();
        }
        else{
            $near = null;
            $get_distance_range = null;

//            $get_distance_range->merge(['img' => 'data']);
//            dd($get_distance_range);
        }
        return view('app.ortu.pendaftaran.index',['ortu' =>$ortu,'id' => $id,'puskesmas'=>$get_distance_range,
            'puskesall' => $puskesall]);
    }
    public function queue($id,$puskesmas)
    {
        $ortu=Ortu::findOrFail($id);
        $pusk=Puskesmas::findOrFail($puskesmas);

        return view('app.ortu.pendaftaran.queue',['ortu' =>$ortu,'id' => $id,'pusk'=>$pusk]);
    }
    public function add($id)
    {
        $anak = Input::get('id_anak');
        $pusk = Input::get('id_puskesmas');
        return  redirect(route('receptionist.add',[$pusk,$anak]));

    }
    public function show($id)
    {
        $ortu=Ortu::findOrFail($id);
        $anak=$ortu->anak()->get();
        foreach ($anak as $key=>$datas){
            $kel[$key]=$datas->id;
        }
        $now = Carbon::now()->toDateString();

        $recept = Receptionist::whereIn('id_anak',$kel)
            ->whereDate('created_at', '>=', date('Y-m-d'))
            ->get();

        return view('app.ortu.pendaftaran.show',['ortu' =>$ortu,'id' => $id,'recept'=>$recept]);


    }
}
