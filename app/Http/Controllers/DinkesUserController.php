<?php

namespace App\Http\Controllers;

use App\User;
use App\UserData;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class DinkesUserController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $user=User::whereHas('role', function($query)
        {
            $query->whereIn('roles.id', [1,5]);
        })->get();
        $c=1;
//        dd($role);
//        $user = $role->user()->get();

        return view('app.admin.dinkes.user.dinkes.index',['user' => $user,'c'=>$c]);
    }

    public function create()
    {
//        $super = Puskesmas::all();

        return view('app.admin.dinkes.user.dinkes.create');
    }

    public function post(Request $request){
        $validator = Validator::make($request->all(), [
            'username'         => 'required|unique:users',       // just a normal required validation
            'email'            => 'required|email|unique:users',  // required and must be unique in the ducks table
            'password'         => 'required',
            'password_confirm' => 'required|same:password',    // required and has to match the password field
            'role' => 'required',
            'nip'             => 'required|unique:user_data|integer',
            'phone'           => 'required',// just a normal required validation
            'name'     => 'required',  // required and must be unique in the ducks table
        ],
            [
//                'password_confirm.same'=>'password harus sama',
//                'required'=>'harus di isi'
            ]);

        if ($validator->fails()) {

            return redirect(route('dinkes.user.create'))
                ->withErrors($validator)
                ->withInput();

        }else {

            $user = new User();
            $user->username = $request->input('username');
            $user->email = $request->input('email');
            $user->password = $request->input('password');

            //Save Pegawai
            $ortu = new UserData();
            $ortu->name = $request->input('name');
            $ortu->nip = $request->input('nip');
            $ortu->phone = $request->input('phone');

            $user->save();
            $newUser = $user->id;
            $ortu->user_id = $newUser;

            $ortu->save();

            User::find($newUser)->role()->sync($request->input('role'));

            return redirect(route('dinkes.user'));
        }
    }
    public function update(Request $request,$id,$code,$userData){
        $validator = Validator::make($request->all(), [
            'username'         => 'required',       // just a normal required validation
            'email'            => 'required',  // required and must be unique in the ducks table
            'role' => 'required',
            'nip'             => 'required|integer',
            'phone'           => 'required',// just a normal required validation
            'name'     => 'required',  // required and must be unique in the ducks table
        ],
            [
//                'password_confirm.same'=>'password harus sama',
//                'required'=>'harus di isi'
            ]);

        if ($validator->fails()) {

            return redirect(route('dinkes.user.edit',[$id,$userData,$code]))
                ->withErrors($validator)
                ->withInput();

        }else {
            if ($request->input('password') != null) {
                $user = User::findOrFail($id);
                $user->username = $request->input('username');
                $user->email = $request->input('email');
                $user->password = $request->input('password');
            }
            else{
                $user = User::findOrFail($id);
                $user->username = $request->input('username');
                $user->email = $request->input('email');
            }
            //Save Pegawai
            $ortu = UserData::findOrFail($userData);
            $ortu->name = $request->input('name');
            $ortu->nip = $request->input('nip');
            $ortu->phone = $request->input('phone');

            $user->save();
            $ortu->save();

            User::find($user->id)->role()->sync($request->input('role'));

            if ($code ==2){
                return redirect(route('dinkes.user.index'));
            }
            else{
                return redirect('home');
            }
        }
    }
    public function edit($id,$code){
        $user = User::findOrFail($id);
        return view('app.admin.dinkes.user.dinkes.edit',['user'=>$user,'code'=>$code]);
    }
}
