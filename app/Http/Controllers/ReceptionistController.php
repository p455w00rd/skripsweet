<?php

namespace App\Http\Controllers;

use App\Anak;
use App\Ortu;
use App\Puskesmas;
use App\Receptionist;
use App\RegisChild;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;

class ReceptionistController extends Controller
{
    public function index($pusdat)
    {
        $puskes=Puskesmas::findOrFail($pusdat);
        $get = $puskes->receptionist();
        $data = $get->where('created_at','>=', Carbon::now()->toDateString())->get();
//        dd($data);
        return view('app.puskesmas.receptionist.index',['puskes'=>$puskes,'data'=>$data]);
    }
    public function check($pusdat)
    {
        $puskes=Puskesmas::findOrFail($pusdat);
        return view('app.puskesmas.receptionist.check.baru_lama',['puskes'=>$puskes]);
    }
    public function add($pusdat,$id)
    {
        $now = Carbon::now()->format('Y-m-d');
        $anak = Anak::where('id',$id)
            ->whereHas('receptionist', function ($query) use($pusdat,$now) {
                $query->where('id_puskes', $pusdat)
                    ->where('status',0)
                    ->whereDate('created_at',$now);
            })->get();
        $puskes=Puskesmas::findOrFail($pusdat);
        $get = $puskes->receptionist()->get();
        $q = $get->where('created_at','>=', Carbon::now()->toDateString())->last();
        if (isset($q)){
            $q = $q->queue+1;
        }
        else{
            $q=1;
        }
        if ($anak->count() != 1){
            $recept = new Receptionist();
            $recept->queue    = $q;
            $recept->id_anak    = $id;
            $recept->id_puskes =  $pusdat;
            $recept->save();
            if (Auth::user()->role()->first()->id != 3){
                return redirect(route('receptionist.index',$pusdat));
            }
            else{
                return redirect(route('ortu.dashboard',Auth::user()->ortu()->first()->id));
            }
        }
        else{
            if (Auth::user()->role()->first()->id != 3){
                return redirect(route('receptionist.index',$pusdat));
            }
            else{
                return redirect(route('ortu.dashboard',Auth::user()->ortu()->first()->id));
            }
        }

    }
    public function cari(Request $request, $pusdat)
    {
        $puskes=Puskesmas::findOrFail($pusdat);
        switch ($request->jenis){

            case 1: //cari NIK Ortu
                $data = Ortu::where('nik',$request->nomor)
                    ->with('anak')
                    ->first();
                $case = 1;
                return view('app.puskesmas.receptionist.check.hasil.ortu',['data'=> $data,'puskes'=>$puskes,'case'=>$case]);
                break;

            case 2: //Cari NIK Anak
                $data = Anak::where('nik','like',$request->nomor)
                    ->get();
                $case = 2;
                return view('app.puskesmas.receptionist.check.hasil.anak',['data'=> $data,'puskes'=>$puskes,'case'=>$case]);
                break;

            case 3: //Cari NO PENDAFTARAN
                $data = RegisChild::where('no_regis','like',$request->nomor)
                    ->where('id_puskes',$pusdat)
                    ->get();
                $case = 3;
                return view('app.puskesmas.receptionist.check.hasil.regis',['data'=> $data,'puskes'=>$puskes]);
                break;
        }

    }
    public function regis(Request $request, $pusdat, $id)
    {
        $anak = Anak::findOrFail($id);
        $res = $anak->receptionist();
        $now = Carbon::now()->format('Y-m-d');
        $q = $res->where('id_puskes', $pusdat)
            ->where('status',0)
            ->whereDate('created_at',$now)
            ->get();

        $puskes=Puskesmas::findOrFail($pusdat);
        $validator = Validator::make($request->all(), [

            'pendaftaran'             => 'required|unique:regis_child,no_regis',

        ],
            [
//                'password_confirm.same'=>'password harus sama',
//                'required'=>'harus di isi'
            ]);

        if ($validator->fails()) {

            return view('app.puskesmas.receptionist.check.hasil.err',['puskes'=>$puskes]);

        }
        else{
            $regis = new RegisChild();
            $regis->id_child   = $id;
            $regis->id_puskes =  $pusdat;
            $regis->no_regis = $request->input('pendaftaran');

            $regis->save();

            if ($q->count() != 0){
                return redirect()->route('receptionist.index',$pusdat);
            }
            else{
                return redirect(route('receptionist.add',[$pusdat,$id]));
            }



        }
    }
    public function delete($pusdat,$recept)
    {
        Receptionist::destroy($recept);

        return redirect()->route('receptionist.index',$pusdat);
    }
}
