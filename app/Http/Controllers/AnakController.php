<?php

namespace App\Http\Controllers;

use App\Anak;
use App\Kecamatan;
use App\Ortu;
use App\RegisChild;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use App\Puskesmas;

class AnakController extends Controller
{
    public function index($pusdat){
        $puskes=Puskesmas::findOrFail($pusdat);
//        $anak = $puskes->anak()->get();
        return view('app.puskesmas.pendaftar.child.index',['puskes'=>$puskes]);
    }
    public function semua($pusdat){
        $puskes=Puskesmas::findOrFail($pusdat);
        $count = 1;
//        $anak = Anak::all();
        return view('app.puskesmas.pendaftar.child.global',['puskes'=>$puskes,'count' => $count]);
    }
    public function profile($pusdat,$id){

        $puskes=Puskesmas::findOrFail($pusdat);
        $anak = Anak::find($id);
        $anak->dob = Carbon::parse($anak->dob);
//        $anak->dob = Carbon::parse('2019-05-17');
        $dob = $anak->dob->diffInMonths();
        return view('app.puskesmas.pendaftar.child.profile.index',['child'=>$anak,'puskes'=>$puskes,'dob'=>$dob]);
    }
    public function height($pusdat,$id){

        $puskes=Puskesmas::findOrFail($pusdat);
        $anak = Anak::find($id);
        $i=1;
        return view('app.puskesmas.pendaftar.child.profile.height',['i'=>$i,'child'=>$anak,'puskes'=>$puskes]);
    }
    public function details($pusdat,$id){

        $puskes=Puskesmas::findOrFail($pusdat);
        $anak = Anak::find($id);
        $i=1;
        return view('app.puskesmas.pendaftar.child.profile.keterangan',['i'=>$i,'child'=>$anak,'puskes'=>$puskes]);
    }
    public function account($pusdat,$id){

        $puskes=Puskesmas::findOrFail($pusdat);
        $kecamatan = Kecamatan::all();
        $anak = Anak::find($id);
        return view('app.puskesmas.pendaftar.child.profile.account',['puskes'=>$puskes,'child'=>$anak, 'kecamatan'=>$kecamatan]);
    }
    public function update($pusdat,$id, Request $request){

        $anak = Anak::findOrFail($id);
        $dob = Carbon::parse($request->input('dob'))->format('Y-m-d');
        Input::merge(['dob' => $dob]);
        $input = $request->all();

//        dd($request->input('dob'));
//        dd($input);
        $anak->fill($input)->save();

        return redirect()->route('anak.profile',[$pusdat,$id]);
    }

    public function update_details($pusdat,$id, Request $request){
        if ($request->input('surviving')==true){
            $surviving = 1;
        }
        else{
            $surviving= 0;
        }
        $anak = Anak::findOrFail($id);
        $anak->keterangan = $request->input('keterangan');
        $anak->surviving = $surviving;
        $anak->save();

        return redirect()->route('anak.profile',[$pusdat,$id]);
    }

    public function create($pusdat,$id){
        $ortu=Ortu::findOrFail($id);
        $puskes=Puskesmas::findOrFail($pusdat);
        return view('app.puskesmas.pendaftar.child.create',['puskes'=>$puskes,'ortu'=>$ortu]);
    }

    public function post($pusdat,$id, Request $request){

        $validator = Validator::make($request->all(), [
            
            'name'             => 'required',
            'pob'           => 'required',// just a normal required validation
            'dob'     => 'required',  // required and must be unique in the ducks table
            'gender'  => 'required',
            'pendaftaran'           => 'required|unique:regis_child,no_regis',
        ],
            [
//                'password_confirm.same'=>'password harus sama',
//                'required'=>'harus di isi'
            ]);
        $ortu=Ortu::findOrFail($id);

        if ($validator->fails()) {

//            $user = new User();
//            $user=User::find($user->id);
//            dd($user);
//            $user->delete();
            return redirect(route('anak.create',[$pusdat,$ortu->id]))
                ->withErrors($validator)
                ->withInput();

        }else{
            $anak = new Anak();

            $anak->name = $request->input('name');
            $anak->nik = $request->input('nik');
            $anak->pob = $request->input('pob');
            $anak->dob = Carbon::parse($request->input('dob'))->format('Y-m-d');
            $anak->gender = $request->input('gender');
            $anak->address = $ortu->address;
            $anak->location = $ortu->location;
            $anak->id_parent = $ortu->id;

            $anak->save();
            $idanak=$anak->id;

            $regis = new RegisChild();

            $regis->id_puskes = $pusdat;
            $regis->no_regis = $request->input('pendaftaran');
            $regis->id_child = $idanak;
            $regis->save();

            return redirect()->route('anak.profile',[$pusdat,$idanak]);
        }

    }
    public function regis(Request $request, $pusdat, $id)
    {
        $puskes=Puskesmas::findOrFail($pusdat);
        $validator = Validator::make($request->all(), [

            'pendaftaran'             => 'required|unique:regis_child,no_regis',

        ],
            [
//                'password_confirm.same'=>'password harus sama',
//                'required'=>'harus di isi'
            ]);

        if ($validator->fails()) {

            return view('app.puskesmas.receptionist.check.hasil.err',['puskes'=>$puskes]);

        }
        else{
            $regis = new RegisChild();
            $regis->id_child   = $id;
            $regis->id_puskes =  $pusdat;
            $regis->no_regis = $request->input('pendaftaran');

            $regis->save();
            return redirect()->back();
        }
    }

    public function no_regis($pusdat){
        $anak = Input::get('id_anak');
//        $pusk = Puskesmas::find($pusdat);
        $result = RegisChild::where('id_child',$anak)
            ->where('id_puskes',$pusdat)
            ->first();
        return response()->json($result);
    }
}
