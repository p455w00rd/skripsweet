<?php

namespace App\Http\Controllers;

use App\History_imun;
use App\Idl;
use App\Imunisasi;
use App\Kecamatan;
use App\Kecamatan2;
use App\PuskesKelurahan;
use App\Target;
use Carbon\Carbon;
use Illuminate\Http\Request;

class LaporanDinkesController extends Controller
{
    public function index(Request $request)
    {
        $imunisasi = Imunisasi::all();
        return view('app.dinkes.laporan.pws.index',['imunisasi'=>$imunisasi]);
    }

    public function laporan($id, Request $request)
    {
        if ($request->input('year')==null){
            $y = Carbon::now()->year;
        }
        else{
            $y = $request->input('year');
        }

        $kecamatan = Kecamatan2::with('puskesmas')
            ->with(array('puskesmas' => function($q) use($y)
            {
                $q->with(array('kelurahan' => function($q) use($y)
                {
                    $q->with(array('proyeksi' => function($q) use($y)
                    {
                        $q->where('tahun',$y);
                    }));
                }));
            }))
            ->get();
        $imunisasi = Imunisasi::findOrFail($id);

        $c=1;

        $history = History_imun::where('id_imunisasi',$id)
            ->whereYear('created_at','=',$y)
            ->get();

        return view('app.dinkes.laporan.pws.laporan',[
            'y'=>$y,'kecamatan'=>$kecamatan,'c'=>$c
            ,'history'=>$history,'imunisasi'=>$imunisasi]);
    }

    public function idl(Request $request)
    {
        if ($request->input('year')==null){
            $y = Carbon::now()->year;
        }
        else{
            $y = $request->input('year');
        }

        $kecamatan = Kecamatan2::with('puskesmas')
            ->with(array('puskesmas' => function($q) use($y)
            {
                $q->with(array('kelurahan' => function($q) use($y)
                {
                    $q->with(array('proyeksi' => function($q) use($y)
                    {
                        $q->where('tahun',$y);
                    }));
                }));
            }))
            ->get();

        $c=1;

        $history = Idl::all();

        return view('app.dinkes.laporan.pws.idl',[
            'y'=>$y,'kecamatan'=>$kecamatan,'c'=>$c
            ,'history'=>$history]);
    }

    public function gis(Request $request)
    {
        if ($request->input('year')==null){
            $y = Carbon::now()->year;
        }
        else{
            $y = $request->input('year');
        }

        $data=Kecamatan::with(array('proyeksi' => function($q) use($y)
            {
                $q->where('tahun',$y)->first();
            }))
            ->with(array('idl' => function($q) use($y)
            {
                $q->whereYear('created_at',$y);
            }))
            ->get();

        return view('app.dinkes.laporan.gis.index',['y'=>$y,'data'=>$data]);
    }
    public function gisdetail($y,$kel,Request $request)
    {
        $imunisasi=Imunisasi::whereNotIn('id',[12,13,15,14,16])
            ->with(array('target' => function($q) use($y)
            {
                $q->where('tahun',$y);
            }))
            ->get();
        $target = Target::where('tahun',$y)->get();
        $kelurah = Kecamatan::where('Name',$kel)
            ->with(array('idl' => function($q) use($y)
            {
                $q->whereYear('created_at',$y);
            }))
            ->with(array('imunisasi' => function($q) use($y)
            {
                $q->whereYear('created_at',$y);
            }))
            ->with(array('pro' => function($q) use($y)
            {
                $q->where('tahun',2016);
            }))
            ->first();
        return view('app.dinkes.laporan.gis.details',['data'=>$kelurah,'y'=>$y,
            'kelurahan'=>$kel,'imunisasi'=>$imunisasi,'target'=>$target]);
//        return view('app.dinkes.laporan.gis.index',['y'=>$y,'data'=>$data]);
    }
}
