<?php

namespace App\Http\Controllers;

use App\Puskesmas;
use App\Receptionist;
use App\RegisChild;
use App\Reschedule_imun;
use Carbon\Carbon;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
//    public function index()
//    {
//        return view('home');
//    }
    public function index($pusdat)
    {
        $now = Carbon::now();
        $seven_day = Carbon::now();
        $now_datang = Carbon::now();
        $month = $now->month;
        $year = $now->year;
        $day = $now->day;
        $seven_day = $seven_day->addDay(8)->toDateString();

        for ($i = 0;$i <= 7 ; $i++){
            $grafik[]=$now_datang->subDay(7)->toDateString();
//            $i=$i;
        }
//        dd($datang[0]);
//        $b = var_dump($a);
//        dd([$grafik]);
//        dd($now);

        $now2=$now->toDateString();
//        dd($now_string);

        $puskes=Puskesmas::findOrFail($pusdat);

        $pegawai = $puskes->user()->count();

        $datang = Receptionist::withTrashed()
                    ->where('id_puskes',$pusdat)
                    ->whereMonth('created_at',$month)
                    ->count();

        $anak = RegisChild::where('id_puskes',$pusdat);

        $anakT = $anak->whereYear('created_at',$year)
            ->count();

        $anakB = $anak->whereMonth('created_at',$month)
            ->count();

        $resB = Reschedule_imun::where('id_puskesmas',$pusdat)
            ->where('status',0)
            ->whereBetween('tanggal_kembali',[$now2,$seven_day])
            ->get();
        $resA = Reschedule_imun::where('id_puskesmas',$pusdat)
            ->where('status',0)
            ->where('tanggal_kembali','<',$now->toDateString())
            ->get();
//        dd($resA);
        $graf0 = Receptionist::withTrashed()
            ->where('id_puskes',$pusdat)
            ->whereBetween('created_at',[$grafik[0],$now])
            ->count();

        for ($i = 0;$i <= 6 ; $i++){

//            $c[]=$i+1;
            $graf[] = Receptionist::withTrashed()
                ->where('id_puskes',$pusdat)
                ->whereBetween('created_at',[$grafik[$i+1],$grafik[$i]])
                ->count();
        }
//        $graf=array_reverse($graf);



        return view('app.puskesmas.dashboard.index',['puskes' => $puskes,'pegawai'=>$pegawai,'datang'=>$datang
        ,'anakB'=>$anakB,'anakT'=>$anakT,'resB'=>$resB,'resA'=>$resA,'graf0'=>$graf0,'graf'=>$graf]);
    }
}
