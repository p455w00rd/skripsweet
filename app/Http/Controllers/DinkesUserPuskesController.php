<?php

namespace App\Http\Controllers;

use App\Puskesmas;
use App\Role;
use App\User;
use App\UserData;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class DinkesUserPuskesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $role=Role::where('id',2)
            ->first();
        $c=1;
        $user = $role->user()->get();

        return view('app.admin.dinkes.user.puskesmas.index',['user' => $user,'c'=>$c]);
    }

    public function create()
    {
        $super = Puskesmas::all();

        return view('app.admin.dinkes.user.puskesmas.create',['super' => $super]);
    }

    public function post(Request $request){
        $validator = Validator::make($request->all(), [
            'username'         => 'required|unique:users',       // just a normal required validation
            'email'            => 'required|email|unique:users',  // required and must be unique in the ducks table
            'password'         => 'required',
            'password_confirm' => 'required|same:password',    // required and has to match the password field
            'role' => 'required',
            'nip'             => 'required|unique:user_data|integer',
            'phone'           => 'required|numeric',// just a normal required validation
            'name'     => 'required',  // required and must be unique in the ducks table
        ],
            [
//                'password_confirm.same'=>'password harus sama',
//                'required'=>'harus di isi'
            ]);

        if ($validator->fails()) {

            return redirect(route('dinkes.user.puskesmas.create'))
                ->withErrors($validator)
                ->withInput();

        }else {

            $user = new User();
            $user->username = $request->input('username');
            $user->email = $request->input('email');
            $user->password = $request->input('password');

            //Save Pegawai
            $ortu = new UserData();
            $ortu->name = $request->input('name');
            $ortu->nip = $request->input('nip');
            $ortu->phone = $request->input('phone');

            $user->save();
            $newUser = $user->id;
            $ortu->user_id = $newUser;

            $ortu->save();

            User::find($newUser)->role()->attach($request->input('role'));
            User::find($newUser)->puskesmas()->attach($request->input('puskesmas'));

            return redirect(route('dinkes.user.puskesmas'));
        }
    }


}
