<?php

namespace App\Http\Controllers;

use App\Ortu;
use App\Reschedule_imun;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Spatie\GoogleCalendar\Event;

class CalendarController extends Controller
{
    public function index($id)
    {
        $ortu = Ortu::findOrFail($id);
        $anak = $ortu->anak()->get();
        foreach ($anak as $key=>$datas){
            $kel[$key]=$datas->id;
        }
        $cal = Reschedule_imun::whereIn('id_anak',$kel)
            ->get();
        return view('app.ortu.calendar.index',['id'=>$id,'ortu'=>$ortu,'cal'=>$cal]);
    }
    public function show($id,$rescedule)
    {
        $ortu = Ortu::findOrFail($id);

        $cal = Reschedule_imun::findOrFail($rescedule);

//        $date = Carbon::parse('2016-09-17');
        $now = Carbon::now();

//        $diff = $date->diffInDays($now);
//        dd($date);

        return view('app.ortu.calendar.show',['id'=>$id,'ortu'=>$ortu,'cal'=>$cal,'now'=>$now,'re'=>$rescedule]);
    }
    public function andro($id,$rescedule)
    {
        $ortu = Ortu::findOrFail($id);
        $email = $ortu->user()->first()->email;

        $cal = Reschedule_imun::findOrFail($rescedule);
        $anak = $cal->anak()->first()->name;

        $now = Carbon::now();

        $b=$cal->tanggal_kembali;

        $g1 =  Carbon::parse($b.'07:00:00');
        $g2 =  Carbon::parse($b.'09:00:00');
        $puskes = $cal->puskesmas()->first()->name;

        $kembali   = Carbon::parse($b)->format('d F Y');

        $event = new Event();

        $event->name = $anak.' diharapkan kembali ke '.$puskes.' pada tanggal '.$kembali;
        $event->startDateTime = $g1;
        $event->endDateTime = $g2;
        $event->addAttendee(['email' => $email]);

        $event->save();

        return view('app.ortu.calendar.show',['id'=>$id,'ortu'=>$ortu,'cal'=>$cal,'now'=>$now,'re'=>$rescedule]);
    }

}
