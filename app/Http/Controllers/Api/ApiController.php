<?php

namespace App\Http\Controllers\Api;

use App\Anak;
use App\Http\Controllers\Controller;
use App\Log_imun;
use App\Ortu;
use App\Puskesmas;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;
class ApiController extends Controller
{
    public function anak_puskes(Request $request)
    {
        $pusdat=$request->input('puskes');
        $puskes=Puskesmas::findOrFail($pusdat);

        $anak = $puskes->anak()->with('ortu')->with('no')->get();
//        dd($anak);

        return DataTables::of($anak)
            ->addColumn('noreg', function (Anak $anak) use ($pusdat) {
                return $anak->no->where('id_puskes',$pusdat)->first()->no_regis ;
            })
            ->editColumn('gender', function ($anak) {
                return $anak->gender == 1 ? 'Laki-Laki' : "Perempuan";
            })
            ->addColumn('umur', function ($anak) {
                return str_replace(['yang lalu','dari sekarang'], [''], $anak->diff());
            })
            ->addColumn('ttl', function ($anak) {
                return $anak->pob.', '.Carbon::parse($anak->dob)->format('d M Y');
            })
            ->addColumn('action', function ($anak) use ($pusdat){
                return '<a href="'.route('anak.profile',[$pusdat,$anak->id]).'" class="btn yellow-saffron m-icon m-icon-only ">
                                            <i class="icon-magnifier"></i>
                                        </a>';
            })
            ->make();
    }
    public function anak_all(Request $request)
    {
        $pusdat=$request->input('puskes');

        $anak = Anak::with('ortu')->with('no')->get();

        return DataTables::of($anak)
//            ->addColumn('noreg', function (Anak $anak) use ($pusdat) {
//                return $anak->no->where('id_puskes',$pusdat)->first()->no_regis ;
//            })
            ->editColumn('gender', function ($anak) {
                return $anak->gender == 1 ? 'Laki-Laki' : "Perempuan";
            })
            ->addColumn('umur', function ($anak) {
                return str_replace(['yang lalu','dari sekarang'], [''], $anak->diff());
            })
            ->addColumn('ttl', function ($anak) {
                return $anak->pob.', '.Carbon::parse($anak->dob)->format('d M Y');
            })
            ->addColumn('action', function ($anak) use ($pusdat){
                return '<a href="'.route('anak.profile',[$pusdat,$anak->id]).'" class="btn yellow-saffron m-icon m-icon-only ">
                                            <i class="icon-magnifier"></i>
                                        </a>';
            })
            ->make();
    }
    public function ortu(Request $request)
    {
        $pusdat=$request->input('puskes');

        $ortu = Ortu::with('user')->get();

        return DataTables::of($ortu)

            ->addColumn('action', function ($ortu) use ($pusdat){
                return '<a href="'.route('ortu.profile',[$pusdat,$ortu->id]).'" class="btn yellow-saffron m-icon m-icon-only ">
                                            <i class="icon-magnifier"></i>
                                        </a>';
            })
            ->make();
    }
    public function log(Request $request)
    {
        $pusdat=$request->input('puskes');

        $log = Log_imun::where('id_puskesmas',$pusdat)
            ->with('puskesmas')
            ->with('peg')
            ->with('imunisasi')
            ->with('anak')
            ->orderBy('created_at','desc')
            ->get();

        return DataTables::of($log)

            ->editColumn('created_at', function ($log) {
                return Carbon::parse($log->created_at)->format('d-M-Y H:i');
//                return $anak->gender == 1 ? 'Laki-Laki' : "Perempuan";
            })
            ->addColumn('log', function ($log) {
                return $log->peg->name.' menambahkan Imuniasasi '.$log->imunisasi->name.' pada Anak '.$log->anak->name;
            })
            ->addColumn('action', function ($log) use ($pusdat){
                return '<a href="'.route('anak.profile',[$pusdat,$log->anak->id]).'" class="btn yellow-saffron m-icon m-icon-only ">
                                            Details
                                        </a>';
//                return '<a href="'.route('anak.profile',[$pusdat,$log->anak->id]).'">'.$log->peg->name.' menambahkan Imuniasasi '.$log->imunisasi->name.' pada Anak '.$log->anak->name.'</a>';
            })
            ->make();
    }
}
