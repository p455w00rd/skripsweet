<?php

namespace App\Http\Controllers;

use App\puskes_place;
use Carbon\Carbon;
use Grimzy\LaravelMysqlSpatial\Types\LineString;
use Grimzy\LaravelMysqlSpatial\Types\Point;
use Grimzy\LaravelMysqlSpatial\Types\Polygon;
use Illuminate\Http\Request;
use Spatie\GoogleCalendar\Event;

class TestingController extends Controller
{
    public function index()
    {

        return view('test');
    }
    public function point()
    {

        $place1 = new puskes_place();
        $place1->name = 'Empire State Building';

// saving a point
        $place1->location = new Point(40.7484404, -73.9878441);	// (lat, lng)
        dd($place1);
        $place1->save();

// saving a polygon
        $place1->area = new Polygon([new LineString([
            new Point(40.74894149554006, -73.98615270853043),
            new Point(40.74848633046773, -73.98648262023926),
            new Point(40.747925497790725, -73.9851602911949),
            new Point(40.74837050671544, -73.98482501506805),
            new Point(40.74894149554006, -73.98615270853043)
        ])]);
        $place1->save();

//        $place1->area = new Polygon();
        dd($place1);
        return $place1;
    }
    public function cek1()
    {
        $cek = Puskes_place::first();
//        dd($cek->location);
        return $cek;
    }
    public function cal(){
//        dd('test calendar');
        $event = new Event();

        $event->name = 'sadasdasdasd';
        $event->startDateTime = Carbon::now();
        $event->endDateTime = Carbon::now()->addHour();
        $event->addAttendee(['email' => 'miutwocaracter@gmail.com']);

//        dd($event);

        $event->save();

    }
}
