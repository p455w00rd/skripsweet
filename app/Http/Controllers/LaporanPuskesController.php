<?php

namespace App\Http\Controllers;

use App\History_imun;
use App\Idl;
use App\Imunisasi;
use App\Kecamatan;
use App\Puskesmas;
use App\Target;
use Carbon\Carbon;
use Illuminate\Http\Request;

class LaporanPuskesController extends Controller
{
    public function pws($pusdat)
    {
        $imunisasi=Imunisasi::whereNotIn('id',[1])->get();
        $puskes=Puskesmas::findOrFail($pusdat);
        return view('app.puskesmas.laporan.pws.index',['puskes'=>$puskes,'imun'=>$imunisasi]);
    }

    public function pencatatan(Request $request, $pusdat)
    {
        $puskes = Puskesmas::findOrFail($pusdat);
        if ($request->input('kelurahan') ==null){
            $kelurahan = $puskes->kelurahan()->first();
        }
        else{
            $kelurahan = Kecamatan::find($request->input('kelurahan'));
        }
        if ($request->input('bulan')==null){
            $m = Carbon::now()->month;
        }
        else{
            $m = $request->input('bulan');
        }
        if ($request->input('year')==null){
            $y = Carbon::now()->year;
        }
        else{
            $y = $request->input('year');
        }
        if ($request->input('data')==null){
            $data = null;
        }
        else{
            $data = $request->input('data');
        }
        if ($request->input('data')==1){
            //ambil data berdasar registrasi anak ke puskesmas
            if ($request->input('bulan')==13){
                $anak = $puskes->anak_created()
                    ->where('child.location',$kelurahan->id)
                    ->whereYear('regis_child.created_at','=',$y)
                    ->with('ortu')
                    ->with('history_imun')
                    ->with(array('history_imun' => function($q) use($pusdat,$m,$y)
                    {
                        $q->where('id_puskesmas',$pusdat);
                    }))
                    ->get();
            }
            else{
                $anak = $puskes->anak_created()
                    ->whereMonth('regis_child.created_at',$m)
                    ->whereYear('regis_child.created_at','=',$y)
                    ->where('child.location',$kelurahan->id)
                    ->with('ortu')
                    ->with(array('history_imun' => function($q) use($pusdat,$m,$y)
                    {
                        $q->where('id_puskesmas',$pusdat);
                    }))
                    ->get();
            }
        }
        else{
            //ambil data berdasar tanggal imunisasi anak
            if ($request->input('bulan')==13){
                $anak = $puskes->anak()
                    ->where('child.location',$kelurahan->id)
                    ->with('ortu')
                    ->with(array('history_imun' => function($q) use($pusdat,$m,$y)
                    {
                        $q->where('id_puskesmas',$pusdat)
                            ->whereYear('created_at','=',$y);
                    }))
                    ->whereHas('history_imun',function($q) use($pusdat,$m,$y){
                        $q->where('id_puskesmas',$pusdat)
                            ->whereYear('created_at','=',$y);
                    })
                    ->get();
            }
            else{
                $anak = $puskes->anak()
                    ->where('child.location',$kelurahan->id)
                    ->with('ortu')
                    ->whereHas('history_imun',function($q) use($pusdat,$m,$y){
                        $q->where('id_puskesmas',$pusdat)
                            ->whereMonth('created_at',$m)
                            ->whereYear('created_at','=',$y);
                    })
                    ->with(array('history_imun' => function($q) use($pusdat,$m,$y)
                    {
                        $q->where('id_puskesmas',$pusdat)
                            ->whereYear('created_at','=',$y);
                    }))
                    ->get();
            }
        }

//        dd($anak);
        $c=1;

        return view('app.puskesmas.laporan.pencatatan',['puskes'=>$puskes , 'anak'=>$anak,'c'=>$c,'kelurahan'=>$kelurahan,
            'm'=>$m,'y'=>$y,'data'=>$data]);
    }
    public function pencatatanExcel(Request $request, $pusdat)
    {
        $puskes = Puskesmas::findOrFail($pusdat);
        if ($request->input('kelurahan') == null) {
            $kelurahan = $puskes->kelurahan()->first();
        } else {
            $kelurahan = Kecamatan::find($request->input('kelurahan'));
        }
        if ($request->input('bulan') == null) {
            $m = Carbon::now()->month;
        } else {
            $m = $request->input('bulan');
        }
        if ($request->input('year') == null) {
            $y = Carbon::now()->year;
        } else {
            $y = $request->input('year');
        }
        if ($request->input('data') == null) {
            $data = null;
        } else {
            $data = $request->input('data');
        }
        if ($request->input('data') == 1) {
            //ambil data berdasar registrasi anak ke puskesmas
            if ($request->input('bulan') == 13) {
                $anak = $puskes->anak_created()
                    ->where('child.location', $kelurahan->id)
                    ->whereYear('regis_child.created_at', '=', $y)
                    ->with('ortu')
                    ->with('history_imun')
                    ->with(array('history_imun' => function ($q) use ($pusdat, $m, $y) {
                        $q->where('id_puskesmas', $pusdat);
                    }))
                    ->get();
            } else {
                $anak = $puskes->anak_created()
                    ->whereMonth('regis_child.created_at', $m)
                    ->whereYear('regis_child.created_at', '=', $y)
                    ->where('child.location', $kelurahan->id)
                    ->with('ortu')
                    ->with(array('history_imun' => function ($q) use ($pusdat, $m, $y) {
                        $q->where('id_puskesmas', $pusdat);
                    }))
                    ->get();
            }
        } else {
            //ambil data berdasar tanggal imunisasi anak
            if ($request->input('bulan') == 13) {
                $anak = $puskes->anak()
                    ->where('child.location', $kelurahan->id)
                    ->with('ortu')
                    ->with(array('history_imun' => function ($q) use ($pusdat, $m, $y) {
                        $q->where('id_puskesmas', $pusdat)
                            ->whereYear('created_at', '=', $y);
                    }))
                    ->whereHas('history_imun', function ($q) use ($pusdat, $m, $y) {
                        $q->where('id_puskesmas', $pusdat)
                            ->whereYear('created_at', '=', $y);
                    })
                    ->get();
            } else {
                $anak = $puskes->anak()
                    ->where('child.location', $kelurahan->id)
                    ->with('ortu')
                    ->whereHas('history_imun', function ($q) use ($pusdat, $m, $y) {
                        $q->where('id_puskesmas', $pusdat)
                            ->whereMonth('created_at', $m)
                            ->whereYear('created_at', '=', $y);
                    })
                    ->with(array('history_imun' => function ($q) use ($pusdat, $m, $y) {
                        $q->where('id_puskesmas', $pusdat)
                            ->whereYear('created_at', '=', $y);
                    }))
                    ->get();
            }
        }

//        dd($anak);
        $c = 1;

        return view('app.puskesmas.laporan.excel.pencatatan', ['puskes' => $puskes, 'anak' => $anak, 'c' => $c, 'kelurahan' => $kelurahan,
            'm' => $m, 'y' => $y, 'data' => $data]);
    }


    public function hb(Request $request, $pusdat)
    {

        if ($request->input('bulan')==null){
            $m = Carbon::now()->month;
        }
        else{
            $m = $request->input('bulan');
        }
        if ($request->input('year')==null){
            $y = Carbon::now()->year;
        }
        else{
            $y = $request->input('year');
        }
        switch($m) {
            case 1:
                $content ='Januari';
                break;
            case 2:
                $content = 'Februari';
                break;
            case 3:
                $content = 'Maret';
                break;
            case 4:
                $content = 'April';
                break;
            case 5:
                $content = 'Mei';
                break;
            case 6:
                $content = 'Juni';
                break;
            case 7:
                $content = 'Juli';
                break;
            case 8:
                $content = 'Agustus';
                break;
            case 9:
                $content = 'September';
                break;
            case 10:
                $content = 'Oktober';
                break;
            case 11:
                $content = 'November';
                break;
            case 12:
                $content = 'Desember';
                break;
        }

        $dateS = Carbon::parse()->startOfMonth()->Month(1)->Year($y);
        $dateE = Carbon::now()->endOfMonth()->Month($m)->Year($y);
        $target = Target::where('tahun',$y)->where('id_imunisasi',1)->first();

        if (isset($target)){
            $target = $target->target;
        }
        else{
            $target = 0;
        }

        $puskes = Puskesmas::findOrFail($pusdat);
        $anak = $puskes->anak()->get();
        $c=1;

        $history = History_imun::where('umur','like','%jam')
            ->where('id_imunisasi','=',1)
            ->where('id_puskesmas','=',$pusdat)->whereBetween('created_at',[$dateS,$dateE]);
        $sd = History_imun::where('umur','like','%jam')
            ->where('id_imunisasi','=',1)
            ->where('id_puskesmas','=',$pusdat)->whereBetween('created_at',[$dateS,$dateE]);

//        dd($history);
        return view('app.puskesmas.laporan.pws.hb.hb',['puskes'=>$puskes , 'anak'=>$anak,'c'=>$c,'history'=>$history,
            'm'=>$m,'y'=>$y,'content'=>$content,'dateS'=>$dateS,'dateE'=>$dateE,'sd'=>$sd,'target'=>$target]);
    }

    public function hbExcel(Request $request, $pusdat)
    {

        if ($request->input('bulan')==null){
            $m = Carbon::now()->month;
        }
        else{
            $m = $request->input('bulan');
        }
        if ($request->input('year')==null){
            $y = Carbon::now()->year;
        }
        else{
            $y = $request->input('year');
        }
        switch($m) {
            case 1:
                $content ='Januari';
                break;
            case 2:
                $content = 'Februari';
                break;
            case 3:
                $content = 'Maret';
                break;
            case 4:
                $content = 'April';
                break;
            case 5:
                $content = 'Mei';
                break;
            case 6:
                $content = 'Juni';
                break;
            case 7:
                $content = 'Juli';
                break;
            case 8:
                $content = 'Agustus';
                break;
            case 9:
                $content = 'September';
                break;
            case 10:
                $content = 'Oktober';
                break;
            case 11:
                $content = 'November';
                break;
            case 12:
                $content = 'Desember';
                break;
        }

        $dateS = Carbon::parse()->startOfMonth()->Month(1)->Year($y);
        $dateE = Carbon::now()->endOfMonth()->Month($m)->Year($y);
        $target = Target::where('tahun',$y)->where('id_imunisasi',1)->first();

        if (isset($target)){
            $target = $target->target;
        }
        else{
            $target = 0;
        }

        $puskes = Puskesmas::findOrFail($pusdat);
        $anak = $puskes->anak()->get();
        $c=1;

        $history = History_imun::where('umur','like','%jam')
            ->where('id_imunisasi','=',1)
            ->where('id_puskesmas','=',$pusdat)->whereBetween('created_at',[$dateS,$dateE]);
        $sd = History_imun::where('umur','like','%jam')
            ->where('id_imunisasi','=',1)
            ->where('id_puskesmas','=',$pusdat)->whereBetween('created_at',[$dateS,$dateE]);

//        dd($history);
        return view('app.puskesmas.laporan.excel.pws.hb.hb',['puskes'=>$puskes , 'anak'=>$anak,'c'=>$c,'history'=>$history,
            'm'=>$m,'y'=>$y,'content'=>$content,'dateS'=>$dateS,'dateE'=>$dateE,'sd'=>$sd,'target'=>$target]);
    }

    public function hb_hari(Request $request, $pusdat)
    {

        if ($request->input('bulan')==null){
            $m = Carbon::now()->month;
        }
        else{
            $m = $request->input('bulan');
        }
        if ($request->input('year')==null){
            $y = Carbon::now()->year;
        }
        else{
            $y = $request->input('year');
        }
        switch($m) {
            case 1:
                $content ='Januari';
                break;
            case 2:
                $content = 'Februari';
                break;
            case 3:
                $content = 'Maret';
                break;
            case 4:
                $content = 'April';
                break;
            case 5:
                $content = 'Mei';
                break;
            case 6:
                $content = 'Juni';
                break;
            case 7:
                $content = 'Juli';
                break;
            case 8:
                $content = 'Agustus';
                break;
            case 9:
                $content = 'September';
                break;
            case 10:
                $content = 'Oktober';
                break;
            case 11:
                $content = 'November';
                break;
            case 12:
                $content = 'Desember';
                break;
        }

        $dateS = Carbon::parse()->startOfMonth()->Month(1)->Year($y);
        $dateE = Carbon::now()->endOfMonth()->Month($m)->Year($y);
        $target = Target::where('tahun',$y)->where('id_imunisasi',1)->first();
        if (isset($target)){
            $target = $target->target;
        }
        else{
            $target = 0;
        }
        $puskes = Puskesmas::findOrFail($pusdat);
        $anak = $puskes->anak()->get();
        $c=1;

        $history = History_imun::where('umur','like','%hari')
            ->where('id_imunisasi','=',1)
            ->where('id_puskesmas','=',$pusdat)->whereBetween('created_at',[$dateS,$dateE]);
        $sd = History_imun::where('umur','like','%hari')
            ->where('id_imunisasi','=',1)
            ->where('id_puskesmas','=',$pusdat)->whereBetween('created_at',[$dateS,$dateE]);

//        dd($history);
        return view('app.puskesmas.laporan.pws.hb.hb_hari',['puskes'=>$puskes , 'anak'=>$anak,'c'=>$c,'history'=>$history,
            'm'=>$m,'y'=>$y,'content'=>$content,'dateS'=>$dateS,'dateE'=>$dateE,'sd'=>$sd,'target'=>$target]);
    }

    public function hb_hariExcel(Request $request, $pusdat)
    {

        if ($request->input('bulan')==null){
            $m = Carbon::now()->month;
        }
        else{
            $m = $request->input('bulan');
        }
        if ($request->input('year')==null){
            $y = Carbon::now()->year;
        }
        else{
            $y = $request->input('year');
        }
        switch($m) {
            case 1:
                $content ='Januari';
                break;
            case 2:
                $content = 'Februari';
                break;
            case 3:
                $content = 'Maret';
                break;
            case 4:
                $content = 'April';
                break;
            case 5:
                $content = 'Mei';
                break;
            case 6:
                $content = 'Juni';
                break;
            case 7:
                $content = 'Juli';
                break;
            case 8:
                $content = 'Agustus';
                break;
            case 9:
                $content = 'September';
                break;
            case 10:
                $content = 'Oktober';
                break;
            case 11:
                $content = 'November';
                break;
            case 12:
                $content = 'Desember';
                break;
        }

        $dateS = Carbon::parse()->startOfMonth()->Month(1)->Year($y);
        $dateE = Carbon::now()->endOfMonth()->Month($m)->Year($y);
        $target = Target::where('tahun',$y)->where('id_imunisasi',1)->first();
        if (isset($target)){
            $target = $target->target;
        }
        else{
            $target = 0;
        }
        $puskes = Puskesmas::findOrFail($pusdat);
        $anak = $puskes->anak()->get();
        $c=1;

        $history = History_imun::where('umur','like','%hari')
            ->where('id_imunisasi','=',1)
            ->where('id_puskesmas','=',$pusdat)->whereBetween('created_at',[$dateS,$dateE]);
        $sd = History_imun::where('umur','like','%hari')
            ->where('id_imunisasi','=',1)
            ->where('id_puskesmas','=',$pusdat)->whereBetween('created_at',[$dateS,$dateE]);

//        dd($history);
        return view('app.puskesmas.laporan.excel.pws.hb.hb_hari',['puskes'=>$puskes , 'anak'=>$anak,'c'=>$c,'history'=>$history,
            'm'=>$m,'y'=>$y,'content'=>$content,'dateS'=>$dateS,'dateE'=>$dateE,'sd'=>$sd,'target'=>$target]);
    }

    public function hb_minggu(Request $request, $pusdat)
    {

        if ($request->input('bulan')==null){
            $m = Carbon::now()->month;
        }
        else{
            $m = $request->input('bulan');
        }
        if ($request->input('year')==null){
            $y = Carbon::now()->year;
        }
        else{
            $y = $request->input('year');
        }
        switch($m) {
            case 1:
                $content ='Januari';
                break;
            case 2:
                $content = 'Februari';
                break;
            case 3:
                $content = 'Maret';
                break;
            case 4:
                $content = 'April';
                break;
            case 5:
                $content = 'Mei';
                break;
            case 6:
                $content = 'Juni';
                break;
            case 7:
                $content = 'Juli';
                break;
            case 8:
                $content = 'Agustus';
                break;
            case 9:
                $content = 'September';
                break;
            case 10:
                $content = 'Oktober';
                break;
            case 11:
                $content = 'November';
                break;
            case 12:
                $content = 'Desember';
                break;
        }

        $dateS = Carbon::parse()->startOfMonth()->Month(1)->Year($y);
        $dateE = Carbon::now()->endOfMonth()->Month($m)->Year($y);

        $target = Target::where('tahun',$y)->where('id_imunisasi',1)->first();
        if (isset($target)){
            $target = $target->target;
        }
        else{
            $target = 0;
        }

        $puskes = Puskesmas::findOrFail($pusdat);
        $anak = $puskes->anak()->get();
        $c=1;

        $history = History_imun::where('umur','NOT LIKE','%hari')
            ->where('umur','NOT LIKE','%jam')
            ->where('id_imunisasi','=',1)
            ->where('id_puskesmas','=',$pusdat)->whereBetween('created_at',[$dateS,$dateE]);
        $sd = History_imun::where('umur','NOT LIKE','%hari')
            ->where('umur','NOT LIKE','%jam')
            ->where('id_imunisasi','=',1)
            ->where('id_puskesmas','=',$pusdat)->whereBetween('created_at',[$dateS,$dateE]);

//        dd($history->get());
        return view('app.puskesmas.laporan.pws.hb.hb_minggu',['puskes'=>$puskes , 'anak'=>$anak,'c'=>$c,'history'=>$history,
            'm'=>$m,'y'=>$y,'content'=>$content,'dateS'=>$dateS,'dateE'=>$dateE,'sd'=>$sd,'target'=>$target]);
    }

    public function hb_mingguExcel(Request $request, $pusdat)
    {

        if ($request->input('bulan')==null){
            $m = Carbon::now()->month;
        }
        else{
            $m = $request->input('bulan');
        }
        if ($request->input('year')==null){
            $y = Carbon::now()->year;
        }
        else{
            $y = $request->input('year');
        }
        switch($m) {
            case 1:
                $content ='Januari';
                break;
            case 2:
                $content = 'Februari';
                break;
            case 3:
                $content = 'Maret';
                break;
            case 4:
                $content = 'April';
                break;
            case 5:
                $content = 'Mei';
                break;
            case 6:
                $content = 'Juni';
                break;
            case 7:
                $content = 'Juli';
                break;
            case 8:
                $content = 'Agustus';
                break;
            case 9:
                $content = 'September';
                break;
            case 10:
                $content = 'Oktober';
                break;
            case 11:
                $content = 'November';
                break;
            case 12:
                $content = 'Desember';
                break;
        }

        $dateS = Carbon::parse()->startOfMonth()->Month(1)->Year($y);
        $dateE = Carbon::now()->endOfMonth()->Month($m)->Year($y);

        $target = Target::where('tahun',$y)->where('id_imunisasi',1)->first();
        if (isset($target)){
            $target = $target->target;
        }
        else{
            $target = 0;
        }

        $puskes = Puskesmas::findOrFail($pusdat);
        $anak = $puskes->anak()->get();
        $c=1;

        $history = History_imun::where('umur','NOT LIKE','%hari')
            ->where('umur','NOT LIKE','%jam')
            ->where('id_imunisasi','=',1)
            ->where('id_puskesmas','=',$pusdat)->whereBetween('created_at',[$dateS,$dateE]);
        $sd = History_imun::where('umur','NOT LIKE','%hari')
            ->where('umur','NOT LIKE','%jam')
            ->where('id_imunisasi','=',1)
            ->where('id_puskesmas','=',$pusdat)->whereBetween('created_at',[$dateS,$dateE]);

//        dd($history->get());
        return view('app.puskesmas.laporan.excel.pws.hb.hb_minggu',['puskes'=>$puskes , 'anak'=>$anak,'c'=>$c,'history'=>$history,
            'm'=>$m,'y'=>$y,'content'=>$content,'dateS'=>$dateS,'dateE'=>$dateE,'sd'=>$sd,'target'=>$target]);
    }

    public function other(Request $request, $pusdat,$id)
    {

        if ($request->input('bulan')==null){
            $m = Carbon::now()->month;
        }
        else{
            $m = $request->input('bulan');
        }
        if ($request->input('year')==null){
            $y = Carbon::now()->year;
        }
        else{
            $y = $request->input('year');
        }
        switch($m) {
            case 1:
                $content ='Januari';
                break;
            case 2:
                $content = 'Februari';
                break;
            case 3:
                $content = 'Maret';
                break;
            case 4:
                $content = 'April';
                break;
            case 5:
                $content = 'Mei';
                break;
            case 6:
                $content = 'Juni';
                break;
            case 7:
                $content = 'Juli';
                break;
            case 8:
                $content = 'Agustus';
                break;
            case 9:
                $content = 'September';
                break;
            case 10:
                $content = 'Oktober';
                break;
            case 11:
                $content = 'November';
                break;
            case 12:
                $content = 'Desember';
                break;
        }

        $dateS = Carbon::parse()->startOfMonth()->Month(1)->Year($y);
        $dateE = Carbon::now()->endOfMonth()->Month($m)->Year($y);

        $puskes = Puskesmas::findOrFail($pusdat);
        $imunisasi = Imunisasi::findOrFail($id);
        $target = Target::where('tahun',$y)->where('id_imunisasi',$id)->first();
        if (isset($target)){
            $target = $target->target;
        }
        else{
            $target = 0;
        }
        $anak = $puskes->anak()->get();
        $c=1;


        $history = History_imun::where('id_imunisasi',$id)
            ->where('id_puskesmas','=',$pusdat)->whereBetween('created_at',[$dateS,$dateE]);
        $sd = History_imun::where('id_imunisasi',$id)
            ->where('id_puskesmas','=',$pusdat)->whereBetween('created_at',[$dateS,$dateE]);

//        dd($history);
        return view('app.puskesmas.laporan.pws.other',['puskes'=>$puskes , 'anak'=>$anak,'c'=>$c,'history'=>$history,
            'm'=>$m,'y'=>$y,'content'=>$content,'dateS'=>$dateS,'dateE'=>$dateE,'sd'=>$sd,'imunisasi'=>$imunisasi,
            'target'=>$target]);
    }

    public function otherExcel(Request $request, $pusdat,$id)
    {

        if ($request->input('bulan')==null){
            $m = Carbon::now()->month;
        }
        else{
            $m = $request->input('bulan');
        }
        if ($request->input('year')==null){
            $y = Carbon::now()->year;
        }
        else{
            $y = $request->input('year');
        }
        switch($m) {
            case 1:
                $content ='Januari';
                break;
            case 2:
                $content = 'Februari';
                break;
            case 3:
                $content = 'Maret';
                break;
            case 4:
                $content = 'April';
                break;
            case 5:
                $content = 'Mei';
                break;
            case 6:
                $content = 'Juni';
                break;
            case 7:
                $content = 'Juli';
                break;
            case 8:
                $content = 'Agustus';
                break;
            case 9:
                $content = 'September';
                break;
            case 10:
                $content = 'Oktober';
                break;
            case 11:
                $content = 'November';
                break;
            case 12:
                $content = 'Desember';
                break;
        }

        $dateS = Carbon::parse()->startOfMonth()->Month(1)->Year($y);
        $dateE = Carbon::now()->endOfMonth()->Month($m)->Year($y);

        $puskes = Puskesmas::findOrFail($pusdat);
        $imunisasi = Imunisasi::findOrFail($id);
        $target = Target::where('tahun',$y)->where('id_imunisasi',$id)->first();
        if (isset($target)){
            $target = $target->target;
        }
        else{
            $target = 0;
        }
        $anak = $puskes->anak()->get();
        $c=1;


        $history = History_imun::where('id_imunisasi',$id)
            ->where('id_puskesmas','=',$pusdat)->whereBetween('created_at',[$dateS,$dateE]);
        $sd = History_imun::where('id_imunisasi',$id)
            ->where('id_puskesmas','=',$pusdat)->whereBetween('created_at',[$dateS,$dateE]);

//        dd($history);
        return view('app.puskesmas.laporan.excel.pws.other',['puskes'=>$puskes , 'anak'=>$anak,'c'=>$c,'history'=>$history,
            'm'=>$m,'y'=>$y,'content'=>$content,'dateS'=>$dateS,'dateE'=>$dateE,'sd'=>$sd,'imunisasi'=>$imunisasi,
            'target'=>$target]);
    }

    public function idl(Request $request, $pusdat)
    {
        if ($request->input('bulan')==null){
            $m = Carbon::now()->month;
        }
        else{
            $m = $request->input('bulan');
        }
        if ($request->input('year')==null){
            $y = Carbon::now()->year;
        }
        else{
            $y = $request->input('year');
        }
        switch($m) {
            case 1:
                $content ='Januari';
                break;
            case 2:
                $content = 'Februari';
                break;
            case 3:
                $content = 'Maret';
                break;
            case 4:
                $content = 'April';
                break;
            case 5:
                $content = 'Mei';
                break;
            case 6:
                $content = 'Juni';
                break;
            case 7:
                $content = 'Juli';
                break;
            case 8:
                $content = 'Agustus';
                break;
            case 9:
                $content = 'September';
                break;
            case 10:
                $content = 'Oktober';
                break;
            case 11:
                $content = 'November';
                break;
            case 12:
                $content = 'Desember';
                break;
        }

        $dateS = Carbon::parse()->startOfMonth()->Month(1)->Year($y);
        $dateE = Carbon::now()->endOfMonth()->Month($m)->Year($y);

        $puskes = Puskesmas::findOrFail($pusdat);
        $anak = $puskes->anak()->get();
        $c=1;


        $history = Idl::where('id_puskesmas','=',$pusdat)
            ->whereBetween('created_at',[$dateS,$dateE]);
        $sd = Idl::where('id_puskesmas','=',$pusdat)
            ->whereBetween('created_at',[$dateS,$dateE]);
        $target = 90;
//        dd($history);
        return view('app.puskesmas.laporan.pws.idl',['puskes'=>$puskes , 'anak'=>$anak,'c'=>$c,'history'=>$history,
            'm'=>$m,'y'=>$y,'content'=>$content,'dateS'=>$dateS,'dateE'=>$dateE,'sd'=>$sd,'target'=>$target]);
    }

    public function idlExcel(Request $request, $pusdat)
    {
        if ($request->input('bulan')==null){
            $m = Carbon::now()->month;
        }
        else{
            $m = $request->input('bulan');
        }
        if ($request->input('year')==null){
            $y = Carbon::now()->year;
        }
        else{
            $y = $request->input('year');
        }
        switch($m) {
            case 1:
                $content ='Januari';
                break;
            case 2:
                $content = 'Februari';
                break;
            case 3:
                $content = 'Maret';
                break;
            case 4:
                $content = 'April';
                break;
            case 5:
                $content = 'Mei';
                break;
            case 6:
                $content = 'Juni';
                break;
            case 7:
                $content = 'Juli';
                break;
            case 8:
                $content = 'Agustus';
                break;
            case 9:
                $content = 'September';
                break;
            case 10:
                $content = 'Oktober';
                break;
            case 11:
                $content = 'November';
                break;
            case 12:
                $content = 'Desember';
                break;
        }

        $dateS = Carbon::parse()->startOfMonth()->Month(1)->Year($y);
        $dateE = Carbon::now()->endOfMonth()->Month($m)->Year($y);

        $puskes = Puskesmas::findOrFail($pusdat);
        $anak = $puskes->anak()->get();
        $c=1;


        $history = Idl::where('id_puskesmas','=',$pusdat)
            ->whereBetween('created_at',[$dateS,$dateE]);
        $sd = Idl::where('id_puskesmas','=',$pusdat)
            ->whereBetween('created_at',[$dateS,$dateE]);
        $target = 90;
//        dd($history);
        return view('app.puskesmas.laporan.excel.pws.idl',['puskes'=>$puskes , 'anak'=>$anak,'c'=>$c,'history'=>$history,
            'm'=>$m,'y'=>$y,'content'=>$content,'dateS'=>$dateS,'dateE'=>$dateE,'sd'=>$sd,'target'=>$target]);
    }
}
