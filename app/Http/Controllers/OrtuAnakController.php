<?php

namespace App\Http\Controllers;

use App\Kecamatan;
use Illuminate\Http\Request;
use App\Ortu;
use App\Anak;
use Carbon\Carbon;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;

class OrtuAnakController extends Controller
{
    public function create($id){
        $ortu=Ortu::findOrFail($id);
        return view('app.puskesmas.pendaftar.child.create',['id'=>$id,'ortu'=>$ortu]);
    }
    public function post($id, Request $request){

        $validator = Validator::make($request->all(), [

            'name'             => 'required',
            'pob'           => 'required',// just a normal required validation
            'dob'     => 'required',  // required and must be unique in the ducks table
            'gender'  => 'required',

        ],
            [
//                'password_confirm.same'=>'password harus sama',
//                'required'=>'harus di isi'
            ]);
        $ortu=Ortu::findOrFail($id);

        if ($validator->fails()) {

//            $user = new User();
//            $user=User::find($user->id);
//            dd($user);
//            $user->delete();
            return redirect(route('ortu.anak.create',[$ortu->id]))
                ->withErrors($validator)
                ->withInput();

        }else{
            $anak = new Anak();

            $anak->name = $request->input('name');
            $anak->nik = $request->input('nik');
            $anak->pob = $request->input('pob');
            $anak->dob = Carbon::parse($request->input('dob'))->format('Y-m-d');
            $anak->gender = $request->input('gender');
            $anak->address = $ortu->address;
            $anak->location = $ortu->location;
            $anak->id_parent = $ortu->id;

            $anak->save();
            $idanak=$anak->id;

//            $regis = new RegisChild();
//
//            $regis->id_puskes = $pusdat;
//            $regis->no_regis = $request->input('pendaftaran');
//            $regis->id_child = $idanak;
//            $regis->save();

            return redirect()->route('ortu.anak.profile',[$id,$idanak]);
        }

    }
    public function profile($id,$anak){

        $ortu=Ortu::findOrFail($id);
        $anak2 = Anak::find($anak);
        return view('app.puskesmas.pendaftar.child.profile.index',['child'=>$anak2,'id'=>$id,'ortu'=>$ortu]);
    }
    public function height($id,$anak){
        $ortu=Ortu::findOrFail($id);
        $anak2 = Anak::find($anak);
        $i=1;
        return view('app.puskesmas.pendaftar.child.profile.height',['i'=>$i,'child'=>$anak2,'id'=>$id,'ortu'=>$ortu]);
    }
    public function details($id,$anak){
        $ortu=Ortu::findOrFail($id);
        $anak2 = Anak::find($anak);
        $i=1;
        return view('app.puskesmas.pendaftar.child.profile.keterangan',['i'=>$i,'child'=>$anak2,'id'=>$id,'ortu'=>$ortu]);
    }
    public function update_details($id,$anak, Request $request){
        if ($request->input('surviving')==true){
            $surviving = 1;
        }
        else{
            $surviving= 0;
        }
        $anak2 = Anak::findOrFail($anak);
        $anak2->keterangan = $request->input('keterangan');
        $anak2->surviving = $surviving;
        $anak2->save();

        return redirect()->route('ortu.anak.profile',[$id,$anak]);
    }
    public function account($id,$anak){
        $ortu=Ortu::findOrFail($id);
        $kecamatan = Kecamatan::all();
        $anak2 = Anak::find($anak);
        return view('app.puskesmas.pendaftar.child.profile.account',['child'=>$anak2, 'kecamatan'=>$kecamatan,
            'ortu'=>$ortu,'id'=>$id]);
    }
    public function update($id,$anak, Request $request){

//        dd($anak);
        $anak2 = Anak::findOrFail($anak);
        $dob = Carbon::parse($request->input('dob'))->format('Y-m-d');
        Input::merge(['dob' => $dob]);
        $input = $request->all();

//        dd($request->input('dob'));
//        dd($input);
        $anak2->fill($input)->save();

        return redirect()->route('ortu.anak.profile',[$id,$anak]);
    }
}
