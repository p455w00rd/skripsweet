<?php

namespace App\Http\Controllers;

use App\Kecamatan;
use App\Kecamatan2;
use App\Puskes_place;
use App\Puskesmas;
use Grimzy\LaravelMysqlSpatial\Types\Point;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class PuskesmasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $puskesmas = Puskesmas::all();
        return view('app.admin.puskesmas.index',['super' => $puskesmas]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $kecamatan = Kecamatan2::all();
        return view('app.admin.puskesmas.create',['kecamatan' => $kecamatan]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
//        dd($request);
        $validator = Validator::make($request->all(), [

            'name'             => 'required',
            'address'             => 'required',
            'phone'           => 'required|numeric',// just a normal required validation
            'location'     => 'required',  // required and must be unique in the ducks table
        ]);

        if ($validator->fails()) {

//            $user = new User();
//            $user=User::find($user->id);
//            dd($user);
//            $user->delete();
            return redirect(route('puskesmas.dinkes.create'))
                ->withErrors($validator)
                ->withInput();

        }else{
            $puskesmas = new Puskesmas();
            $input = $request->all();
            $loc=new Point($request->lat, $request->long);
            $puskesmas->fill($input)->save();

            $coor =new Puskes_place();
            $coor->id_puskesmas = $puskesmas->id;
            $coor->location = $loc;
            $coor->save();

            Puskesmas::find($puskesmas->id)->kelurahan()->sync($request->input('kelurahan'));


            return redirect()->route('puskesmas.dinkes');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $puskesmas= Puskesmas::findOrFail($id);
        $puskes = $puskesmas;
        $kecamatan = Kecamatan::all();
        $kecamatan2 = Kecamatan2::all();
        return view('app.admin.puskesmas.edit',['puskes'=>$puskes,'super' => $puskesmas , 'kecamatan' => $kecamatan, 'kecamatan2' => $kecamatan2]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
//        dd(Auth::user()->role()->whereNotIn('roles.id',[1,5])->count());
        $puskesmas = Puskesmas::findOrFail($id);

        $input = $request->all();
        $loc=new Point($request->lat, $request->long);

        if (isset($puskesmas->coordinate()->first()->id)){
            $coor=$puskesmas->coordinate()->first();
        }
        else{
            $coor =new Puskes_place();
            $coor->id_puskesmas = $puskesmas->id;
        }
        $coor->location = $loc;



//        dd($request->input('dob'));
//        dd($input);
        $coor->save();
        $puskesmas->fill($input)->save();

        $puskesmas->kelurahan()->sync($request->input('kelurahan'));

        if (Auth::user()->role()->whereNotIn('roles.id',[1,5])->count()==0){
            return redirect()->route('puskesmas.dinkes');
        }
        else{
            return redirect()->route('puskesmas.dashboard',$id);
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
