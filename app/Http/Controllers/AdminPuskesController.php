<?php

namespace App\Http\Controllers;

use App\Imunisasi;
use App\Kecamatan2;
use App\Puskesmas;
use App\User;
use App\UserData;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class AdminPuskesController extends Controller
{
    public function index($pusdat){
        $puskes=Puskesmas::findOrFail($pusdat);
        $user = $puskes->user()->get();
        return view('app.admin.user.index',['puskes'=>$puskes,'user' => $user]);
    }
    public function create($pusdat){
        $puskes=Puskesmas::findOrFail($pusdat);
        $user = $puskes->user()->get();
        return view('app.admin.user.create',['puskes'=>$puskes,'user' => $user]);
    }
    public function post($pusdat, Request $request){
        $validator = Validator::make($request->all(), [
            'username'         => 'required|unique:users',       // just a normal required validation
            'email'            => 'required|email|unique:users',  // required and must be unique in the ducks table
            'password'         => 'required',
            'password_confirm' => 'required|same:password',    // required and has to match the password field
            'role' => 'required',
            'nip'             => 'required|unique:user_data|integer',
            'phone'           => 'required',// just a normal required validation
            'name'     => 'required',  // required and must be unique in the ducks table
        ],
            [
//                'password_confirm.same'=>'password harus sama',
//                'required'=>'harus di isi'
            ]);

        if ($validator->fails()) {

            return redirect(route('puskesmas.user.create',$pusdat))
                ->withErrors($validator)
                ->withInput();

        }else {

            $user = new User();
            $user->username = $request->input('username');
            $user->email = $request->input('email');
            $user->password = $request->input('password');

            //Save Pegawai
            $ortu = new UserData();
            $ortu->name = $request->input('name');
            $ortu->nip = $request->input('nip');
            $ortu->phone = $request->input('phone');

            $user->save();
            $newUser = $user->id;
            $ortu->user_id = $newUser;

            $ortu->save();

            User::find($newUser)->role()->attach($request->input('role'));
            User::find($newUser)->puskesmas()->attach($pusdat);

            return redirect(route('puskesmas.user.index', $pusdat));
        }
    }

    public function edit($pusdat){
        $puskes=Puskesmas::findOrFail($pusdat);
        $kecamatan2 = Kecamatan2::all();
        return view('app.admin.puskesmas.edit',['puskes'=>$puskes,'kecamatan2'=>$kecamatan2]);
    }
    public function editProfile($pusdat,$id,$code){
        $puskes=Puskesmas::findOrFail($pusdat);
        $user = User::findOrFail($id);
        return view('app.admin.user.edit',['puskes'=>$puskes,'user'=>$user,'code'=>$code]);
    }
    public function imunisasi($pusdat){
        $puskes=Puskesmas::findOrFail($pusdat);
        $imunisasi = Imunisasi::all();
        return view('app.admin.imunisasi.index',['puskes'=>$puskes,'imun'=>$imunisasi]);
    }
    public function update($pusdat, Request $request,$id,$userData,$code){
//        dd($request->input('role'));
        $validator = Validator::make($request->all(), [
            'username'         => 'required',       // just a normal required validation
            'email'            => 'required',  // required and must be unique in the ducks table
            'role' => 'required',
            'nip'             => 'required|integer',
            'phone'           => 'required',// just a normal required validation
            'name'     => 'required',  // required and must be unique in the ducks table
        ],
            [
//                'password_confirm.same'=>'password harus sama',
//                'required'=>'harus di isi'
            ]);

        if ($validator->fails()) {

            return redirect(route('puskesmas.user.edit',[$pusdat,$id,$code]))
                ->withErrors($validator)
                ->withInput();

        }else {
            if ($request->input('password') != null) {
                $user = User::findOrFail($id);
                $user->username = $request->input('username');
                $user->email = $request->input('email');
                $user->password = $request->input('password');
            }
            else{
                $user = User::findOrFail($id);
                $user->username = $request->input('username');
                $user->email = $request->input('email');
            }
            //Save Pegawai
            $ortu = UserData::findOrFail($userData);
            $ortu->name = $request->input('name');
            $ortu->nip = $request->input('nip');
            $ortu->phone = $request->input('phone');

            $user->save();
            $ortu->save();

            User::find($user->id)->role()->sync($request->input('role'));

            if ($code ==2){
                return redirect(route('puskesmas.user.index', $pusdat));
            }
            else{
                return redirect('home');
            }
        }
    }
}
