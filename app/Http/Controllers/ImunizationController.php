<?php

namespace App\Http\Controllers;

use App\Anak;
use App\Berat;
use App\History_imun;
use App\Idl;
use App\Imunisasi;
use App\Kecamatan;
use App\Log_imun;
use App\Puskesmas;
use App\Receptionist;
use App\Reschedule_imun;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Spatie\GoogleCalendar\Event;

class ImunizationController extends Controller
{
    public function create($pusdat,$id,$recept)
    {
        $puskes = Puskesmas::findOrFail($pusdat);
        $imun=Imunisasi::all();
        $anak = Anak::findOrFail($id);
        $kelurahan = Kecamatan::all();
        return view('app.puskesmas.imunisasi.create',['anak'=>$anak,'imun' => $imun,'puskes'=>$puskes,'kelurahan'=>$kelurahan,'recept'=>$recept]);
    }

    public function create_langsung($pusdat,$id)
    {
        $puskes = Puskesmas::findOrFail($pusdat);
        $imun=Imunisasi::all();
        $anak = Anak::findOrFail($id);
        $kelurahan = Kecamatan::all();
        return view('app.puskesmas.imunisasi.create',['anak'=>$anak,'imun' => $imun,'puskes'=>$puskes,'kelurahan'=>$kelurahan]);
    }

    public function post(Request $request, $pusdat,$id)
    {
//        dd($request);
        $anak = Anak::findOrFail($id);
        $puskes = Puskesmas::findOrFail($pusdat);
        $g1 =  Carbon::parse($request->input('back').'07:00:00');
        $g2 =  Carbon::parse($request->input('back').'09:00:00');
        $kembali   = Carbon::parse($request->input('back'))->format('d F Y');
//        dd($kembali);
        $email = $anak->ortu()->first()->user()->first()->email;
        $berat = new Berat();
        $berat->id_anak     = $id;
        $berat->berat     = $request->input('berat');

        $remove_sch = $anak->kembali_imun()->get()->last();
        if ($remove_sch != null){
        $remove_sch->status     = 1;
        $remove_sch->save();
        }
//        dd($remove_sch);

        $back = new Reschedule_imun();
        $back->id_anak     = $id;
        $back->id_puskesmas = $pusdat;
        $back->tanggal_kembali   = Carbon::parse($request->input('back'))->format('Y-m-d');
//        dd($email);

        if ($request->has('idl')){
            $idl = new Idl();
            $idl->id_anak = $id;
            $idl->location = $request->input('kelurahan');
            $idl->id_puskesmas = $pusdat;
            $idl->gender= $request->input('gender');
            $idl->save();
        }

        $berat->save();
        $idberat = $berat->id;
        $back->save();

        if ($request->input('recept')!=null){
            $recept=$request->input('recept');
            $recept = Receptionist::find($recept);
            $recept->status = 1;
            $recept->save();
        }


        $imuni = $request->input('imun');
        $date = $request->input('date');

        for($i=0;$i<count($imuni);$i++){

            $record= new History_imun();
            $record->id_anak     = $id;
            $record->id_puskesmas   = $pusdat;
            $record->id_imunisasi = $imuni[$i];
            $record->id_berat = $idberat;
            $record->created_at   = Carbon::parse($date[$i])->format('Y-m-d H:i:s');
            $record->umur= $request->input('umur');
            $record->gender= $request->input('gender');
            $record->location = $request->input('kelurahan');
            $record->id_peg = Auth::user()->user_data()->first()->id;
            $record->save();

            $log = new Log_imun();
            $log->id_puskesmas = $pusdat;
            $log->id_imunisasi = $imuni[$i];
            $log->id_peg = Auth::user()->user_data()->first()->id;
            $log->id_anak = $id;
            $log->save();
        }
        $event = new Event();

        $event->name = $anak->name.' diharapkan kembali ke '.$puskes->name.' pada tanggal '.$kembali;
        $event->startDateTime = $g1;
        $event->endDateTime = $g2;
        $event->addAttendee(['email' => $email]);

        $event->save();

//        foreach($request->input('imun') as $key => $value) {
//
//        }

        if ($request->input('recept')!=null){
        return redirect()->route('receptionist.index',$pusdat);
        }
        else{
            return redirect()->route('anak.profile',[$pusdat,$id]);
        }
    }

    public function delete($pusdat,$id, $i)
    {

        History_imun::destroy($i);

        return redirect()->route('anak.profile',[$pusdat,$id]);
    }

    public function update(Request $request, $pusdat,$id, $i)
    {

        $history = History_imun::find($i);

        $history->created_at = Carbon::parse( $request->input('date'))->format('Y-m-d H:i:s');
        $history->save();

        return redirect()->route('anak.profile',[$pusdat,$id]);
    }
}
