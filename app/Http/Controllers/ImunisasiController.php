<?php

namespace App\Http\Controllers;

use App\Imunisasi;
use App\Target;
use Illuminate\Http\Request;

class ImunisasiController extends Controller
{
    public function index()
    {
        $imun=Imunisasi::all();
        return view('app.admin.imunisasi.index',['imun' => $imun]);
    }
    public function create()
    {
        return view('app.admin.imunisasi.create');
    }
    public function post(Request $request)
    {
        $imun = new Imunisasi();
        $imun->name     = $request->input('name');

        $target = new Target();
        $target->target = $request->input('target');
        $target->tahun = $request->input('tahun');

        $imun->save();
        $target->id_imunisasi = $imun->id;
        $target->save();

        return redirect()->route('imunisasi.dinkes');
    }
    public function edit($id)
    {
        $imun = Imunisasi::find($id);

        return view('app.admin.imunisasi.edit',['imun' => $imun]);
    }
    public function update(Request $request, $id)
    {
        $imun = Imunisasi::find($id);
        $imun->name     = $request->input('name');
//
//        $target = new Target();
//        $target->target = $request->input('target');
//        $target->tahun = $request->input('tahun');

        $imun->save();
//        $target->id_imunisasi = $imun->id;
//        $target->save();

        return redirect()->route('imunisasi.dinkes');
    }
    public function show($id)
    {
        $imun = Imunisasi::find($id);

        return view('app.admin.imunisasi.show',['imun' => $imun]);
    }
    public function target_add($id)
    {
        $imun = Imunisasi::find($id);

        return view('app.admin.imunisasi.target.add',['imun' => $imun]);
    }
    public function target_post(Request $request, $id)
    {
        $target = new Target();
        $target->target = $request->input('target');
        $target->tahun = $request->input('tahun');
        $target->id_imunisasi = $id;
        $target->save();

        return redirect()->route('imunisasi.dinkes');
    }
    public function target_edit(Request $request, $id)
    {
        $target = Target::findOrFail($request->input('id'));
        $target->target = $request->input('target');
        $target->save();

        return redirect()->route('imunisasi.show.dinkes',$id);
    }
}
