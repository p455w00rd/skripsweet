<?php

namespace App\Http\Controllers;

use App\Proyeksi;
use App\Puskesmas;
use Illuminate\Http\Request;

class SurvivingController extends Controller
{
    public function index($pusdat)
    {
        $puskes = Puskesmas::findOrFail($pusdat);

        $data=$puskes->kelurahan()->get();
        foreach ($data as $key=>$datas){
            $kel[$key]=$datas->id;
        }
        $kel=collect($kel);

        $proyeksi=Proyeksi::whereIn('kelurahan',$kel)->get();

        return view('app.puskesmas.surviving.index',['puskes'=>$puskes,'proyeksi'=>$proyeksi]);
    }
    public function edit($pusdat,$id)
    {
        $puskes = Puskesmas::findOrFail($pusdat);
        $proyeksi=Proyeksi::findOrFail($id);

        return view('app.puskesmas.surviving.edit',['puskes'=>$puskes,'proyeksi'=>$proyeksi]);
    }
    public function update(Request $request, $pusdat)
    {
//        dd($request);
        $puskes = Puskesmas::findOrFail($pusdat);
        $proyeksi=Proyeksi::findOrFail($request->input('id'));
        $proyeksi->SL = $request->input('sl');
        $proyeksi->SP = $request->input('sp');
        $proyeksi->save();
//        return $proyeksi;

        return redirect()->route('surviving.index',['puskes'=>$puskes]);
    }
}
