<?php

namespace App\Http\Controllers;

use App\Anak;
use App\Kecamatan;
use App\MailVerification;
use App\Ortu;
use App\ParentPlace;
use App\Puskes_place;
use App\RegisChild;
use Carbon\Carbon;
use Grimzy\LaravelMysqlSpatial\Types\Point;
use Illuminate\Http\Request;
use App\User;
//use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use App\Puskesmas;
//use Illuminate\Mail\Mailer;


class OrtuController extends Controller
{
    public function index($pusdat)
    {
        $puskes=Puskesmas::findOrFail($pusdat);
        $count = 1;
//        $ortu = Ortu::all();
        return view('app.puskesmas.pendaftar.parent.index',['puskes' =>$puskes,'count' => $count]);
    }
    public function create($pusdat)
    {
        $puskes=Puskesmas::findOrFail($pusdat);
        $kecamatan = Kecamatan::all();
        return view('app.puskesmas.pendaftar.parent.create',['kecamatan' => $kecamatan,'puskes'=>$puskes]);
    }
    public function post(Request $request,$pusdat)
    {
//        $puskes=Puskesmas::findOrFail($pusdat);
        $validator = Validator::make($request->all(), [
            'username'         => 'required|unique:users',       // just a normal required validation
            'email'            => 'required|email|unique:users',  // required and must be unique in the ducks table
            'password'         => 'required',
            'password_confirm' => 'required|same:password',    // required and has to match the password field

            'nik'             => 'required|unique:parent|integer',
            'phone'           => 'required|numeric',// just a normal required validation
            'name-parent'     => 'required',  // required and must be unique in the ducks table
            'address-parent'  => 'required',
            'location'        => 'required',    // required and has to match the password field

//            'name-child'=>'name',
            'name-child'             => 'required',
            'pendaftaran'           => 'required|unique:regis_child,no_regis',// just a normal required validation
            'dob-child'     => 'required',  // required and must be unique in the ducks table
            'gender-child'  => 'required',
        ],
            [
//                'password_confirm.same'=>'password harus sama',
//                'required'=>'harus di isi'
            ]);

        if ($validator->fails()) {

//            $user = new User();
//            $user=User::find($user->id);
//            dd($user);
//            $user->delete();
            return redirect(route('ortu.create',$pusdat))
                ->withErrors($validator)
                ->withInput();

        }else{
            $loc=new Point($request->lat, $request->long);

            //Save User
            $user = new User();
            $user->username     = $request->input('username');
            $user->email    = $request->input('email');
            $user->password =  $request->input('password');


            //Save Ortu
            $ortu = new Ortu();
            $ortu->name = $request->input('name-parent');
            $ortu->nik = $request->input('nik');
            $ortu->phone = $request->input('phone');
            $ortu->address = $request->input('address-parent');
            $ortu->location = $request->input('location');


            //Save Child
            $anak = new Anak();

            $anak->name = $request->input('name-child');
            $anak->nik = $request->input('nik-child');
            $anak->pob = $request->input('pob-child');
            $anak->dob = Carbon::parse($request->input('dob-child'))->format('Y-m-d');
            $anak->gender = $request->input('gender-child');
            $anak->address = $request->input('address-parent');
            $anak->location = $request->input('location');

            $regis = new RegisChild();

            $regis->id_puskes = $pusdat;
            $regis->no_regis = $request->input('pendaftaran');

            $coor =new ParentPlace();

            $coor->location = $loc;

            $user->save();
            $newUser=$user->id;
            $ortu->id_user = $newUser;

            $ortu->save();
            $newParent=$ortu->id;
            $coor->id_parent = $newParent;
            $anak->id_parent = $newParent;

            $anak->save();
            $newAnak=$anak->id;
            $regis->id_child = $newAnak;

            $regis->save();

            if ($request->lat!=null){
                $coor->save();
            }

            User::find($newUser)->role()->attach(3);

            // redirect ----------------------------------------
            // redirect our user back to the form so they can do it all over again
           return redirect(route('anak.profile',[$pusdat,$newAnak]));

        }

    }

    public function profile($pusdat,$id)
    {
        $puskes=Puskesmas::findOrFail($pusdat);
        $kecamatan = Kecamatan::all();
        $ortu=Ortu::find($id);

        return view('app.puskesmas.pendaftar.parent.profile.profile',['ortu'=>$ortu,'kecamatan'=>$kecamatan,'puskes'=>$puskes]);
    }
    public function anak($pusdat,$id)
    {
        $puskes=Puskesmas::findOrFail($pusdat);
        $ortu=Ortu::find($id);
        $anaks=$ortu->anak()->paginate(10);
//        dd($anak);
        return view('app.puskesmas.pendaftar.parent.profile.child',['ortu'=>$ortu,'puskes'=>$puskes,'anaks'=>$anaks]);
    }

    public function account($pusdat,$id)
    {
        $puskes=Puskesmas::findOrFail($pusdat);
        $ortu=Ortu::find($id);
        return view('app.puskesmas.pendaftar.parent.profile.account',['ortu'=>$ortu,'puskes'=>$puskes]);
    }
    public function update_account(Request $request,$pusdat, $id)
    {

//        dd($request->input('username'));
        if ($request->input('password') != null) {
            $user = User::find($request->input('id_user'));
            $user->username = $request->input('username');
            $user->email = $request->input('email');
            $user->password = $request->input('password');
            $user->save();
        } else {
            $user = User::find($request->input('id_user'));
//            dd($user);
            $user->username = $request->input('username');
            $user->email = $request->input('email');
//            dd($user);
            $user->save();
        }
        return redirect()->route('ortu.profile',[$pusdat,$id]);
    }
    public function update_profile(Request $request,$pusdat, $id)
    {
//        dd($request);
        $user = Ortu::find($id);
        $user->name = $request->input('name-parent');
        $user->address = $request->input('address-parent');
        $user->phone = $request->input('phone');
        $user->location = $request->input('location');
        $user->save();

        return redirect()->route('ortu.profile',[$pusdat,$id]);
    }
    public function map($pusdat,$id)
    {
        $puskes=Puskesmas::findOrFail($pusdat);
        $ortu=Ortu::find($id);
        return view('app.puskesmas.pendaftar.parent.profile.map',['ortu'=>$ortu,'puskes'=>$puskes]);
    }
    public function editmap($pusdat,$id, Request $request)
    {
        $ortu=Ortu::find($id);

        $loc=new Point($request->lat, $request->long);

        if (isset($ortu->place()->first()->id)){
            $coor=$ortu->place()->first();
        }
        else{
            $coor =new ParentPlace();
            $coor->id_parent = $ortu->id;
        }
        $coor->location = $loc;

        $coor->save();
        return redirect()->route('ortu.profile',[$pusdat,$id]);
    }

    public function near($id)
    {

        $ortu=Ortu::findOrFail($id);

        $near=$ortu->place()->first();
        if ($near != null){
            $near = $near->location;


        $distsp = Puskes_place::distanceExcludingSelf('location',$near,0.02)->get();
//        return $distsp;
        }
        else{
            $near = null;
            $distsp = null;
        }
        return view('app.ortu.near.index',['ortu'=>$ortu,'near'=>$near,'dist'=>$distsp,'id'=>$id]);
    }
    public function dashboard($id)
    {

        $ortu=Ortu::findOrFail($id);

        return view('app.ortu.dashboard.index',['ortu'=>$ortu,'id'=>$id]);
    }
    public function profile_me($id)
    {

        $ortu=Ortu::findOrFail($id);
        $kecamatan = Kecamatan::all();
        return view('app.puskesmas.pendaftar.parent.profile.profile',['ortu'=>$ortu,'kecamatan'=>$kecamatan,'id'=>$id]);

    }
    public function profile_update_me(Request $request, $id)
    {
//        dd($request);
        $user = Ortu::find($id);
        $user->name = $request->input('name-parent');
        $user->address = $request->input('address-parent');
        $user->phone = $request->input('phone');
        $user->location = $request->input('location');
        $user->save();

        return redirect()->route('ortu.profile.me',[$id]);
    }
    public function account_me($id)
    {
        $ortu=Ortu::find($id);
        return view('app.puskesmas.pendaftar.parent.profile.account',['ortu'=>$ortu,'id'=>$id]);
    }
    public function account_me_update(Request $request, $id)
    {
//        dd($request->input('username'));
        if ($request->input('password') != null) {
            $user = User::find($request->input('id_user'));
            $user->username = $request->input('username');
            $user->email = $request->input('email');
            $user->password = $request->input('password');
            $user->save();
        } else {
            $user = User::find($request->input('id_user'));
//            dd($user);
            $user->username = $request->input('username');
            $user->email = $request->input('email');
//            dd($user);
            $user->save();
        }
        return redirect()->route('ortu.profile.me',[$id]);
    }

    public function map_me($id)
    {
        $ortu=Ortu::find($id);
        return view('app.puskesmas.pendaftar.parent.profile.map',['ortu'=>$ortu,'id'=>$id]);
    }
    public function map_me_update($id, Request $request)
    {
        $ortu=Ortu::find($id);

        $loc=new Point($request->lat, $request->long);

        if (isset($ortu->place()->first()->id)){
            $coor=$ortu->place()->first();
        }
        else{
            $coor =new ParentPlace();
            $coor->id_parent = $ortu->id;
        }
        $coor->location = $loc;

        $coor->save();
        return redirect()->route('ortu.profile.me',[$id]);
    }
    public function anak_me($id)
    {
        $ortu=Ortu::find($id);
//        return json_decode($ortu);
        $anak = $ortu->anak()->paginate(10);

//        dd($anak);
        return view('app.puskesmas.pendaftar.parent.profile.child',['ortu'=>$ortu,'id'=>$id,'anaks'=>$anak]);
    }

    public function post_reg(Request $request)
    {
//        dd($request->input('email'));
//        $puskes=Puskesmas::findOrFail($pusdat);
        $validator = Validator::make($request->all(), [
            'name'             =>'required',
            'username'         => 'required|unique:users',       // just a normal required validation
            'email'            => 'required|email|unique:users',  // required and must be unique in the ducks table
            'password'         => 'required',
            'password_confirm' => 'required|same:password',    // required and has to match the password field

        ],
            [
//                'password_confirm.same'=>'password harus sama',
//                'required'=>'harus di isi'
            ]);

        if ($validator->fails()) {

            return redirect(route('register'))
                ->withErrors($validator)
                ->withInput();

        }else{

            $token=str_random(255);
            //Save User
            $user = new User();
            $user->username     = $request->input('username');
            $user->email    = $request->input('email');
            $user->password =  $request->input('password');

            //Save Ortu
            $ortu = new Ortu();
            $ortu->name = $request->input('name');

            //Mail Verification
            $verification = new MailVerification();

            $verification->token= $token;
            $verification->email= $request->input('email');

            $user->save();
            $newUser= $user->id;
            $ortu->id_user = $newUser;
            $ortu->save();

            User::find($newUser)->role()->attach(3);

            $verification->user_id=$newUser;
            $verification->save();
            $input=$request->all();
//            $data=$this->create($input)->toArray();
//            dd($data);

            Mail::send('mail.confirmation', ['email' => $request->input('email'), 'token' => $token], function ($message) use ($request)
            {
                $message->subject('Verifikasi Email SI IMUN');
                $message->from('si.imun.surabaya19@gmail.com', 'SI IMUN SURABAYA');
                $message->to($request->input('email'));
            });

            // redirect ----------------------------------------
            // redirect our user back to the form so they can do it all over again
            return redirect(route('ortu_reg_suc'));

        }

    }
    public function reg_success()
    {
        return view('auth.success');
    }
    public function ortu_data($id)
    {
//        dd($id);
        $ortu = Ortu::find($id);
        $kelurahan = Kecamatan::all();
        return view('app.ortu.after_register.index',['UserId'=>$id,'kelurahan'=>$kelurahan,'ortu'=>$ortu]);
    }
    public function post_ortu_data(Request $request,$id)
    {
//        dd($id);
//        dd($request->all());
        $validator = Validator::make($request->all(), [
            'nik'             => 'required|unique:parent|integer',
            'phone'           => 'required|numeric',// just a normal required validation
            'name-parent'     => 'required',  // required and must be unique in the ducks table
            'address-parent'  => 'required',
            'location'        => 'required',    // required and has to match the password field

//            'name-child'=>'name',
            'name-child'             => 'required',
            'dob-child'     => 'required',  // required and must be unique in the ducks table
            'gender-child'  => 'required',
        ],
            [
//                'password_confirm.same'=>'password harus sama',
//                'required'=>'harus di isi'
            ]);

        if ($validator->fails()) {

            return redirect(route('lengkapi',$id))
                ->withErrors($validator)
                ->withInput();

        }else{

            $loc=new Point($request->lat, $request->long);

            //Save Ortu
            $ortu = Ortu::findOrFail($id);
            $ortu->name = $request->input('name-parent');
            $ortu->nik = $request->input('nik');
            $ortu->phone = $request->input('phone');
            $ortu->address = $request->input('address-parent');
            $ortu->location = $request->input('location');

            //Save Child
            $anak = new Anak();

            $anak->name = $request->input('name-child');
            $anak->nik = $request->input('nik-child');
            $anak->pob = $request->input('pob-child');
            $anak->dob = Carbon::parse($request->input('dob-child'))->format('Y-m-d');
            $anak->gender = $request->input('gender-child');
            $anak->address = $request->input('address-parent');
            $anak->location = $request->input('location');

            $coor =new ParentPlace();

            $coor->location = $loc;

            $ortu->save();
            $newParent=$ortu->id;
            $coor->id_parent = $newParent;
            $anak->id_parent = $newParent;

            $anak->save();

            if ($request->lat!=null){
                $coor->save();
            }

            // redirect ----------------------------------------
            // redirect our user back to the form so they can do it all over again
            return redirect('');

        }

    }
    public function send_verify($id)
    {
        $ortu=Ortu::find($id);
        $user = $ortu->user()->first();
        $user_id = $user->id;
        $email = $user->email;
        $chek = MailVerification::where('user_id',$user_id)
            ->get();
//        dd($chek);

        if ($chek->count()==0)
        {
            $token=str_random(255);
            $verification = new MailVerification();

            $verification->token = $token;
            $verification->email = $email;
            $verification->user_id =$user_id;
            $verification->save();
        }
        else{
            $token = $chek->first()->token;

        }
        Mail::send('mail.confirmation', ['email' => $email, 'token' => $token], function ($message) use ($email)
        {
            $message->subject('Verifikasi Email SI IMUN');
            $message->from('si.imun.surabaya19@gmail.com', 'SI IMUN SURABAYA');
            $message->to($email);
        });

        return redirect('home');
    }

    public function verify($token)
    {
        $kelurahan = MailVerification::where('token',$token)
            ->first();
//        dd($kelurahan);
        $kelurahan->confirmed = 1;
        $kelurahan->save();
        return redirect('home');
    }

}
