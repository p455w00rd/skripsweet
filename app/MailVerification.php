<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MailVerification extends Model
{
    protected $table = 'mail_verification';

    protected $fillable = ['id_user','email','confirmed','token'];
}
