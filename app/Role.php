<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    protected $table = 'roles';

    protected $fillable = ['type'];

    public function user()
    {
        return $this->belongsToMany(User::class ,'users_roles', 'role_id', 'user_id');
    }
}
