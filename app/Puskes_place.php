<?php

namespace App;

use Grimzy\LaravelMysqlSpatial\Eloquent\SpatialTrait;
use Illuminate\Database\Eloquent\Model;

class Puskes_place extends Model
{
    use SpatialTrait;
    protected $table = 'puskes_places';

    protected $fillable = [
        'id_puskesmas'
    ];

    protected $spatialFields = [
        'location'
    ];
    public function puskesmas()
    {
        return $this->belongsTo(Puskesmas::class, 'id_puskesmas' , 'id');
    }
}
