<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Imunisasi extends Model
{
    use SoftDeletes;

    protected $table = 'imunisasi';

    protected $fillable = ['name'];

    public function target()
    {
        return $this->hasMany(Target::class, 'id_imunisasi', 'id' );
    }
}
