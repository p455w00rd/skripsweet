<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Idl extends Model
{
    protected $table = 'idl';

    protected $fillable = ['id_anak','location','id_puskesmas','gender'];

    public function anak()
    {
        return $this->belongsTo(Anak::class, 'id_anak' , 'id');
    }
}
