<?php

return [

    /*
     * Path to the json file containing the credentials.
     */
    'service_account_credentials_json' => storage_path('app/google-calendar/calendar puskesmas-ffc4a51159e3.json'),

    /*
     *  The id of the Google Calendar that will be used by default.
     */
    'calendar_id' => 'si.imun.surabaya19@gmail.com',
];
